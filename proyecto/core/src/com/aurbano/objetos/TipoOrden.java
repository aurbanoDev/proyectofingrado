package com.aurbano.objetos;

/**
 * Created by adriu on 26/01/2016.
 */
public enum TipoOrden {
    NOMBRE,
    TIPO_OBJETO,
    CANTIDAD,
    BATALLA
}

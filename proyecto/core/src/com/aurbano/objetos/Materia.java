package com.aurbano.objetos;

import com.badlogic.gdx.scenes.scene2d.ui.Image;

/**
 * Created by adriu on 04/01/2016.
 */
public class Materia extends Objeto {

    private boolean equipada;

    public Materia(int id, String nombre, String descripcion, TipoObjeto tipoMateria) {
        super(id, nombre, descripcion, tipoMateria);

        cantidad = 1;
        equipada = false;
        icono = tipoMateria.getIcono();
    }

    public boolean isEquipada() {
        return equipada;
    }

    public void setEquipada(boolean equipada) {
        this.equipada = equipada;
    }

    @Override
    public Image getIcono() {
        if (icono == null)
            icono = tipo.getIcono();

        return icono;
    }
}

package com.aurbano.objetos;

import com.aurbano.gestores.GestorRecursos;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

import static com.aurbano.juego.Constantes.ATLAS_GUI;

/**
 * Created by adriu on 09/01/2016.
 */
public class Dinero extends Objeto {

    public Dinero(int cantidad) {
        super(RegistroObjetos.DINERO, "Monedas", "", TipoObjeto.DINERO);

        this.cantidad = cantidad;
        icono = GestorRecursos.nuevaImagen(ATLAS_GUI, "oro");
    }

}

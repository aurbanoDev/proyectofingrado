package com.aurbano.objetos;

import com.badlogic.gdx.scenes.scene2d.ui.Image;


/**
 * Created by adriu on 13/12/2015.
 */
public class Arma extends Equipo {

    private int ataque;
    private int ataqueMagico;
    private int porcentajeAtaque;

    public Arma(int id, String nombre, String descripcion, int ataque, int ataqueMagico, int porcentajeAtaque,
                int ranurasDobles, int ranurasSimples, TipoObjeto tipoArma) {

        super(id, nombre, descripcion, tipoArma, ranurasDobles, ranurasSimples);

        this.ataque = ataque;
        this.ataqueMagico = ataqueMagico;
        this.porcentajeAtaque = porcentajeAtaque;
        this.icono = tipoArma.getIcono();
    }

    public int getAtaque() {
        return ataque;
    }

    public int getAtaqueMagico() {
        return ataqueMagico;
    }

    public int getPorcentajeAtaque() {
        return porcentajeAtaque;
    }

    @Override
    public Image getIcono() {
        if (icono == null)
            icono = tipo.getIcono();

        return icono;
    }
}

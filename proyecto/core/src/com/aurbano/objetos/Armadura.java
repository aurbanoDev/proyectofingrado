package com.aurbano.objetos;

import com.aurbano.gestores.GestorRecursos;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

import static com.aurbano.juego.Constantes.ATLAS_GUI;

/**
 * Created by adriu on 13/12/2015.
 */
public class Armadura extends Equipo {

    private int defensa;
    private int defensaMagica;
    private int porcentajeDefensa;


    public Armadura(int id, String nombre, String descripcion, int defensa, int defensaMagica, int porcentajeDefensa,
                    int ranurasDobles, int ranurasSimples) {

        super(id, nombre, descripcion, TipoObjeto.ARMADURA, ranurasDobles, ranurasSimples);

        this.defensa = defensa;
        this.defensaMagica = defensaMagica;
        this.porcentajeDefensa = porcentajeDefensa;
        this.icono = new Image(GestorRecursos.getAtlas(ATLAS_GUI).findRegion("armadura"));
    }

    public int getDefensa() {
        return defensa;
    }

    public int getDefensaMagica() {
        return defensaMagica;
    }

    public int getPorcentajeDefensa() {
        return porcentajeDefensa;
    }

}

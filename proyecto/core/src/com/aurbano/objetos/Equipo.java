package com.aurbano.objetos;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by adriu on 01/12/2015.
 */
public abstract class Equipo extends Objeto {

    protected boolean equipado;
    protected int ranurasDobles;
    protected int ranurasSimples;

    protected ArrayList<Materia> materias;

    public Equipo(int id, String nombre, String descripcion, TipoObjeto tipo, int ranurasDobles, int ranurasSimples) {
        super(id, nombre, descripcion, tipo);

        cantidad = 1;
        this.ranurasSimples = ranurasSimples;
        this.ranurasDobles = ranurasDobles;
    }

    public boolean isEquipado() {
        return equipado;
    }

    public void setEquipado(boolean equipado) {
        this.equipado = equipado;
    }

    public int getRanurasSimples() {
        return ranurasSimples;
    }

    public int getRanurasDobles() {
        return ranurasDobles;
    }

}

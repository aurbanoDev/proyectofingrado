package com.aurbano.objetos;

import com.aurbano.Utilidades.UtilsGdx;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

import java.io.Serializable;

/**
 * Clase abstracta que define la estructura básica de los objetos del juego.
 *
 * @author Adrián Urbano
 */
public abstract class Objeto implements Serializable {

    protected final int id;
    protected int cantidad;
    protected final String nombre;
    protected final String descripcion;
    protected final TipoObjeto tipo;
    protected transient Image icono;


    public Objeto(final int id, final String nombre, final String descripcion, TipoObjeto tipo) {
        this.id = id;
        this.tipo = tipo;
        this.nombre = nombre;
        this.descripcion = descripcion;
    }

    public int getId() {
        return id;
    }

    public Image getIcono() {
        if (icono == null)
            icono = tipo.getIcono();

        return icono;
    }

    public int getCantidad() {
        return cantidad;
    }

    public String getCantidadComoString() {
        return UtilsGdx.formatearCantidad(cantidad).append(' ').toString();
    }

    public String getNombre() {
        return nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public TipoObjeto getTipo() {
        return tipo;
    }

    @Override
    public boolean equals(Object obj) {
        Objeto objeto = (Objeto) obj;
        return this.getId() == objeto.getId();
    }
}

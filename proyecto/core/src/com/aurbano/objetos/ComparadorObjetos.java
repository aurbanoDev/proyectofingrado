package com.aurbano.objetos;

import java.util.Comparator;

/**
 * Comparator de objetos para agrupar por tipo.
 *
 * Created by adriu on 26/01/2016.
 */
public class ComparadorObjetos implements Comparator<Objeto> {

    private TipoOrden tipoOrden;

    public void setTipoOrden(TipoOrden tipoOrden) {
        this.tipoOrden = tipoOrden;
    }

    @Override
    public int compare(Objeto o1, Objeto o2) {
        switch (tipoOrden) {
            case NOMBRE:
                return o1.getNombre().compareTo(o2.getNombre());
            case TIPO_OBJETO:
                return o1.getTipo().compareTo(o2.getTipo());
            case CANTIDAD:
                return o2.getCantidad() - o1.getCantidad();
            case BATALLA:
                return o1.getId() - o2.getId();
            default:
                return 0;
        }
    }
}

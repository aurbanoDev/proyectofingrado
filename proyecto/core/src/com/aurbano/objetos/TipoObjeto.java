package com.aurbano.objetos;

import com.aurbano.gestores.GestorRecursos;
import com.aurbano.juego.Constantes;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

import static com.aurbano.juego.Constantes.ATLAS_GUI;


/**
 * Enumeracion para clasificar los objetos.
 *
 * Created by adriu on 27/11/2015.
 */
public enum TipoObjeto {

    ELEMENTO {
        @Override
        public Image getIcono() {
            return GestorRecursos.nuevaImagen(Constantes.ATLAS_GUI, "elemento");
        }
    },

    ARMA_ESPADA {
        @Override
        public Image getIcono() {
            return GestorRecursos.nuevaImagen(ATLAS_GUI, "espada");
        }
    },

    ARMA_LANZA {
        @Override
        public Image getIcono() {
            return GestorRecursos.nuevaImagen(ATLAS_GUI, "lanza");
        }
    },

    ARMA_BASTON {
        @Override
        public Image getIcono() {
            return GestorRecursos.nuevaImagen(ATLAS_GUI, "baston");
        }
    },

    ARMADURA {
        @Override
        public Image getIcono() {
            return new Image(GestorRecursos.getAtlas(ATLAS_GUI).findRegion("armadura"));
        }
    },

    ACCESORIO {
        @Override
        public Image getIcono() {
            return new Image(GestorRecursos.getAtlas(ATLAS_GUI).findRegion("accesorio"));
        }
    },

    DINERO {
        @Override
        public Image getIcono() {
            return GestorRecursos.nuevaImagen(Constantes.ATLAS_GUI, "oro");
        }
    },

    MATERIA_VERDE {
        @Override
        public Image getIcono() {
            return new Image(GestorRecursos.getAtlas(ATLAS_GUI).findRegion("verde"));
        }
    },

    MATERIA_ROJA {
        @Override
        public Image getIcono() {
            return new Image(GestorRecursos.getAtlas(ATLAS_GUI).findRegion("roja"));
        }
    },

    MATERIA_AZUL {
        @Override
        public Image getIcono() {
            return new Image(GestorRecursos.getAtlas(ATLAS_GUI).findRegion("azul"));
        }
    },

    MATERIA_MORADA {
        @Override
        public Image getIcono() {
            return new Image(GestorRecursos.getAtlas(ATLAS_GUI).findRegion("morada"));
        }
    },

    MATERIA_AMARILLA {
        @Override
        public Image getIcono() {
            return new Image(GestorRecursos.getAtlas(ATLAS_GUI).findRegion("amarilla"));
        }
    };

    /**
     * Recupera el icono del objeto asociado por tipo.
     *
     * @return el icono del objeto
     */
    public abstract Image getIcono();
}

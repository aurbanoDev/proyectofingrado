package com.aurbano.objetos;

import com.aurbano.gestores.GestorRecursos;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

import static com.aurbano.juego.Constantes.ATLAS_GUI;

/**
 * Created by adriu on 13/12/2015.
 */
public class Accesorio extends Equipo {

    public Accesorio(int id, String nombre, String descripcion) {
        super(id, nombre, descripcion, TipoObjeto.ACCESORIO, 0, 0);

        this.icono = new Image(GestorRecursos.getAtlas(ATLAS_GUI).findRegion("accesorio"));
    }
}

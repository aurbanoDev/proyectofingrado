package com.aurbano.objetos;

import static com.aurbano.objetos.TipoObjeto.ARMA_ESPADA;
import static com.aurbano.objetos.TipoObjeto.MATERIA_ROJA;
import static com.aurbano.objetos.TipoObjeto.MATERIA_VERDE;

/**
 * Clase Encargada de crear y manejar los objetos del juego.
 *
 * @author Adrián Urbano
 */
public class RegistroObjetos {

    public static final int POCION = 0, POCION_S = 1, POCION_X = 2;
    public static final int ETER = 3, ETER_TURBO = 4;
    public static final int ELIXIR = 5;
    public static final int COLA_FENIX = 6;
    public static final int CARPA = 7;

    public static final int ARMADURA_ACERO = 8;
    public static final int ARMADURA_CUERO = 9;
    public static final int PETO_CUERO = 10;
    public static final int TUNICA_BLANCA = 11;

    public static final int ANILLO_PLATA = 12;
    public static final int ANILLO_LENN = 13;
    public static final int COLLAR_SELENE = 14;
    public static final int COLLAR_PLATA = 15;

    public static final int ESPADA_ACERO = 16;
    public static final int ESPADA_CEREMONIAL = 17;
    public static final int BASTON_MADERA = 18;
    public static final int LANZA_CORTA = 19;
    public static final int ESPADA_FINAL = 20;

    public static final int RECUPERAR = 21;
    public static final int FUEGO = 22;
    public static final int RAYO = 23;
    public static final int HIELO = 24;
    public static final int AGUA = 25;
    public static final int ODIN = 26;

    public static final int DINERO = 99;


    public static Elemento nuevoElemento(final int id) {
        switch (id) {
            case POCION:
                return new Elemento(POCION, "Pocion", "Restaura 100 puntos de salud a un miembro del grupo");
            case POCION_S:
                return new Elemento(POCION_S, "Pocion S", "Restaura 500 puntos de salud a un miembro del grupo");
            case POCION_X:
                return new Elemento(POCION_X, "Pocion X", "Restaura 9999 puntos de salud a un miembro del grupo");
            case ETER:
                return new Elemento(ETER, "Eter", "Restaura 100 puntos de mana a un miembro del grupo");
            case ETER_TURBO:
                return new Elemento(ETER_TURBO, "Eter Turbo", "Restaura 999 puntos de mana a un miembro del grupo");
            case COLA_FENIX:
                return new Elemento(COLA_FENIX, "Cola Fenix", "Devuelve a la vida a un miembro del grupo caido");
            case ELIXIR:
                return new Elemento(ELIXIR, "Elixir", "Restaura toda la salud y el mana a un miembro del grupo");
            case CARPA:
                return new Elemento(CARPA, "Carpa", "Restaura la salud y el mana de todo el grupo");
            default:
                return null;
        }
    }

    public static Armadura nuevaArmadura(final int id) {
        switch (id) {
            case ARMADURA_ACERO:
                return new Armadura(ARMADURA_ACERO, "Armadura de Acero", "Armadura ligera de Acero", 5, 0, 0, 0, 2);
            case ARMADURA_CUERO:
                return new Armadura(ARMADURA_CUERO, "Armadura de Cuero", "Armadura ligera de Cuero", 4, 1, 0, 0, 1);
            case PETO_CUERO:
                return new Armadura(PETO_CUERO, "Peto de Cuero", "Peto ligero de combate", 4, 1, 0, 0, 1);
            case TUNICA_BLANCA:
                return new Armadura(TUNICA_BLANCA, "Tunica blanca", "Tunica sencilla", 2, 2, 2, 0, 1);
            default:
                return null;
        }
    }

    public static Arma nuevaArma(final int id) {
        switch (id) {
            case ESPADA_ACERO:
                return new Arma(ESPADA_ACERO, "Espada inicial", "Arma inicial", 10, 0, 90, 1, 0, ARMA_ESPADA);
            case ESPADA_CEREMONIAL:
                return new Arma(ESPADA_CEREMONIAL, "Espada ceremonial", "Arma ceremonial", 18, 3, 89, 1, 1, ARMA_ESPADA);
            case BASTON_MADERA:
                return new Arma(BASTON_MADERA, "Baston de Madera", "Arma inicial", 6, 3, 92, 0, 2, TipoObjeto.ARMA_BASTON);
            case LANZA_CORTA:
                return new Arma(LANZA_CORTA, "Lanza corta", "Arma inicial", 7, 1, 91, 1, 0, TipoObjeto.ARMA_LANZA);
            case ESPADA_FINAL:
                return new Arma(ESPADA_FINAL, "Espada Final", "", 49, 40, 100, 4, 0, ARMA_ESPADA);
            default:
                return null;
        }
    }

    public static Accesorio nuevoAccesorio(final int id) {
        switch (id) {
            case ANILLO_PLATA:
                return new Accesorio(ANILLO_PLATA, "Anillo de Plata", "Anillo decorativo");
            case ANILLO_LENN:
                return new Accesorio(ANILLO_LENN, "Anillo de Lenn", "Anillo personal");
            case COLLAR_SELENE:
                return new Accesorio(COLLAR_SELENE, "Collar de Selene", "Collar personal");
            case COLLAR_PLATA:
                return new Accesorio(COLLAR_PLATA, "Collar de Plata", "Collar decorativo");
            default:
                return null;
        }
    }

    public static Materia nuevaMateria(final int id) {
        switch (id) {
            case RECUPERAR:
                return  new Materia(RECUPERAR, "Recuperar", "Sana a un aliado", MATERIA_VERDE);
            case FUEGO:
                return  new Materia(FUEGO, "Fuego", "Impacto por fuego", MATERIA_VERDE);
            case HIELO:
                return  new Materia(HIELO, "Hielo", "Impacto por hielo", MATERIA_VERDE);
            case RAYO:
                return  new Materia(RAYO, "Relampago", "Impacto por electricidad", MATERIA_VERDE);
            case AGUA:
                return new Materia(AGUA, "Agua", "Impacto por agua", MATERIA_VERDE);
            case ODIN:
                return  new Materia(ODIN, "Odin", "Invoca a Odin", MATERIA_ROJA);
            default:
                return null;
        }
    }

    public static Equipo nuevoEquipo(final int id) {
        if (nuevaArma(id) != null)
            return nuevaArma(id);

        if (nuevaArmadura(id) != null)
            return nuevaArmadura(id);

        return nuevoAccesorio(id);
    }
}

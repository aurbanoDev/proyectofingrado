package com.aurbano.objetos;

import com.aurbano.persistencia.EstadoMundo;
import com.badlogic.gdx.utils.Array;

import static com.aurbano.objetos.RegistroObjetos.*;

/**
 * Clase que representa el inventario que comparten los 3 miembros del grupo. Las operaciones de recuperacion y guardado
 * se hacen siémpre a traves de una única instancia obtenida con el metodo: getInstance()
 *
 * @author Adrián Urbano
 */
public class Inventario {

    private int dinero;
    private Array<Equipo> listaEquipo;
    private Array<Materia> listaMaterias;
    private Array<Elemento> listaElementos;
    private Array<Objeto> listaCompleta;

    private static Inventario instance;

    public static Inventario getInstance() {
        if (instance == null)
            instance = new Inventario();

        return instance;
    }

    private Inventario() {
        listaEquipo = new Array<>();
        listaMaterias = new Array<>();
        listaElementos = new Array<>();
        listaCompleta = new Array<>();

        dinero = 1400;
        agregarObjetosIniciales();
    }

    private void agregarObjetosIniciales() {
        Elemento pocion = RegistroObjetos.nuevoElemento(POCION);
        pocion.incrementarCantidad(5);
        Elemento pocionS = RegistroObjetos.nuevoElemento(POCION_S);
        pocionS.incrementarCantidad(3);
        Elemento eter = RegistroObjetos.nuevoElemento(ETER);
        eter.incrementarCantidad(3);
        Elemento eterTurbo = RegistroObjetos.nuevoElemento(ETER_TURBO);
        eterTurbo.incrementarCantidad(2);
        Elemento colaFenix = RegistroObjetos.nuevoElemento(COLA_FENIX);
        colaFenix.incrementarCantidad(2);

        guardarElemento(pocion);
        guardarElemento(pocionS);
        guardarElemento(eter);
        guardarElemento(eterTurbo);
        guardarElemento(colaFenix);

        guardarEquipo(RegistroObjetos.nuevaArma(ESPADA_ACERO));
        guardarEquipo(RegistroObjetos.nuevaArma(ESPADA_CEREMONIAL));
        guardarEquipo(RegistroObjetos.nuevaArma(LANZA_CORTA));
        guardarEquipo(RegistroObjetos.nuevaArma(BASTON_MADERA));

        guardarEquipo(RegistroObjetos.nuevaArmadura(TUNICA_BLANCA));
        guardarEquipo(RegistroObjetos.nuevaArmadura(PETO_CUERO));
        guardarEquipo(RegistroObjetos.nuevaArmadura(ARMADURA_ACERO));
        guardarEquipo(RegistroObjetos.nuevaArmadura(ARMADURA_CUERO));

        guardarEquipo(RegistroObjetos.nuevoAccesorio(ANILLO_PLATA));
        guardarEquipo(RegistroObjetos.nuevoAccesorio(COLLAR_PLATA));
        guardarEquipo(RegistroObjetos.nuevoAccesorio(COLLAR_SELENE));
        guardarEquipo(RegistroObjetos.nuevoAccesorio(ANILLO_LENN));

        guardarMateria(RegistroObjetos.nuevaMateria(FUEGO));
        guardarMateria(RegistroObjetos.nuevaMateria(HIELO));
        guardarMateria(RegistroObjetos.nuevaMateria(AGUA));
        guardarMateria(RegistroObjetos.nuevaMateria(RAYO));
    }

    /**
     * Clasifica y guarda el contenido del array de objetos en el inventario
     *
     * @param contenido ArrayList con los objetos.
     */
    public void guardar(Array<Objeto> contenido) {
        for (Objeto objeto : contenido) {
            if (objeto instanceof Equipo)
                guardarEquipo((Equipo) objeto);

            if (objeto instanceof Elemento)
                guardarElemento((Elemento) objeto);

            if (objeto instanceof Materia)
                guardarMateria((Materia) objeto);

            if (objeto instanceof Dinero)
                aumentarDinero(objeto.cantidad);

        }
    }

    /**
     * Guarda un elemento en el inventario, en caso de que el mismo elemento ya estuviese en el inventario, sumará la
     * cantidad del objeto a guardar.
     *
     * @param nuevoElemento a guardar en el inventario
     */
    public void guardarElemento(Elemento nuevoElemento) {
        for (Elemento elemento : listaElementos) {
            if (elemento.equals(nuevoElemento)) {
                elemento.incrementarCantidad(nuevoElemento.getCantidad());
                return;
            }
        }

        listaElementos.add(nuevoElemento);
        actualizarListaCompleta();
    }

    public void guardarEquipo(Equipo nuevoEquipo) {
        listaEquipo.add(nuevoEquipo);
        actualizarListaCompleta();
    }

    public void guardarMateria(Materia materia) {
        listaMaterias.add(materia);
        actualizarListaCompleta();
    }

    /**
     * Actualiza la lista de objetos completa (incluye elementos, armas, armadura, accesorios , materias...)
     */
    public void actualizarListaCompleta() {
        listaCompleta.clear();
        listaCompleta.addAll(listaElementos);
        listaCompleta.addAll(listaEquipo);
        listaCompleta.addAll(listaMaterias);
    }

    /**
     * Aumenta el dinero dado la cantidad pasada como parametro
     *
     * @param cantidad que sera aumentada
     */
    public void aumentarDinero(int cantidad) {
        dinero += cantidad;
    }

    /**
     * Disminuye el dinero dado la cantidad pasada como parametro.
     *
     * @param cantidad que sera restada
     */
    public void disminuirDinero(int cantidad) {
        if (dinero - cantidad < 0) {
            dinero = 0;
            return;
        }

        dinero -= cantidad;
    }

    /**
     * Ordena la lista de elementos utilizando como criterio el tipo de orden proporcionado como parametro.
     *
     * @param tipoOrden que se utilizara para ordenar
     */
    public void ordenarElementos(TipoOrden tipoOrden) {
        ComparadorObjetos comparador = new ComparadorObjetos();
        comparador.setTipoOrden(tipoOrden);

        actualizarListaCompleta();
        listaCompleta.sort(comparador);
    }

    /**
     * Declara la instancia del singleton como nula, para que con la siguiente llamada a getInstance() se
     * cree un objeto nuevo, con los valores establecidos por defecto.
     */
    public static void restablecer () {
        instance = null;
    }

    /**
     * Restablece el estado del inventario a partir del backup pasado como parametro.
     *
     * @param estado del mundo que se utilizara para restablecer el inventario
     */
    public void restablecer(EstadoMundo estado) {
        dinero = estado.getDinero();
        listaEquipo = estado.getEquipos();
        listaElementos = estado.getElementos();
        listaMaterias = estado.getMaterias();
    }

    /**
     * Devuelve una lista con todos los objetos equipables.
     *
     * @return Arraylist con la lista de equipo.
     */
    public Array<Equipo> getTodoElEquipo() {
        return listaEquipo;
    }

    public Array<Arma> getArmas() {
        Array<Arma> armas = new Array<Arma>();

        for (Equipo equipo : getTodoElEquipo()) {
            if (equipo instanceof Arma && !equipo.isEquipado())
                armas.add((Arma) equipo);
        }
        return armas;
    }

    public Array<Armadura> getArmaduras() {
        Array<Armadura> armaduras = new Array<Armadura>();

        for (Equipo equipo : getTodoElEquipo()) {
            if (equipo instanceof Armadura && !equipo.isEquipado())
                armaduras.add((Armadura) equipo);
        }
        return armaduras;
    }

    public Array<Accesorio> getAccesorios() {
        Array<Accesorio> accesorios = new Array<Accesorio>();

        for (Equipo equipo : getTodoElEquipo()) {
            if (equipo instanceof Accesorio && !equipo.isEquipado())
                accesorios.add((Accesorio) equipo);
        }
        return accesorios;
    }

    /**
     * Recupera una lista con todos los objetos del inventario.
     *
     * @return lista que incluye elementos , equipo y materia.
     */
    public Array<Objeto> getListaCompleta() {
        if (listaCompleta.size == 0)
            actualizarListaCompleta();

        return listaCompleta;
    }

    public Arma getArma(int id) {
        for (Arma arma : getArmas()) {
            if (arma.getId() == id)
                return arma;
        }
        return null;
    }

    public Armadura getArmadura(int id) {
        for (Armadura armadura : getArmaduras()) {
            if (armadura.getId() == id)
                return armadura;
        }
        return null;
    }

    public Accesorio getAccesorio(int id) {
        for (Accesorio accesorio : getAccesorios()) {
            if (accesorio.getId() == id)
                return accesorio;
        }
        return null;
    }


    public int getDinero() {
        return dinero;
    }

    public void setDinero(int dinero) {
        this.dinero = dinero;
    }

    public Array<Materia> getMaterias() {
        return listaMaterias;
    }

    public Array<Elemento> getElementos() {
        return listaElementos;
    }
}

package com.aurbano.objetos;

import com.aurbano.gestores.GestorRecursos;
import com.aurbano.juego.Constantes;

/**
 * Clase que representa un elemento del juego. Los elementos son objetos consumibles , que pueden
 * utilizarse desde el menú de los personajes.
 *
 * @author Adrián Urbano
 */
public class Elemento extends Objeto {

    public static final int CANTIDAD_MAXIMA = 99;

    public Elemento(int id, String nombre, String descripcion) {
        this(id, nombre, descripcion, 1);
    }

    public Elemento(int id, String nombre, String descripcion, int cantidad) {
        super(id, nombre, descripcion, TipoObjeto.ELEMENTO);

        this.cantidad = cantidad;
        this.icono = GestorRecursos.nuevaImagen(Constantes.ATLAS_GUI, "elemento");
    }

    /**
     * Incrementa la cantidad del objeto.
     *
     * @param incremento que se llevara a cabo
     */
    public void incrementarCantidad(final int incremento) {
        if (cantidad + incremento > CANTIDAD_MAXIMA) {
            cantidad = 99;
            return;
        }

        cantidad += incremento;
    }

    /**
     * Decrementa la cantidad del objeto.
     *
     * @param reduccion que se llevara a cabo
     */
    public void reducirCantidad(final int reduccion) {
        if (cantidad - reduccion < 0) {
            cantidad = 0;
            return;
        }

        cantidad -= reduccion;
    }

    /**
     * Indica si el elemento le quedan usos.
     *
     * @return true si la cantidad es 0 , false de otro modo
     */
    public boolean estaAgotado() {
        return cantidad == 0;
    }
}

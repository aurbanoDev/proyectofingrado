package com.aurbano.mapas;

import com.aurbano.entidades.Enemigo;
import com.aurbano.entidades.Entidad;
import com.aurbano.entidades.FactoriaEnemigos;
import com.aurbano.entidades.PuntoGuardado;
import com.aurbano.gestores.GestorRecursos;
import com.aurbano.gestores.GestorSonido;
import com.aurbano.objetos.*;
import com.aurbano.persistencia.EstadoMundo;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapObjects;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.objects.TextureMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer.Cell;
import com.badlogic.gdx.maps.tiled.TiledMapTileSet;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Array;

import java.util.ArrayList;

import static com.aurbano.entidades.Entidad.Estado.MUERTO;
import static com.aurbano.juego.Constantes.*;

/**
 * Clase que gestiona los mapas de Tiled en el juego. Contiene métodos útiles como la detección
 * de celdas de interés en las capas, evaluacion de objetos del mapa, zonas de transición etc..
 *
 * @author Adrián Urbano
 */
public class Mapa {

    /* Indexado de la estructura de las capas en Tiled */
    public static final int COLISIONES = 0;
    public static final int SUELO = 1;
    public static final int OBSTACULO = 2;
    public static final int TECHO = 3;
    public static final int AYUDA = 4;

    private TiledMap mapa;
    private String nombreMapa;
    private Image campoBatalla;
    private PuntoGuardado objetoPuntoGuardado;

    private Array<TextureMapObject> objetosTexturizados;

    /* Referencias utiles para evaluar si el personaje a alcanzado el final de un mapa */
    private Array<RectangleMapObject> objetosTransicion;

    /* Referencias utiles para comprobar si el personaje a alcanzado un evento de dialogo */
    private Array<RectangleMapObject> objetosDialogo;

    /* Identificadores de los identificadoresEnemigos que pueden aparecer en este mapa */
    private Array<Integer> identificadoresEnemigos;

    /* Indica si el mapa actual tiene encuentros con enemigos o no */
    private boolean pacifico;

    public Mapa() {
        identificadoresEnemigos = new Array<>();
        objetosTexturizados = new Array<>();
        objetosTransicion = new Array<>();
        objetosDialogo = new Array<>();
    }

    /**
     * Carga un mapa cotejando por nombre, al concluir la carga se mapearan diversos datos de interes
     * para su utilización posterior, evitando cargar o destruir objetos durante el juego.
     *
     * @param nombreMapa que se intentará cargar
     */
    public void cargarMapa(String nombreMapa) {
        this.nombreMapa = nombreMapa;
        this.mapa = GestorRecursos.getMapa(nombreMapa);

        indexarEnemigos();
        establecerEstadoDelMapa();
        cargarCampoBatalla();
        mapearObjetosTexurizados();
        mapearObjetosDeTransicion();
        mapearObjetosDeGuardado();
        mapearObjetosDialogo();
    }


    /**
     * Carga la imagen que se renderizara de fondo durante los encuentros aleatorios.
     */
    public void cargarCampoBatalla() {
        String propiedad = getCapaColisiones().getProperties().get("batalla", String.class);
        if (propiedad == null)
            return;

        campoBatalla = new Image(new Texture(propiedad));
    }

    /**
     * Lee del mapa las referencias a los IDs de los enemigos que pueden aparecer
     * en el mapa actual y los recoge en una lista. Estos ID_enemigo permitiran
     * automatizar la aparicion de enemigos mediante una factoria.
     */
    public void indexarEnemigos() {
        String propiedad = getCapaColisiones().getProperties().get("enemigos", String.class);
        if (propiedad == null)
            return;

        String[] indices = propiedad.split(" ");

        identificadoresEnemigos.clear();
        for (String indice : indices)
            identificadoresEnemigos.add(Integer.parseInt(indice));
    }

    /**
     * Lee el mapa y establece si el mapa actual es pacifico o de lo contrario
     * es susceptible de encuentros aleatorios ( combates ).
     */
    public void establecerEstadoDelMapa() {
        String propiedad = getCapaColisiones().getProperties().get("pacifico", String.class);
        pacifico = Boolean.valueOf(propiedad);
    }

    /**
     * Mapea todos los objetos del mapa actual que marcan una transicion entre mapas.
     * El campo "destino" contiene una cadena de texto con el nombre del mapa al que transicionara.
     * El campo "posicion" contiene la posicion del mapa en la que aparecerá el jugador.
     * El campo "direccion" contiene la direccion en la que mirará el jugador al cargar el mapa.
     *
     * @return lista con los objetos de transicion.
     */
    private void mapearObjetosDeTransicion() {
        objetosTransicion.clear();
        for (MapObject objeto : getObjetos()) {
            if (!(objeto instanceof RectangleMapObject))
                continue;

            if (objeto.getProperties().get("destino", String.class) == null)
                continue;

            objetosTransicion.add((RectangleMapObject) objeto);
        }
    }

    /**
     * Mapea todos los objetos con textura del mapa. Sirve para tener acceso a
     * los objetos que cuentan con animaciones, como puede ser el caso de un cofre.
     */
    private void mapearObjetosTexurizados() {
        objetosTexturizados.clear();
        for (MapObject objeto : getObjetos()) {
            if (objeto instanceof TextureMapObject)
                objetosTexturizados.add((TextureMapObject) objeto);
        }
    }

    /**
     * Mapea los objetos que representan un punto de salvado para guardar el progreso de la partida.
     * Estos puntos son referenciados a traves de rectangle mapObjects en Tiled (name = puntoGuardado).
     */
    private void mapearObjetosDeGuardado() {

        for (MapObject objeto : getObjetos()) {
            if (!(objeto instanceof RectangleMapObject))
                continue;

            if (objeto.getName().equalsIgnoreCase("puntoGuardado")) {
                Rectangle rectangulo = ((RectangleMapObject) objeto).getRectangle();
                objetoPuntoGuardado = new PuntoGuardado(rectangulo.getX(), rectangulo.getY());
                break;
            }
        }
    }

    /**
     * Mapea los objetos que delimitan el area de un evento de dialogo.
     */
    private void mapearObjetosDialogo() {
        for (MapObject objeto : getObjetos()) {
            if (!(objeto instanceof RectangleMapObject))
                continue;

            if (objeto.getName().equalsIgnoreCase("dialogo"))
                objetosDialogo.add((RectangleMapObject) objeto);
        }
    }

    /**
     * Evalua si la entidad proporcionada como parametro colisiona en el eje horizontal.
     *
     * @param entidad cuyo rectangulo se utilizará para evaluar
     * @return
     */
    public boolean colisionaHorizontal(Entidad entidad) {
        boolean colisionHorizontal = false;

        if (entidad.getVelocidadX() < 0)
            colisionHorizontal = testColisionaIzquierda(entidad);

        if (entidad.getVelocidadX() > 0)
            colisionHorizontal = testColisionaDerecha(entidad);

        return colisionHorizontal;
    }

    /**
     * Evalua si la entidad proporcionada como parametro colisiona en el eje vertical.
     *
     * @param personaje cuyo rectangulo se utilizará para evaluar
     * @return
     */
    public boolean colisionaVertical(Entidad personaje) {
        boolean colisionVertical = false;

        if (personaje.getVelocidadY() < 0)
            colisionVertical = testColisionaAbajo(personaje);

        if (personaje.getVelocidadY() > 0)
            colisionVertical = testColisionaArriba(personaje);

        return colisionVertical;
    }

    private boolean testColisionaDerecha(Entidad entidad) {
        for (float step = 0; step < ALTO_SPRITE / 2; step += LADO_TILE / 2) {
            if (esCeldaColisionable(entidad.getX() + ANCHO_SPRITE, entidad.getY() + step))
                return true;
        }

        return false;
    }

    private boolean testColisionaIzquierda(Entidad entidad) {
        for (float step = 0; step < ALTO_SPRITE / 2; step += LADO_TILE / 2) {
            if (esCeldaColisionable(entidad.getX(), entidad.getY() + step))
                return true;
        }

        return false;

    }

    private boolean testColisionaArriba(Entidad entidad) {
        for (float step = 0; step < ANCHO_SPRITE / 2; step += LADO_TILE / 2) {
            if (esCeldaColisionable((entidad.getX() + 16) + step, entidad.getY() + ANCHO_SPRITE))
                return true;
        }

        return false;

    }

    private boolean testColisionaAbajo(Entidad entidad) {
        for (float step = 0; step < ANCHO_SPRITE / 2; step += LADO_TILE / 2) {
            if (esCeldaColisionable((entidad.getX() + 16) + step, entidad.getY() - 1))
                return true;
        }

        return false;
    }

    private boolean esCeldaColisionable(float x, float y) {
        if (celdaDeInteres(x, y, getCapaColisiones()))
            return true;

        return false;
    }

    private boolean celdaDeInteres(float x, float y, TiledMapTileLayer capa) {
        Cell celda = getCelda(x, y, capa);
        if (celda == null || celda.getTile() == null)
            return false;

        return true;
    }

    private Cell getCelda(float x, float y, TiledMapTileLayer capa) {
        int posicionX = (int) (x / capa.getTileWidth());
        int posicionY = (int) (y / capa.getTileHeight());

        return capa.getCell(posicionX, posicionY);
    }

    /**
     * Devuelve un ArrayList con los Tiles que representan la animacion de un objeto.
     *
     * @param nombreObjeto
     * @return ArrayList con los fotogramas(En Tiles) de la animacion de un objeto.
     */
    private ArrayList<TiledMapTile> getFotogramas(String nombreObjeto) {
        TiledMapTileSet tileset = mapa.getTileSets().getTileSet("generalSet");
        ArrayList<TiledMapTile> fotogramas = new ArrayList<TiledMapTile>();

        for (TiledMapTile tile : tileset) {
            String valor = (String) tile.getProperties().get(nombreObjeto);
            if (valor == null)
                continue;

            fotogramas.add(tile);
        }

        return fotogramas;
    }

    /**
     * Reproduce una vez la secuencia de fotogramas que representa la animación de la recogida del objeto en el mapa.
     * Por ejemplo la apertura de un cofre. El Objeto en Tiled tiene las siguientes propiedades:
     * La propiedad retirar indica si debe desaparecer del mapa despues de recogerlo ( ejemplo pociones o materias)
     *
     * @param objeto del que se quiere mostrar la animación.
     */
    public void animarRecogidaObjeto(TextureMapObject objeto){
        String tipoObjeto = objeto.getName();
        GestorSonido.reproducirSonido(tipoObjeto);

        if (objeto.getProperties().get("retirar") != null) {
            getObjetos().remove(objeto);
            return;
        }

        float tiempoAnimacion = 0;
        ArrayList<TiledMapTile> fotogramas = getFotogramas(objeto.getName());

        for (int i = 0; i < fotogramas.size();) {
            tiempoAnimacion += Gdx.graphics.getDeltaTime();

            if (tiempoAnimacion > 0.5) {
                objeto.setTextureRegion(fotogramas.get(i).getTextureRegion());
                tiempoAnimacion = 0;
                i++;
            }
        }
    }

    /**
     * Devuelve una lista con los objetos que contiene el objeto del mapa(cofres, sacos..)
     * La propiedad recogido indica si ya ha sido recogido o no.
     * La propiedad elemento contiene el id del elemento y la cantidad de elementos separado por una coma.
     *
     * @param objeto del mapa del cual se quiere recuperar el contenido
     * @return lista de objetos contenidos en el objeto del mapa
     */
    public Array<Objeto> recuperarContenido(TextureMapObject objeto) {

        Array<Objeto> contenido = new Array<Objeto>();

        boolean contieneElementos = objeto.getProperties().containsKey("elemento1");
        boolean contieneEquipo = objeto.getProperties().containsKey("equipo");
        boolean contieneMateria = objeto.getProperties().containsKey("materia");
        boolean contieneDinero = objeto.getProperties().containsKey("dinero");

        if (contieneElementos) {
            for (int i = 1; i < 5; i++) {
                String propiedad = (String) objeto.getProperties().get("elemento" + i);
                if (propiedad == null)
                    break;

                String[] datos = propiedad.split(",");
                int id = Integer.parseInt(datos[0]);
                int cantidad = Integer.parseInt(datos[1]);

                Elemento elemento = RegistroObjetos.nuevoElemento(id);
                elemento.incrementarCantidad(cantidad);
                contenido.add(elemento);
            }
        }

        if (contieneEquipo) {
            int id = Integer.parseInt((String) objeto.getProperties().get("equipo"));
            Equipo equipo = RegistroObjetos.nuevoEquipo(id);
            contenido.add(equipo);
        }

        if (contieneMateria) {
            int id = Integer.parseInt((String) objeto.getProperties().get("materia"));
            Materia materia = RegistroObjetos.nuevaMateria(id);
            contenido.add(materia);
        }

        if (contieneDinero) {
            int cantidad = Integer.parseInt((String) objeto.getProperties().get("dinero"));
            Dinero dinero = new Dinero(cantidad);
            contenido.add(dinero);
        }

        objeto.getProperties().put("recogido", "true");
        return contenido;
    }


    /**
     * Recupera todos los objetos con textura que pertenen al mapa proporcionado como parametro.
     *
     * @param tiledMap cuyos objetos seran devueltos.
     * @return lista de TextureMapObjects registrados en el tiledMap
     */
    public static Array<TextureMapObject> getObjetosTexturizados(TiledMap tiledMap) {
        Array<TextureMapObject> lista = new Array<>();
        for (MapObject objeto : tiledMap.getLayers().get("objetos").getObjects()) {
            if (objeto instanceof TextureMapObject)
                lista.add((TextureMapObject) objeto);
        }

        return lista;
    }

    /**
     * Recupera una lista de objetos que hacen las veces de backup, almacenando
     * las propiedades de los objetos del mapa proporcionado como parametro.
     *
     * @param mapa tiled del cual se quieren obtener las referencias.
     * @return lista de referencias con los datos de cada objeto del mapa.
     */
    public static Array<ContenedorObjetos> getContenedores(TiledMap mapa) {
        Array<ContenedorObjetos> lista = new Array<>();
        for (TextureMapObject objeto : getObjetosTexturizados(mapa))
            lista.add(new ContenedorObjetos(objeto));

        return lista;
    }

    /**
     * Evalua si un objeto de tiled map fue recogido y retirado por el jugador.
     *
     * @param objeto que se evaluará
     * @param backups previos de los elementos del mapa para comparar.
     * @return
     */
    public static boolean elementoRetirado(TextureMapObject objeto, Array<ContenedorObjetos> backups) {
        for (ContenedorObjetos backup : backups) {
            if (backup.getId() == objeto.getProperties().get("id", Integer.class))
                return false;
        }

        return true;
    }

    /**
     * Recupera el estado previo de todos los objetos del mapa, restaurando
     * sus propiedades a traves de un backup serializado en un archivo binario.
     *
     * @param estadoMundo que se utilizara para recuperar el estado previo.
     */
    public void restaurarEstadoObjetosDelMapa(EstadoMundo estadoMundo) {
        Array<TextureMapObject> objetos = new Array<>();
        objetos.addAll(getObjetosTexturizados(GestorRecursos.getMapa("ciudadBosque")));
        objetos.addAll(getObjetosTexturizados(GestorRecursos.getMapa("cueva")));
        objetos.addAll(getObjetosTexturizados(GestorRecursos.getMapa("cuevaTesoro")));

        Array<ContenedorObjetos> lista = estadoMundo.getContenedores();
        for (TextureMapObject objectoMapa : objetos) {
            if (elementoRetirado(objectoMapa, lista)) {
                String nombreMapa = objectoMapa.getProperties().get("mapa", String.class);
                GestorRecursos.getMapa(nombreMapa).getLayers().get("objetos").getObjects().remove(objectoMapa);
            }
        }

        for (ContenedorObjetos contenedor : lista) {
            int idContenedor = contenedor.getId();
            for (TextureMapObject tmo : objetos) {
                int idObjeto = tmo.getProperties().get("id", Integer.class);
                if (idObjeto != idContenedor)
                    continue;

                restaurarObjetoDelMapa(tmo, contenedor);
            }
        }
    }

    /**
     * Recupera el estado previo de un objeto del mapa.
     *
     * @param objeto cuyo estado se pondrá al día
     * @param backup que se utiliza para actualizar el objeto del mapa
     * @see ContenedorObjetos
     */
    private void restaurarObjetoDelMapa(TextureMapObject objeto, ContenedorObjetos backup) {
        objeto.getProperties().put("recogido", String.valueOf(backup.isRecogido()));
        if (backup.esCofre() && backup.isRecogido()) {
            ArrayList<TiledMapTile> fotogramas = getFotogramas(objeto.getName());
            TextureRegion cofreAbierto = fotogramas.get(fotogramas.size() - 1).getTextureRegion();
            objeto.setTextureRegion(cofreAbierto);
        }

        if (backup.esSaco()) {
            objeto.getProperties().put("retirar", String.valueOf(backup.isRetirar()));
            GestorRecursos.getMapa(backup.getNombreMapa()).getLayers().get("objetos").getObjects().remove(objeto);
        }
    }

    public MapLayer getCapaObjetos() {
        return  mapa.getLayers().get("objetos");
    }

    public MapObjects getObjetos() {
        return getCapaObjetos().getObjects();
    }

    public TiledMap getMapaTiled() {
        return mapa;
    }

    public TiledMapTileLayer getCapaColisiones() {
        return (TiledMapTileLayer) mapa.getLayers().get(COLISIONES);
    }

    public TiledMapTileLayer getCapaSuelo() {
        return (TiledMapTileLayer) mapa.getLayers().get(SUELO);
    }

    public TiledMapTileLayer getCapaMedia() {
        return (TiledMapTileLayer) mapa.getLayers().get(OBSTACULO);
    }

    public TiledMapTileLayer getCapaAlta() {
        return (TiledMapTileLayer) mapa.getLayers().get(TECHO);
    }

    public TiledMapTileLayer getCapaAyuda() {
        return (TiledMapTileLayer) mapa.getLayers().get(AYUDA);
    }

    public Array<Integer> getIdentificadoresEnemigos() {
        return identificadoresEnemigos;
    }

    public Array<RectangleMapObject> getObjetosTransicion() {
        return objetosTransicion;
    }

    public Array<TextureMapObject> getObjetosTexturizados() {
        return objetosTexturizados;
    }

    public PuntoGuardado getObjetoPuntoGuardado() {
        return objetoPuntoGuardado;
    }

    public boolean esPacifico() {
        return pacifico;
    }

    public String getNombre() {
        return nombreMapa;
    }

    public String getLocalizacion() {
        return getCapaColisiones().getProperties().get("area", String.class);
    }

    public boolean tieneCombateContraJefe() {
        if (!getCapaColisiones().getProperties().containsKey("jefe"))
            return false;

        String propiedad = getCapaColisiones().getProperties().get("jefe", String.class);
        return Boolean.valueOf(propiedad);
    }

    public Rectangle getAreaEventoJefe() {
        RectangleMapObject objetoEventoJefe = null;

        for (MapObject objetoMapa : getObjetos()) {
            if (!(objetoMapa instanceof RectangleMapObject))
                continue;

            if (objetoMapa.getName().equals("jefe"))
                objetoEventoJefe = (RectangleMapObject) objetoMapa;
        }

        return objetoEventoJefe.getRectangle();
    }

    public Enemigo getJefe() {
        RectangleMapObject mapObjectJefe = null;

        for (MapObject objetoMapa : getObjetos()) {
            if (objetoMapa.getName().equals("jefe"))
                mapObjectJefe = (RectangleMapObject) objetoMapa;
        }

        String propiedadID = mapObjectJefe.getProperties().get("idJefe", String.class);
        Enemigo jefe = FactoriaEnemigos.nuevoEnemigo(Integer.parseInt(propiedadID));

        float posicionX = mapObjectJefe.getRectangle().x + 32;
        float posicionY = mapObjectJefe.getRectangle().y + 32;
        jefe.setPosicion(posicionX, posicionY);

        String propiedadMuerto = mapObjectJefe.getProperties().get("muerto", String.class);
        boolean muerto = Boolean.valueOf(propiedadMuerto);

        if (muerto)
            jefe.setEstadoActual(MUERTO);

        return jefe;
    }

    public Rectangle getAreaFinDelCapitulo() {
        for (MapObject objeto : getObjetos()) {
            if (objeto.getName().equalsIgnoreCase("fin"))
                return ((RectangleMapObject)objeto).getRectangle();
        }
        return null;
    }

    public Image getCampoBatalla() {
        return campoBatalla;
    }

    public Array<RectangleMapObject> getObjetosDialogo() {
        return objetosDialogo;
    }
}

package com.aurbano.mapas;

import com.badlogic.gdx.maps.objects.TextureMapObject;

import java.io.Serializable;

/**
 * Clase que representa el estado de un objeto del mapa de Tiled.
 * Recoge la informacion necesaria para que el estado de un objeto en un mapa tiled pueda recuperarse.
 *
 * Created by Adrián Urbano
 */
public class ContenedorObjetos implements Serializable {

    private static final int COFRE = 1;
    private static final int SACO = 2;

    private int id;
    private int tipoObjeto;
    private String nombreMapa;
    private boolean recogido;

    /* Indica si el objeto debe dejar de renderizarse una vez recogido */
    private boolean retirar;


    public ContenedorObjetos(TextureMapObject objeto) {
        id = objeto.getProperties().get("id", Integer.class);
        nombreMapa = objeto.getProperties().get("mapa", String.class);
        recogido = Boolean.valueOf(objeto.getProperties().get("recogido", String.class));

        String retirarProperty = objeto.getProperties().get("retirar", String.class);
        if (retirarProperty == null) {
            tipoObjeto = COFRE;
            return;
        }

        tipoObjeto = SACO;
        retirar = Boolean.valueOf(objeto.getProperties().get("retirar", String.class));
    }

    public int getId() {
        return id;
    }

    public boolean isRecogido() {
        return recogido;
    }

    public boolean isRetirar() {
        return retirar;
    }

    public String getNombreMapa() {
        return nombreMapa;
    }

    public boolean esCofre() {
        return tipoObjeto == COFRE;
    }

    public boolean esSaco() {
        return tipoObjeto == SACO;
    }

    @Override
    public String toString() {
        return "ContenedorObjetos{" +
                "nombreMapa='" + nombreMapa + '\'' +
                ", recogido=" + recogido +
                ", retirar=" + retirar +
                ", tipoObjeto=" + tipoObjeto +
                ", id=" + id +
                '}';
    }
}

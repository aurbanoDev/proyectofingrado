package com.aurbano.mapas;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.objects.TextureMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;

/**
 * Customizacion básica de un MapRenderer, la unica funcionalidad adicional que ofrece es la posibilidad
 * de renderizar los graficos de los mapObjects de Tiled.
 *
 * @author Adrián Urbano
 */
public class RenderizadorMapas extends OrthogonalTiledMapRenderer {

    public RenderizadorMapas(TiledMap map, Batch batch) {
        super(map, batch);
    }

    @Override
    public void renderObjects(MapLayer layer) {
        super.renderObjects(layer);
    }

    @Override
    public void renderObject(MapObject object) {
        if (object == null)
            return;

        if(object instanceof TextureMapObject) {
            TextureMapObject objetoConTextura = (TextureMapObject) object;
            batch.draw(objetoConTextura.getTextureRegion(), objetoConTextura.getX(), objetoConTextura.getY());
        }
    }

    @Override
    public void render() {
       super.render();
    }
}

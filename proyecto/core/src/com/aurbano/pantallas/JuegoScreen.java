package com.aurbano.pantallas;


import com.aurbano.gestores.GestorSonido;
import com.aurbano.juego.Juego;
import com.aurbano.juego.Mundo.EstadoJuego;


public class JuegoScreen extends ScreenAbstracta {

	public JuegoScreen(Juego juego) {
		super(juego);
	}

	@Override
	public void show() {
		super.show();

		GestorSonido.pararMusica("intro");
		GestorSonido.reproducirMusica("ambiente");
        world.setEstadoJuegoActual(EstadoJuego.MAPA);
	}

	@Override
	public void render(float delta) {
		super.render(delta);

        world.actualizar(delta);
        renderizador.renderizar();
        world.acumularTiempoJuego(delta);
    }

	@Override
	public void hide() {
		super.hide();

		GestorSonido.pausarMusica("ambiente");
	}

	@Override
	public void act(float delta) {
		super.act(delta);
    }

}

package com.aurbano.pantallas;

import com.aurbano.juego.Juego;
import com.aurbano.gestores.GestorPantallas;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Container;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

public class IntroScreen extends ScreenAbstracta {

	private  float timer;

	public IntroScreen(Juego juego) {
		super(juego);
	}

	@Override
	public void show() {
		Container<Image> contenedor = new Container<Image>();
		contenedor.setBounds(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

        Texture texture = new Texture(Gdx.files.internal("imagenes/splash.png"));
        Image imagen = new Image(new TextureRegionDrawable(new TextureRegion(texture)));

		contenedor.setActor(imagen);
		addActor(contenedor);
	}

	@Override
	public void render(float delta) {
		act(delta);
		draw();

		timer += delta;
		if (timer > 2 && timer < 2.50)
			GestorPantallas.getInstance().cargar(Pantalla.MENU_PRINCIPAL);
	}

	@Override
	public void dispose() {
		super.dispose();
		GestorPantallas.getInstance().dispose(Pantalla.INTRO);
	}
}

package com.aurbano.pantallas;

import com.aurbano.entidades.Aventurero;
import com.aurbano.gestores.GestorEfectos;
import com.aurbano.gestores.GestorPantallas;
import com.aurbano.gestores.GestorRecursos;
import com.aurbano.gestores.GestorSonido;
import com.aurbano.gestores.controles.GestorControles;
import com.aurbano.interfazUsuario.menus.MenuResumen;
import com.aurbano.interfazUsuario.menus.MenuResumen.FaseMenuResumen;
import com.aurbano.juego.Juego;
import com.aurbano.objetos.Inventario;

/**
 * Created by adriu on 29/12/2015.
 */
public class MenuResumenScreen extends ScreenAbstracta {

    private MenuResumen menuResumen;


    public MenuResumenScreen(Juego juego) {
        super(juego);

        menuResumen = world.getMenuResumen();
    }

    @Override
    public void show() {
        super.show();
        GestorSonido.reproducirMusica("ambiente");

        menuResumen.setFaseActual(FaseMenuResumen.SELECCION_SECCION);
        menuResumen.personajes.cargarAventurerosConBarraPX(world.getGrupo());
        menuResumen.tablaPie.mostrarAyuda(world.mapaActual.getLocalizacion());
        menuResumen.tablaTiempoOro.actualizarTiempoJugado(world.getTiempoJuego());
        menuResumen.tablaTiempoOro.actualizarOro(Inventario.getInstance().getDinero());

        addActor(menuResumen.personajes);
        addActor(menuResumen.tablaPie);
        addActor(menuResumen.tablaSecciones);
        addActor(menuResumen.tablaTiempoOro);
    }

    @Override
    public void render(float delta) {
        super.render(delta);

        act(delta);
        draw();
    }

    @Override
    public void act(float delta) {
        super.act(delta);

        switch (menuResumen.faseActual) {
            case SELECCION_SECCION:
                actualizarSeleccionSeccion();
                break;
            case SELECCION_AVENTURERO:
                actualizarSeleccionAventurero();
                break;
        }

        world.acumularTiempoJuego(delta);
        menuResumen.tablaTiempoOro.actualizarTiempoJugado(world.getTiempoJuego());
    }

    private void actualizarSeleccionSeccion() {
        GestorControles controles = GestorControles.getInstance();

        if (controles.siguiente())
            menuResumen.tablaSecciones.celdaSiguiente();

        if (controles.anterior())
            menuResumen.tablaSecciones.celdaAnterior();

        if (controles.pulsacionConfirmar())
            actualizarAperturaSeccion();

        if (controles.pulsacionCancelar()) {
            transicionar(Pantalla.JUEGO);
            GestorSonido.reproducirSonido("atras");
        }
    }

    private void actualizarAperturaSeccion() {
        String opcion = menuResumen.tablaSecciones.getContenidoActual();

        if (opcion.equalsIgnoreCase("Elementos"))
            GestorPantallas.getInstance().cargar(Pantalla.MENU_ELEMENTOS);

        if (opcion.equalsIgnoreCase("Equipo") || opcion.equalsIgnoreCase("Estado"))  {
            menuResumen.personajes.seleccionarPrimeraCelda();
            menuResumen.setFaseActual(FaseMenuResumen.SELECCION_AVENTURERO);
        }

        if (opcion.equalsIgnoreCase("Guardar"))
            abrirMenuSalvar();

        if (opcion.equalsIgnoreCase("Opciones"))
            GestorPantallas.getInstance().cargar(Pantalla.OPCIONES);

        if (opcion.equalsIgnoreCase("Salir")) {
            GestorSonido.pararMusica("ambiente");
            GestorRecursos.restablecerMapasPorDefecto();
            GestorPantallas.getInstance().dispose();
            GestorPantallas.getInstance().cargar(Pantalla.MENU_PRINCIPAL);
        }

        if (opcion.equalsIgnoreCase("Magia"))
            GestorSonido.reproducirSonido("error");

        if (opcion.equalsIgnoreCase("Materia"))
            GestorSonido.reproducirSonido("error");

    }

    private void actualizarSeleccionAventurero() {
        if (controles.siguiente())
            menuResumen.personajes.celdaSiguiente();

        if (controles.anterior())
            menuResumen.personajes.celdaAnterior();

        if (controles.pulsacionCancelar()) {
            menuResumen.personajes.limpiarSeleccion();
            menuResumen.setFaseActual(FaseMenuResumen.SELECCION_SECCION);
        }

        if (controles.pulsacionConfirmar()) {
            //TODO hacer mas secciones
            String opcion = menuResumen.tablaSecciones.getContenidoActual();
            if (opcion.equalsIgnoreCase("Equipo"))
                abrirMenuEquipo();

            if (opcion.equalsIgnoreCase("Estado"))
                abrirMenuEstado();
        }
    }

    private void abrirMenuSalvar() {
        if (!menuResumen.estaGuardadoActivado()) {
            GestorSonido.reproducirSonido("error");
            return;
        }

        world.menuSalvarCargarPartida.setSalvando(true);
        GestorPantallas.getInstance().cargar(Pantalla.SALVAR_GUARDAR_PARTIDA);
    }

    private void abrirMenuEquipo() {
        Aventurero seleccionado = menuResumen.personajes.getContenidoActual();
        world.getMenuEquipo().cargarDatos(seleccionado);
        GestorPantallas.getInstance().cargar(Pantalla.MENU_EQUIPO);
    }

    private void abrirMenuEstado() {
        Aventurero seleccionado = menuResumen.personajes.getContenidoActual();
        world.getMenuEstado().cargarDatos(seleccionado);
        GestorPantallas.getInstance().cargar(Pantalla.MENU_ESTADO);
    }

    @Override
    public void transicionar(final Pantalla pantalla) {
        GestorEfectos.interpolacionSalida(menuResumen);
        encolarAccion(nuevaTransicion(pantalla));
        menuResumen.setFaseActual(FaseMenuResumen.SELECCION_SECCION);
    }
}

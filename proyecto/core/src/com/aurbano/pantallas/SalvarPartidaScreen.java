package com.aurbano.pantallas;

import com.aurbano.interfazUsuario.menus.MenuSalvarPartida;
import com.aurbano.juego.Juego;

/**
 * Created by infe on 06/05/2016.
 */
public class SalvarPartidaScreen extends ScreenAbstracta {

    MenuSalvarPartida menuSalvarPartida;

    public SalvarPartidaScreen(Juego juego) {
        super(juego);

        menuSalvarPartida = world.getMenuSalvarCargarPartida();
    }

    @Override
    public void show() {
        super.show();

        clear();
        addActor(menuSalvarPartida);
        addActor(menuSalvarPartida.getTablaConfirmar());
        menuSalvarPartida.cargarRanurasDisponibles();
        recuperarAlfaGradualmente(0.6f);
    }

    @Override
    public void render(float delta) {
        super.render(delta);

        act(delta);
        draw();
    }

    @Override
    public void act(float delta) {
        super.act(delta);

        menuSalvarPartida.actualizar();
    }

    @Override
    public void hide() {
        super.hide();
    }
}

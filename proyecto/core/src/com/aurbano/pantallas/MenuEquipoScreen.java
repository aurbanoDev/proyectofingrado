package com.aurbano.pantallas;

import com.aurbano.interfazUsuario.menus.MenuEquipo;
import com.aurbano.juego.Juego;

/**
 * Created by adriu on 28/12/2015.
 */
public class MenuEquipoScreen extends ScreenAbstracta {

    private MenuEquipo menuEquipo;

    public MenuEquipoScreen(Juego juego) {
        super(juego);

        menuEquipo = world.getMenuEquipo();
    }

    @Override
    public void show() {
        super.show();

        addActor(menuEquipo);
        addActor(menuEquipo.titulo);
    }

    @Override
    public void render(float delta) {
        super.render(delta);

        act(delta);
        draw();
    }

    @Override
    public void act(float delta) {
        super.act(delta);

        world.acumularTiempoJuego(delta);
        menuEquipo.actualizar();
    }
}

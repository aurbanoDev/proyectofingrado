package com.aurbano.pantallas;


import com.aurbano.gestores.GestorPantallas;
import com.aurbano.gestores.GestorSonido;
import com.aurbano.interfazUsuario.menus.MenuPrincipal;
import com.aurbano.juego.Juego;
import com.aurbano.persistencia.GestorPersistencia;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;

public class MenuPrincipalScreen extends ScreenAbstracta {

	private MenuPrincipal menuPrincipal;

	public MenuPrincipalScreen(Juego juego) {
		super(juego);

		menuPrincipal = new MenuPrincipal();
	}

	@Override
	public void show() {
		super.show();

		GestorSonido.reanudarGradualmente("intro", 1, 1);
		addActor(menuPrincipal);
		recuperarAlfaGradualmente(1f);
	}

	@Override
	public void render(float delta) {
		super.render(delta);

        act(delta);
		draw();
	}

	@Override
	public void act(float delta) {
		super.act(delta);

        if (controles.pulsacionAbajo())
            menuPrincipal.getSeleccion().celdaSiguiente();

        if (controles.pulsacionArriba())
            menuPrincipal.getSeleccion().celdaAnterior();

        if (controles.pulsacionConfirmar())
            confirmar();
    }

    @Override
    public void transicionar(Pantalla pantalla) {

    }

    @Override
    public void hide() {
		super.hide();

		getRoot().addAction(Actions.fadeOut(1));
	}

	@Override
	public void dispose() {
		GestorPantallas.getInstance().dispose(Pantalla.MENU_PRINCIPAL);
	}

	private void confirmar() {
		int indice = menuPrincipal.getSeleccion().getIndiceCeldaSeleccionada();

		if (indice == 0) {
            juego.inicializarNucleo();
            GestorSonido.reproducirSonido("confirmar");
			GestorSonido.reproducirMusica("ambiente", 70);
			GestorPantallas.getInstance().cargar(Pantalla.JUEGO);
		}

		if (indice == 1) {
			GestorPersistencia persistencia = GestorPersistencia.getInstance();
			if (!persistencia.hayPartidasGuardadas()) {
				GestorSonido.reproducirSonido("error");
				return;
			}

			world.menuSalvarCargarPartida.setSalvando(false);
			GestorPantallas.getInstance().cargar(Pantalla.SALVAR_GUARDAR_PARTIDA);
		}

		if (indice == 2)
			Gdx.app.exit();
	}

}

package com.aurbano.pantallas;

import com.aurbano.entidades.Aventurero;
import com.aurbano.gestores.GestorPantallas;
import com.aurbano.gestores.GestorSonido;
import com.aurbano.interfazUsuario.celdas.CeldaAbstracta;
import com.aurbano.interfazUsuario.celdas.CeldaObjeto;
import com.aurbano.interfazUsuario.menus.MenuElementos;
import com.aurbano.interfazUsuario.menus.MenuElementos.FaseMenuElementos;
import com.aurbano.juego.Juego;
import com.aurbano.objetos.*;

import static com.aurbano.objetos.RegistroObjetos.*;

/**
 * Created by adriu on 28/12/2015.
 */
public class MenuElementosScreen extends ScreenAbstracta {

    private MenuElementos menuElementos;

    public MenuElementosScreen(Juego juego) {
        super(juego);

        menuElementos = world.getMenuElementos();
    }

    @Override
    public void show() {
        super.show();

        menuElementos.cargarMenu(world.getGrupo());
        addActor(menuElementos);
        addActor(menuElementos.tablaTiposOrden);
    }

    @Override
    public void render(float delta) {
        super.render(delta);

        act(delta);
        draw();
    }

    @Override
    public void act(float delta) {
        super.act(delta);

        switch (menuElementos.faseActual) {
            case CABECERA:
                actualizarCabecera();
                break;
            case USAR:
                actualizarUsar();
                break;
            case ELEGIR_OBJETIVO:
                actualizarSeleccionObjetivo();
                break;
            case ORDENAR:
                actualizarMenuOrdenarElementos();
                break;
            case SALIR:
                GestorPantallas.getInstance().cargar(Pantalla.MENU_RESUMEN);
                GestorSonido.reproducirSonido("atras");
                break;
        }

        world.acumularTiempoJuego(delta);
    }

    private void actualizarCabecera() {

        if (controles.siguiente())
            menuElementos.cabecera.celdaSiguiente();

        if (controles.anterior())
            menuElementos.cabecera.celdaAnterior();

        if (controles.pulsacionConfirmar())
            abrirMenuSeleccion();

        if (controles.pulsacionCancelar())
            menuElementos.setFaseActual(FaseMenuElementos.SALIR);
    }

    private void abrirMenuSeleccion() {
        String accion = menuElementos.cabecera.getContenidoActual();

        if (accion.equalsIgnoreCase("Usar")) {
            if (!world.estaGuardadoActivado()) {
                for (CeldaAbstracta celda : menuElementos.tablaObjetos.getCeldas()) {
                    if (((CeldaObjeto) celda).getContenido().getId() == CARPA) {
                        ((CeldaObjeto) celda).setEstiloActivado(false);
                        celda.refrescar();
                    }
                }
            }
            menuElementos.mostrarDescripcion();
            menuElementos.cabecera.limpiarSeleccion();
            menuElementos.tablaObjetos.seleccionarPrimeraCelda();
            menuElementos.setFaseActual(FaseMenuElementos.USAR);
        }

        if (accion.equalsIgnoreCase("Ordenar")) {
            menuElementos.tablaTiposOrden.mostrar();
            menuElementos.cabecera.limpiarSeleccion();
            menuElementos.setFaseActual(FaseMenuElementos.ORDENAR);
        }

        if (accion.equalsIgnoreCase("Objetos Clave")) {

        }
    }

    private void actualizarMenuOrdenarElementos() {

        if (controles.siguiente())
            menuElementos.tablaTiposOrden.celdaSiguiente();

        if (controles.anterior())
            menuElementos.tablaTiposOrden.celdaAnterior();

        if (controles.pulsacionCancelar()) {
            menuElementos.tablaTiposOrden.ocultar();
            menuElementos.cabecera.seleccionarPrimeraCelda();
            menuElementos.setFaseActual(FaseMenuElementos.CABECERA);
        }

        if (controles.pulsacionConfirmar()) {

            if (menuElementos.tablaTiposOrden.seleccionadaOpcion("Tipo"))
                menuElementos.tablaObjetos.ordenarObjetos(TipoOrden.TIPO_OBJETO);

            if (menuElementos.tablaTiposOrden.seleccionadaOpcion("Nombre"))
                menuElementos.tablaObjetos.ordenarObjetos(TipoOrden.NOMBRE);

            if (menuElementos.tablaTiposOrden.seleccionadaOpcion("Cantidad"))
                menuElementos.tablaObjetos.ordenarObjetos(TipoOrden.CANTIDAD);

            if (menuElementos.tablaTiposOrden.seleccionadaOpcion("Batalla"))
                menuElementos.tablaObjetos.ordenarObjetos(TipoOrden.BATALLA);

            menuElementos.tablaTiposOrden.ocultar();
            menuElementos.cabecera.seleccionar(0);
            menuElementos.setFaseActual(FaseMenuElementos.CABECERA);
        }
    }

    private void actualizarUsar() {

        if (controles.siguiente())
            menuElementos.siguienteObjeto();

        if (controles.anterior())
            menuElementos.anteriorObjeto();

        if (controles.pulsacionConfirmar())
            seleccionarObjetivo();

        if (controles.pulsacionCancelar()) {
            menuElementos.tablaAyuda.mostrarAyuda("");
            menuElementos.tablaObjetos.limpiarSeleccion();
            menuElementos.cabecera.seleccionarPrimeraCelda();
            menuElementos.setFaseActual(FaseMenuElementos.CABECERA);
        }
    }

    private void actualizarSeleccionObjetivo() {
        if (controles.siguiente())
            menuElementos.tablaPersonajes.celdaSiguiente();

        if (controles.anterior())
            menuElementos.tablaPersonajes.celdaAnterior();

        if (controles.pulsacionCancelar()) {
            menuElementos.tablaPersonajes.limpiarSeleccion();
            menuElementos.setFaseActual(FaseMenuElementos.USAR);
            GestorSonido.reproducirSonido("atras");
        }

        if (controles.pulsacionConfirmar())
            utilizarObjeto();
    }

    private void seleccionarObjetivo() {
        Objeto seleccionado = menuElementos.tablaObjetos.getContenidoActual();
        if (!(seleccionado instanceof Elemento)) {
            GestorSonido.reproducirSonido("error");
            return;
        }

        menuElementos.tablaPersonajes.seleccionarPrimeraCelda();
        menuElementos.setFaseActual(FaseMenuElementos.ELEGIR_OBJETIVO);
    }

    private void utilizarObjeto() {
        Aventurero objetivo = menuElementos.tablaPersonajes.getContenidoActual();
        Elemento elementoSeleccionado = (Elemento) menuElementos.tablaObjetos.getContenidoActual();
        if (elementoSeleccionado.getCantidad() == 0) {
            GestorSonido.reproducirSonido("error");
            return;
        }

        int identificador = elementoSeleccionado.getId();
        switch (identificador) {
            case CARPA:
                if (!juego.world.estaGuardadoActivado()) {
                    GestorSonido.reproducirSonido("error");
                    return;
                }
                break;
            case POCION:
                if (objetivo.getVida().getPorcentaje() == 100) {
                    GestorSonido.reproducirSonido("error");
                    return;
                }
                objetivo.getVida().sumar(100);
                break;
            case POCION_S:
                if (objetivo.getVida().getPorcentaje() == 100) {
                    GestorSonido.reproducirSonido("error");
                    return;
                }
                objetivo.getVida().sumar(500);
                break;
            case POCION_X:
                if (objetivo.getVida().getPorcentaje() == 100) {
                    GestorSonido.reproducirSonido("error");
                    return;
                }
                objetivo.getVida().sumar(1000);
                break;
            case ETER:
                if (objetivo.getMana().getPorcentaje() == 100) {
                    GestorSonido.reproducirSonido("error");
                    return;
                }
                objetivo.getMana().sumar(50);
                break;
            case ETER_TURBO:
                if (objetivo.getMana().getPorcentaje() == 100) {
                    GestorSonido.reproducirSonido("error");
                    return;
                }
                objetivo.getMana().sumar(150);
                break;
        }

        elementoSeleccionado.reducirCantidad(1);
        menuElementos.tablaPersonajes.cargarContenido(world.getGrupo());
        menuElementos.tablaObjetos.cargarContenido(Inventario.getInstance().getListaCompleta());
        menuElementos.tablaObjetos.recuperarUltimaSeleccion();
        menuElementos.setFaseActual(FaseMenuElementos.USAR);
        GestorSonido.reproducirSonido("saco");
    }

}
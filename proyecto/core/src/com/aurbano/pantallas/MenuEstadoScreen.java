package com.aurbano.pantallas;

import com.aurbano.gestores.GestorPantallas;
import com.aurbano.gestores.GestorSonido;
import com.aurbano.interfazUsuario.menus.MenuEstado;
import com.aurbano.juego.Juego;

/**
 * Created by adriu on 08/01/2016.
 */
public class MenuEstadoScreen extends ScreenAbstracta {

    MenuEstado menuEstado;

    public MenuEstadoScreen(Juego juego) {
        super(juego);

        menuEstado = world.getMenuEstado();
    }

    @Override
    public void show() {
        super.show();
        clear();

        addActor(menuEstado);
        addActor(menuEstado.titulo);
    }

    @Override
    public void render(float delta) {
        super.render(delta);

        act(delta);
        draw();
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        world.acumularTiempoJuego(delta);

        if (controles.pulsacionCancelar()) {
            GestorPantallas.getInstance().cargar(Pantalla.MENU_RESUMEN);
            GestorSonido.reproducirSonido("atras");
        }
    }
}

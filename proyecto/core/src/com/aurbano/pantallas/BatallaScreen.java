package com.aurbano.pantallas;

import com.aurbano.gestores.GestorSonido;
import com.aurbano.interfazUsuario.menus.MenuBatalla;
import com.aurbano.interfazUsuario.menus.MenuBatalla.FaseMenuBatalla;
import com.aurbano.juego.Juego;
import com.aurbano.juego.Mundo.EstadoJuego;

/**
 * Created by adriu on 19/01/2016.
 */
public class BatallaScreen extends ScreenAbstracta {

    private MenuBatalla menuBatalla;

    public BatallaScreen(Juego juego) {
        super(juego);

        menuBatalla = world.getMenuBatalla();
    }

    @Override
    public void show() {
        super.show();

        if (menuBatalla.esEncuentroAleatorio())
            GestorSonido.reanudarGradualmente("combate", 4, 0.5f);
        else
            GestorSonido.reanudarGradualmente("combateJefe", 4, 0.5f);

        menuBatalla.setTurnoResolviendose(false);
        menuBatalla.getCabeceraAyuda().setVisible(false);
        world.setEstadoJuegoActual(EstadoJuego.BATALLA);
        menuBatalla.setFaseActual(FaseMenuBatalla.SELECCION);

        clear();
        addActor(menuBatalla);
        addActor(menuBatalla.getTablaAcciones());
        addActor(menuBatalla.getTablaElementos());
        addActor(menuBatalla.getTablaMagias());
        addActor(menuBatalla.getCursorEnemigos());
        addActor(menuBatalla.getCursorAliados());
        addActor(menuBatalla.getCabeceraAyuda());
    }

    @Override
    public void render(float delta) {
        super.render(delta);

        world.actualizar(delta);
        renderizador.renderizar();

        act(delta);
        draw();
    }

    @Override
    public void act(float delta) {
        super.act(delta);
    }

}

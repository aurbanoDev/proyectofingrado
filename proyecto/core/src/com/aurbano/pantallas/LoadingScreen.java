package com.aurbano.pantallas;

import com.aurbano.gestores.GestorRecursos;
import com.aurbano.gestores.GestorSonido;
import com.aurbano.juego.Juego;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.assets.loaders.SkinLoader;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;


public class LoadingScreen extends ScreenAbstracta {

    private Stage stage;
    private Image logo;
    private Image loadingFrame;
    private Image loadingBarHidden;
    private Image screenBg;
    private Image loadingBg;

    private float startX, endX;
    private float percent;

    private Actor loadingBar;

    public LoadingScreen(Juego juego) {
        super(juego);
    }

    @Override
    public void show() {

        stage = new Stage();
        juego.manager.load("atlas/loading.pack", TextureAtlas.class);
        juego.manager.finishLoading();

        TextureAtlas atlas = juego.manager.get("atlas/loading.pack", TextureAtlas.class);

        logo = new Image(atlas.findRegion("libgdx-logo"));
        loadingFrame = new Image(atlas.findRegion("loading-frame"));
        loadingBarHidden = new Image(atlas.findRegion("loading-bar-hidden"));
        screenBg = new Image(atlas.findRegion("screen-bg"));
        loadingBg = new Image(atlas.findRegion("loading-frame-bg"));

        Animation anim = new Animation(0.05f, atlas.findRegions("loading-bar-anim") );
        anim.setPlayMode(Animation.PlayMode.LOOP_REVERSED);
        loadingBar = new LoadingBar(anim);

        stage.addActor(screenBg);
        stage.addActor(loadingBar);
        stage.addActor(loadingBg);
        stage.addActor(loadingBarHidden);
        stage.addActor(loadingFrame);
        stage.addActor(logo);

        juego.manager.load("atlas/gui.pack", TextureAtlas.class);
        juego.manager.load("atlas/aventureros.pack", TextureAtlas.class);
        juego.manager.load("atlas/enemigos.pack", TextureAtlas.class);
        juego.manager.load("atlas/puntoGuardado.pack", TextureAtlas.class);
        juego.manager.load("atlas/item.pack", TextureAtlas.class);
        juego.manager.load("atlas/efectos.pack", TextureAtlas.class);
        juego.manager.load("atlas/guardia.pack", TextureAtlas.class);

        juego.manager.load("audios/saco.ogg", Sound.class);
        juego.manager.load("audios/error.ogg", Sound.class);
        juego.manager.load("audios/atras.ogg", Sound.class);
        juego.manager.load("audios/cofre.ogg", Sound.class);
        juego.manager.load("audios/equipar.ogg", Sound.class);
        juego.manager.load("audios/hachazo.ogg", Sound.class);
        juego.manager.load("audios/espadazo.ogg", Sound.class);
        juego.manager.load("audios/lanzazo.ogg", Sound.class);
        juego.manager.load("audios/elemento.ogg", Sound.class);
        juego.manager.load("audios/mazazo.ogg", Sound.class);
        juego.manager.load("audios/usarElementoBatalla.ogg", Sound.class);
        juego.manager.load("audios/salvarCargar.ogg", Sound.class);
        juego.manager.load("audios/saveActivado.ogg", Sound.class);
        juego.manager.load("audios/turnoPreparado.ogg", Sound.class);
        juego.manager.load("audios/desplazarCursor.ogg", Sound.class);

        juego.manager.load("audios/fuego.ogg", Sound.class);
        juego.manager.load("audios/rayo.ogg", Sound.class);
        juego.manager.load("audios/hielo.ogg", Sound.class);
        juego.manager.load("audios/agua.ogg", Sound.class);
        juego.manager.load("audios/encuentro.ogg", Sound.class);

        juego.manager.load("audios/intro.mp3", Music.class);
        juego.manager.load("audios/ambiente.mp3", Music.class);
        juego.manager.load("audios/combate.mp3", Music.class);
        juego.manager.load("audios/combateJefe.mp3", Music.class);

        juego.manager.load("skin/uiskin.json", Skin.class, new SkinLoader.SkinParameter("uiskin.atlas"));

        juego.manager.setLoader(TiledMap.class, new TmxMapLoader(new InternalFileHandleResolver()));
        juego.manager.load("mapas/ciudadBosque.tmx", TiledMap.class);
        juego.manager.load("mapas/cueva.tmx", TiledMap.class);
        juego.manager.load("mapas/cuevaTesoro.tmx", TiledMap.class);
    }

    @Override
    public void render(float delta) {
        super.render(delta);

        if (juego.manager.update())
            iniciarJuego();

        percent = Interpolation.linear.apply(percent, juego.manager.getProgress(), 0.1f);
        loadingBarHidden.setX(startX + endX * percent);
        loadingBg.setX(loadingBarHidden.getX() + 30);
        loadingBg.setWidth(450 - 450 * percent);
        loadingBg.invalidate();

        stage.act();
        stage.draw();
    }

    /**
     * Mapea los recursos cargados y arranca el menu principal del juego.
     * El metodo se lanzara en cuanto el usuario haga cualquier tipo de input de raton o teclado.
     */
    private void iniciarJuego() {
        if (!Gdx.input.isTouched() && !Gdx.input.isKeyJustPressed(Input.Keys.ANY_KEY))
            return;

        GestorRecursos.mapearRecursos(juego.manager);
        GestorSonido.mapearSonidos(juego.manager);

        juego.inicializarNucleo();
        juego.setScreen(new MenuPrincipalScreen(juego));
    }

    @Override
    public void resize(int width, int height) {

        stage.getViewport().update(width , height, false);

        screenBg.setSize(width, height);

        logo.setX((width - logo.getWidth()) / 2);
        logo.setY((height - logo.getHeight()) / 2 + 100);

        loadingFrame.setX((stage.getWidth() - loadingFrame.getWidth()) / 2);
        loadingFrame.setY((stage.getHeight() - loadingFrame.getHeight()) / 2);

        loadingBar.setX(loadingFrame.getX() + 15);
        loadingBar.setY(loadingFrame.getY() + 5);

        loadingBarHidden.setX(loadingBar.getX() + 35);
        loadingBarHidden.setY(loadingBar.getY() - 3);

        startX = loadingBarHidden.getX();
        endX = 440;

        loadingBg.setSize(450, 50);
        loadingBg.setX(loadingBarHidden.getX() + 30);
        loadingBg.setY(loadingBarHidden.getY() + 3);
    }

    @Override
    public void hide() {
        // Al salir de la pantalla de carga descargamos todos los recursos graficos de la pantalla de carga.
        juego.manager.unload("atlas/loading.pack");
    }
}

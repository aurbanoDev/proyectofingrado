package com.aurbano.pantallas;

import com.aurbano.juego.Juego;

/**
 * Enumeración que clasifica los tipos de pantallas del juego.
 *
 * @author Adrián Urbano
 */
public enum Pantalla {

    INTRO {
        @Override
        public ScreenAbstracta getInstance(Juego juego) {
            return new IntroScreen(juego);
        }
    },

    CARGA {
        @Override
        public ScreenAbstracta getInstance(Juego juego) {
            return new LoadingScreen(juego);
        }
    },

    MENU_PRINCIPAL {
        @Override
        public ScreenAbstracta getInstance(Juego juego) {
            return new MenuPrincipalScreen(juego);
        }
    },

    MENU_RESUMEN {
        @Override
        public ScreenAbstracta getInstance(Juego juego) {
            return new MenuResumenScreen(juego);
        }

    },

    MENU_ELEMENTOS {
        @Override
        public ScreenAbstracta getInstance(Juego juego) {
            return new MenuElementosScreen(juego);
        }

    },

    MENU_EQUIPO {
        @Override
        public ScreenAbstracta getInstance(Juego juego) {
            return new MenuEquipoScreen(juego);
        }

    },

    MENU_ESTADO {
        @Override
        public ScreenAbstracta getInstance(Juego juego) {
            return new MenuEstadoScreen(juego);
        }

    },

    BATALLA {
        @Override
        public ScreenAbstracta getInstance(Juego juego) {
            return new BatallaScreen(juego);
        }

    },

    RECOMPENSA {
        @Override
        public ScreenAbstracta getInstance(Juego juego) {
            return new RecomensaScreen(juego);
        }
    },

    SALVAR_GUARDAR_PARTIDA {
        @Override
        public ScreenAbstracta getInstance(Juego juego) {
            return new SalvarPartidaScreen(juego);
        }
    },

    OPCIONES {
        @Override
        public ScreenAbstracta getInstance(Juego juego) {
            return new OpcionesScreen(juego);
        }
    },

    JUEGO {
        @Override
        public ScreenAbstracta getInstance(Juego juego) {
            return new JuegoScreen(juego);
        }
    };

    /**
     * Método para crear una instancia de la screen de juego a la que hace alusión.
     *
     * @param juego
     * @return
     */
    public abstract ScreenAbstracta getInstance(Juego juego);
}
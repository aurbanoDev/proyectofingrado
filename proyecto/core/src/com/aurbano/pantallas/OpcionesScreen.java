package com.aurbano.pantallas;

import com.aurbano.interfazUsuario.menus.MenuOpciones;
import com.aurbano.juego.Juego;

/**
 * Created by infe on 12/05/2016.
 */
public class OpcionesScreen extends ScreenAbstracta {

    private MenuOpciones opciones;

    public OpcionesScreen(Juego juego) {
        super(juego);
        opciones = new MenuOpciones();
    }

    @Override
    public void show() {
        super.show();

        clear();
        opciones.getOpciones().seleccionarPrimeraCelda();
        opciones.actualizarCabecera();
        opciones.actualizarPreferencias();
        addActor(opciones);
    }

    @Override
    public void render(float delta) {
        super.render(delta);

        act(delta);
        draw();
    }

    @Override
    public void act(float delta) {
        super.act(delta);

        opciones.actualizar();
    }
}

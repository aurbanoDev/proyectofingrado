package com.aurbano.pantallas;

import com.aurbano.gestores.GestorPantallas;
import com.aurbano.gestores.controles.GestorControles;
import com.aurbano.juego.Juego;
import com.aurbano.juego.Renderizador;
import com.aurbano.juego.Mundo;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.utils.viewport.FitViewport;

/**
 * Clase abstracta que recoge la funcionalidad de una screen y un Stage.
 * De esta clase extienden todas las screens de juego.
 *
 * @author Adrián Urbano
 */
public abstract class ScreenAbstracta extends Stage implements Screen {

	protected Mundo world;
    protected SpriteBatch batch;
    protected OrthographicCamera camara;
    protected GestorControles controles;
    protected Renderizador renderizador;

	protected Juego juego;

    public ScreenAbstracta(Juego juego) {
        super(new FitViewport(1280, 736), juego.batch);

		this.juego = juego;
        this.batch = juego.batch;
        this.world = juego.world;
        this.camara = juego.camara;
		this.renderizador = juego.renderizador;
		this.controles = GestorControles.getInstance();
    }

	@Override
	public void show() {
		/* Al mostrar la pantalla activa el capturador de eventos del teclado */
		Gdx.input.setInputProcessor(controles);
	}

	@Override
	public void render(float delta) {
		Gdx.gl20.glClearColor(0, 0, 0, 1);
		Gdx.gl20.glClear(GL20.GL_COLOR_BUFFER_BIT);
	}

	@Override
	public void resize(int width, int height) {
		getViewport().update(width, height);
	}

	@Override
	public void hide() {
		/* Desactiva el capturador de eventos de teclado al salir de la pantalla. */
		Gdx.input.setInputProcessor(null);
	}

	@Override
	public void act() {
		super.act();
	}

	@Override
	public void act(float delta) {
		super.act(delta);
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	/**
	 * Añade una acción a la cola de acciones.
     *
	 * @param accion que se añadirá a la cola de acciones.
     */
	protected void encolarAccion(Action accion) {
		if (getActors().first() == null)
			return;

		getActors().first().addAction(Actions.after(accion));
	}

	/**
	 * Crea una Acción para cambiar de pantalla.
     *
	 * @param destino pantalla destino
	 * @return Accion con la transición a la pantalla.
     */
	protected Action nuevaTransicion(final Pantalla destino) {
		Action accion = Actions.run(new Runnable() {
			@Override
			public void run() {
				GestorPantallas.getInstance().cargar(destino);
			}
		});

		return accion;
	}

	/**
	 * Método que las screens que lo necesiten pueden sobreescribir para añadir efectos a las transiciones.
     *
	 * @see com.aurbano.gestores.GestorEfectos
	 * @param pantalla a la que se transicionará
     */
	public void transicionar(final Pantalla pantalla) {

    }

	protected void recuperarAlfaGradualmente(float duracion) {
		getRoot().setColor(0, 0, 0 , 0);
		getRoot().addAction(Actions.fadeIn(duracion));
	}
}

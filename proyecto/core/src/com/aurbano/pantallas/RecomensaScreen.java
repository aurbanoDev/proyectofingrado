package com.aurbano.pantallas;

import com.aurbano.entidades.Aventurero;
import com.aurbano.entidades.Entidad;
import com.aurbano.gestores.GestorPantallas;
import com.aurbano.gestores.GestorSonido;
import com.aurbano.interfazUsuario.menus.MenuBatalla;
import com.aurbano.interfazUsuario.tablas.TablaAzul;
import com.aurbano.interfazUsuario.tablas.TablaPersonajes;
import com.aurbano.juego.Juego;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

import static com.aurbano.juego.Constantes.*;

/**
 * Created by infe on 01/05/2016.
 */
public class RecomensaScreen extends ScreenAbstracta {

    private Table contenedor;
    private TablaAzul tablaPie;
    private TablaAzul tablaOro;
    private TablaAzul tablaExperiencia;
    private TablaPersonajes tablaPersonajes;

    private MenuBatalla menuBatalla;

    public RecomensaScreen(Juego juego) {
        super(juego);

        menuBatalla = world.getMenuBatalla();
        tablaPersonajes = new TablaPersonajes
                (X_FULL_SCREEN + 75, Gdx.graphics.getHeight() -494 , ANCHO_TOTAL - 150, 420);

        tablaPie = new TablaAzul(X_FULL_SCREEN + 75, 68, ANCHO_TOTAL - 150, 75);
        tablaPie.setFondoAzul();
        tablaPie.activarSkin();
        tablaPie.mostrarMensajePulsaParaContinuar();

        contenedor = new Table();
        contenedor.setBounds(X_FULL_SCREEN + 75, 145 , ANCHO_TOTAL - 150, 95);
        contenedor.defaults().expand().fill();

        tablaExperiencia = new TablaAzul();
        tablaExperiencia.setFondoAzul();
        tablaExperiencia.activarSkin();

        tablaOro = new TablaAzul();
        tablaOro.setFondoAzul();
        tablaOro.activarSkin();

        contenedor.add(tablaExperiencia);
        contenedor.add(tablaOro);
    }

    @Override
    public void show() {
        super.show();
        clear();

        int premioOro = menuBatalla.getDineroObtenido();
        int premioPx = menuBatalla.getExperienciaObtenida();

        for (Aventurero aventurero : world.getGrupo())
            aventurero.getExperiencia().sumar(premioPx);

        tablaOro.mostrarMensajeOroGanado(premioOro);
        tablaExperiencia.mostrarMensajeExperienciaGanada(premioPx);
        tablaPersonajes.cargarAventurerosConBarraPX(world.getGrupo());

        addActor(tablaPersonajes);
        addActor(contenedor);
        addActor(tablaPie);
    }

    @Override
    public void render(float delta) {
        super.render(delta);

        act(delta);
        draw();
    }

    @Override
    public void act(float delta) {
        super.act(delta);

        if (controles.pulsacionConfirmar()) {
            world.protagonista.restaurarPosicionPreBatalla();
            world.protagonista.setUltimoEstado(Entidad.Estado.ABAJO);
            world.protagonista.setEstadoActual(Entidad.Estado.ABAJO);
            GestorSonido.reproducirSonido("desplazarCursor");
            GestorPantallas.getInstance().cargar(Pantalla.JUEGO);
        }
    }

    @Override
    public void hide() {
        super.hide();

        getRoot().addAction(Actions.fadeOut(1));
    }
}

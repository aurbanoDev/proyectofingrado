package com.aurbano.Utilidades;

import com.aurbano.entidades.Aventurero;
import com.aurbano.entidades.Enemigo;
import com.aurbano.entidades.Entidad;
import com.aurbano.gestores.GestorRecursos;
import com.aurbano.objetos.Arma;
import com.aurbano.objetos.Armadura;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.ui.ProgressBar;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;

import java.io.File;

/**
 * Clase de utilidades varias.
 * @author Adrián Urbano
 */
public class UtilsGdx {
    /**
     * Formatea el tiempo de juego de segundos a HH-MM-SS.
     * @param cantidadTiempoEnSegundos
     * @return StringBuffer con el tiempo formateado.
     */
    public static String formatearTiempoJuego(int cantidadTiempoEnSegundos) {
        int horas = cantidadTiempoEnSegundos / 3600;
        int minutos = (cantidadTiempoEnSegundos % 3600) / 60;
        int segundos = cantidadTiempoEnSegundos % 60;

        return String.format("%02d:%02d:%02d", horas, minutos, segundos);
    }
    /**
     * Construye un NinePatchDrawable con 10 pixeles de margen
     * @param ruta
     * @return NinePatchDrawable
     */
    public static NinePatchDrawable getNinePatch(String ruta) {
        final Texture t = new Texture(Gdx.files.internal(ruta));
        return new NinePatchDrawable(
                new NinePatch(new TextureRegion(t, 1, 1, t.getWidth() - 2, t.getHeight() - 2), 10, 10, 10, 10));
    }
    /**
     * Construye un NinePatchDrawable con el margen delimitado a la cantidad pasada como parámetro.
     * @param ruta
     * @param margen
     * @return NinePatchDrawable con margen variable
     */
    public static NinePatchDrawable getNinePatch(String ruta, int margen) {
        final Texture textura = new Texture(Gdx.files.internal(ruta));
        return new NinePatchDrawable(new NinePatch(new TextureRegion
                (textura, 1, 1, textura.getWidth() - 1, textura.getHeight() - 1), margen, margen, margen, margen));
    }

    /**
     * Construye una cámara con la proyección ortográfica ajustada al ancho y alto del juego.
     * @return la cámara ajustada
     */
    public static OrthographicCamera nuevaCamaraFullScreen() {
        OrthographicCamera camara = new OrthographicCamera();
        camara.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        camara.update();

        return camara;
    }
    /**
     * Contruye una barra de progreso con min-max 0-100 que avanza de 1 en 1.
     * @return ProgressBar skin personalizada
     */
    public static ProgressBar nuevaBarraProgreso() {
        ProgressBar barra = new ProgressBar(0, 100, 1, false, GestorRecursos.getEstiloBarraProgreso());
        return barra;
    }
    /**
     * Contruye una barra de progreso con min-max 0-100 que avanza de 1 en 1 escogiendo el color.
     * @return ProgressBar skin personalizada
     */
    public static ProgressBar nuevaBarraProgreso(Color color) {
        ProgressBar barra = new ProgressBar(0, 100, 1, false, GestorRecursos.getEstiloBarraProgreso(color));
        return barra;
    }
    /**
     * Formatea cantidades númericas para alinearse a la derecha independientemente de número de digitos.
     * @param valor del numero a formatear
     * @param caracteres cantidad de carácteres a la que se ajustará el alineamiento.
     * @return StringBuffer con la cantidad formateada.
     */
    public static StringBuffer formatearNumero(int valor , int caracteres) {
        StringBuffer numero = new StringBuffer(caracteres);
        numero.append(valor);

        if (valor < 10) {
            espaciar(numero, caracteres - 1);
            return numero;
        }

        if (valor < 100) {
            espaciar(numero, caracteres - 2);
            return numero;
        }

        if (valor < 1000) {
            espaciar(numero, caracteres - 3);
            return numero;
        }
        return numero;
    }

    private static void espaciar(StringBuffer texto, int espacios) {
        for (int i = 0; i < espacios; i++)
            texto.insert(0, " ");
    }

    public static StringBuffer formatearCantidad(int cantidad) {
        StringBuffer cantidadFormateada = new StringBuffer(5);
        cantidadFormateada.append(' ').append(':').append(' ').append(cantidad);

        return cantidadFormateada;
    }

    public static StringBuffer separador() {
        return new StringBuffer("/");
    }

    public static int calcularAtaque(Aventurero aventurero) {
        return aventurero.getFuerza() + aventurero.getArma().getAtaque();
    }

    public static int calcularAtaque(Aventurero aventurero, Arma arma) {
        return aventurero.getFuerza() + arma.getAtaque();
    }

    public static int calcularAtaqueMagico(Aventurero aventurero) {
        return aventurero.getMagia() + aventurero.getArma().getAtaqueMagico();
    }

    public static int calcularAtaqueMagico(Aventurero aventurero, Arma arma) {
        return aventurero.getMagia() + arma.getAtaqueMagico();
    }

    public static int calcularDefensa(Aventurero aventurero) {
        return  aventurero.getDestreza() + aventurero.getArmadura().getDefensa();
    }

    public static int calcularDefensa(Aventurero aventurero, Armadura armadura) {
        return aventurero.getDestreza() + armadura.getDefensa();
    }

    public static int calcularDefensaMagica(Aventurero aventurero) {
        return  aventurero.getEspiritu() + aventurero.getArmadura().getDefensaMagica();
    }

    public static int calcularDefensaMagica(Aventurero aventurero, Armadura armadura) {
        return  aventurero.getEspiritu() + armadura.getDefensaMagica();
    }

    public static int calcularPorcentajeAtaque(Aventurero aventurero) {
        int bonusFuerza = (int) (aventurero.getFuerza() * 0.1f);
        int punteria = aventurero.getArma().getPorcentajeAtaque();

        return bonusFuerza + punteria;
    }

    public static int calcularPorcentajeAtaque(Aventurero aventurero, Arma arma) {
        int bonusFuerza = (int) (aventurero.getFuerza() * 0.1f);
        int punteria = arma.getPorcentajeAtaque();

        return bonusFuerza + punteria;
    }

    public static int calcularPorcentajeDefensa(Aventurero aventurero) {
        int reflejos = aventurero.getArmadura().getPorcentajeDefensa();
        int bonusDestreza = (int) (aventurero.getDestreza() * 0.1f);

        return reflejos + bonusDestreza;
    }

    public static int calcularPorcentajeDefensa(Aventurero aventurero, Armadura armadura) {
        int reflejos = armadura.getPorcentajeDefensa();
        int bonusDestreza = (int) (aventurero.getDestreza() * 0.1f);

        return reflejos + bonusDestreza;
    }

    public static int calcularPorcentaje(float progreso, float total) {
        return (int) (progreso * 100 / total);
    }

    public static int calcularDanoAtaqueBasico(Entidad agresor, Entidad defensor) {

        int cantidadDano = 0;
        if (agresor instanceof Aventurero) {
            int bonus = calcularAtaque((Aventurero) agresor);
            cantidadDano = MathUtils.random(240, 320) * bonus / 10;
        }

        if (agresor instanceof Enemigo) {
            int bonus = agresor.getNivel() * agresor.getFuerza() * MathUtils.random(2, 6);
            cantidadDano = MathUtils.random(200, 250) + bonus;
        }

        return cantidadDano;
    }

    public static int calcularDanoAtaqueEspecial(Entidad agresor, Entidad defensor) {
        int cantidadDano = 0;
        if (agresor instanceof Aventurero) {
            int bonus = calcularAtaqueMagico((Aventurero) agresor);
            cantidadDano = MathUtils.random(300, 450) + bonus;
        }

        if (agresor instanceof Enemigo) {
            int bonus = agresor.getNivel() * agresor.getMagia() * MathUtils.random(3, 7);
            cantidadDano = MathUtils.random(225, 350) + bonus;
        }

        return cantidadDano;
    }

    public static String getNombrePartidaGuardada(File save) {
        String nombre = save.getName();
        return nombre.replace(".save", "");
    }

    public static int getRanuraSave(File save) {
        return Integer.parseInt(getNombrePartidaGuardada(save));
    }

}

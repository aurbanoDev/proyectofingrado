package com.aurbano.persistencia;

import com.aurbano.Utilidades.UtilsGdx;
import com.aurbano.entidades.Aventurero;
import com.aurbano.entidades.Protagonista;
import com.aurbano.gestores.GestorRecursos;
import com.aurbano.juego.Mundo;
import com.aurbano.mapas.ContenedorObjetos;
import com.aurbano.mapas.Mapa;
import com.aurbano.objetos.*;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.utils.Array;

import java.io.Serializable;

/**
 * Clase serializable que sirve para que todos los elementos que configuran el estado del mundo puedan persistir.
 * Created by adriua on 06/05/2016.
 */
public class EstadoMundo implements Serializable {

    private String localizacion;
    private String nombreMapaActual;
    private Protagonista protagonista;
    private Aventurero lenn;

    private int dinero;
    private float tiempoJuego;
    private boolean vacio;

    private Equipo[] equipos;
    private Materia[] materias;
    private Elemento[] elementos;
    private ContenedorObjetos[] contenedores;

    public EstadoMundo(Mundo world) {
        if (world == null) {
            vacio = true;
            return;
        }

        Inventario inventario = Inventario.getInstance();
        this.dinero = inventario.getDinero();
        this.materias = inventario.getMaterias().toArray(Materia.class);
        this.equipos = inventario.getTodoElEquipo().toArray(Equipo.class);
        this.elementos = inventario.getElementos().toArray(Elemento.class);

        this.tiempoJuego = world.getTiempoJuego();
        this.nombreMapaActual = world.getMapaActual().getNombre();
        this.localizacion = world.getMapaActual().getLocalizacion();
        this.protagonista = world.getProtagonista();
        this.lenn = world.lenn;

        mapearContenedoresObjetos();
    }

    public void mapearContenedoresObjetos() {
        TiledMap mapa;
        Array<ContenedorObjetos> listaTemporal = new Array<>();

        mapa = GestorRecursos.getMapa("ciudadBosque");
        listaTemporal.addAll(Mapa.getContenedores(mapa));

        mapa = GestorRecursos.getMapa("cueva");
        listaTemporal.addAll(Mapa.getContenedores(mapa));

        mapa = GestorRecursos.getMapa("cuevaTesoro");
        listaTemporal.addAll(Mapa.getContenedores(mapa));

        contenedores = listaTemporal.toArray(ContenedorObjetos.class);
    }

    public boolean isVacio() {
        return vacio;
    }

    public float getTiempoJuego() {
        return tiempoJuego;
    }

    public String getTiempoJuegoFormatedo() {
        return UtilsGdx.formatearTiempoJuego((int) tiempoJuego);
    }

    public int getDinero() {
        return dinero;
    }

    public Protagonista getProtagonista() {
        return protagonista;
    }

    public String getNombreMapaActual() {
        return nombreMapaActual;
    }

    public String getLocalizacion() {
        return localizacion;
    }

    public Array<Elemento> getElementos() {
        return new Array<>(elementos);
    }

    public Array<Materia> getMaterias() {
        return new Array<>(materias);
    }

    public Array<Equipo> getEquipos() {
        return new Array<>(equipos);
    }

    public Array<ContenedorObjetos> getContenedores() {
        return new Array(contenedores);
    }

    public Aventurero getLenn() {
        return lenn;
    }

    public Array<Aventurero> getGrupo() {
        Array<Aventurero> lista = new Array<>();
        lista.add(protagonista);
        lista.add(lenn);

        return lista;
    }
}

package com.aurbano.persistencia;

/**
 * Representacion abstracta de un parámetro SQL.
 */
public class Parametro {

    private Object valor;
    private Class<?> tipado;

    public Parametro(Object valor, Class<?> tipado) {
        this.valor = valor;
        this.tipado = tipado;
    }

    public <T> T getValor() {
        return (T) valor;
    }

    public boolean esString() {
        return tipado == String.class;
    }

    public boolean esInteger() {
        return tipado == Integer.class;
    }

    public boolean esFloat() {
        return tipado == Float.class;
    }

    public boolean esDouble() {
        return tipado == Double.class;
    }

    public boolean esBoolean() {
        return tipado == Boolean.class;
    }
}

package com.aurbano.persistencia;

import com.aurbano.Utilidades.UtilsGdx;
import com.aurbano.gestores.GestorSonido;
import com.aurbano.juego.Constantes;
import com.aurbano.juego.Juego;
import com.aurbano.juego.Mundo;
import com.aurbano.mapas.Mapa;
import com.aurbano.objetos.Inventario;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.utils.Array;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Map;

import static com.aurbano.juego.Constantes.*;

/**
 * Clase para gestionar las operaciones de persistencia durante el juego.
 *
 * Created by infe on 05/05/2016.
 */
public class GestorPersistencia {

    private static GestorPersistencia instance;

    private Juego juego;
    private BaseDatos baseDatos;

    private GestorPersistencia() {
        baseDatos = new BaseDatos();
    }

    public void inicializar(Juego juego) {
        this.juego = juego;
    }

    public static GestorPersistencia getInstance() {
        if (instance == null)
            instance = new GestorPersistencia();

        return instance;
    }

    /**
     * Comprueba que todas las rutas que necesita el juego existen y estan inicializadas correctamente.
     * Comprobara la disposicion de las carpetas que necesita el juego , asi como el estado de la base de datos
     * y del fichero de configuracion con las preferencias.
     */
    public void asegurarRutas() {
        File carpetaRaiz = new File(RUTA_CARPETA);
        File carpetaSaves = new File(RUTA_CARPETA_SAVES);
        File baseDatosLite = new File(FICHERO_SQLITE);

        if (!carpetaRaiz.exists())
            carpetaRaiz.mkdir();

        if (!carpetaSaves.exists())
            carpetaSaves.mkdir();

        if (!baseDatosLite.exists())
            inicializarBaseDatos();

        asegurarFicheroPreferencias();
    }

    private void inicializarBaseDatos() {
        baseDatos.conectar();
        baseDatos.crearTablas();
        baseDatos.desconectar();
    }

    /**
     * Comprueba si existe el fichero de preferencias del juego. En caso de no existir creara
     * uno nuevo donde se guardará la configuracion por defecto.
     */
    private void asegurarFicheroPreferencias() {
        Preferences config = Gdx.app.getPreferences(Constantes.FICHERO_CONFIGURACION);

        if (config.contains("defecto"))
            return;

        config.putBoolean("defecto", true);
        config.putBoolean("ayuda", true);
        config.putBoolean("sonido", true);
        config.putBoolean("musica", true);
        config.putBoolean("dificultad", false);
        config.flush();
    }

    /**
     * Recupera una preferencia del fichero de preferencias del juego.
     *
     * @param preferencia que se quiere recuperar
     * @return el valor de la preferencia
     */
    public static boolean getPreferenciaBoolean(String preferencia) {
        Preferences config = Gdx.app.getPreferences(Constantes.FICHERO_CONFIGURACION);
        return config.getBoolean(preferencia);
    }

    /**
     * Guarda en el fichero de preferencias del juego un valor.
     *
     * @param clave de la propiedad
     * @param estado que sera almacenado
     */
    public static void guardarPreferencia(String clave, boolean estado) {
        Preferences config = Gdx.app.getPreferences(FICHERO_CONFIGURACION);
        config.putBoolean(clave, estado);
        config.flush();
    }

    /**
     * Guarda el estado del mundo en el momento de llevar a cabo la operacion, de tal manera
     * que sea posible recuperar ese estado mas tarde. Para ello se serializara en un fichero
     * binario toda la informacion necesaria y se registrara en la base de datos el numero de save
     * asociado al archivo.
     *
     * @param ranura indica el numero del save
     * @param world que se utilizara como referencia a preservar
     */
    public void guardarPartida(int ranura, Mundo world) {

        try (ObjectOutputStream serializador = getSerializador(ranura)) {
            EstadoMundo estado = new EstadoMundo(world);
            serializador.writeObject(estado);

            String localizacion = world.getMapaActual().getLocalizacion();
            String tiempoJugado = UtilsGdx.formatearTiempoJuego(world.getTiempoJuego());
            int dinero = Inventario.getInstance().getDinero();
            int nivelProtagonista = world.getProtagonista().getNivel();

            baseDatos.conectar();
            baseDatos.salvarPartida(ranura, localizacion, tiempoJugado, dinero, nivelProtagonista);
        } catch (IOException e) {
            e.printStackTrace();
        }

        baseDatos.desconectar();
    }

    private ObjectOutputStream getSerializador(int ranura) throws IOException {
        FileOutputStream destino = new FileOutputStream(RUTA_CARPETA_SAVES + ranura + ".save");
        return new ObjectOutputStream(destino);
    }

    private ObjectInputStream getDeserializador(int ranura) throws IOException {
        FileInputStream origen = new FileInputStream(RUTA_CARPETA_SAVES + ranura + ".save");
        return new ObjectInputStream(origen);
    }

    private EstadoMundo deserializarEstadoDelMundo(int ranuraGuardado) {
        try (ObjectInput deserializador = getDeserializador(ranuraGuardado)) {
            Object estadoMundo = deserializador.readObject();
            return (EstadoMundo) estadoMundo;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Recupera el estado del mundo apartir de un backup proporcionado como parametro.
     *
     * @param estado que sera utilizado para poner al dia el mundo.
     */
    public void recuperarEstadoDelMundo(EstadoMundo estado) {
        Mundo world = juego.world;
        Mapa mapaActual = world.getMapaActual();
        mapaActual.restaurarEstadoObjetosDelMapa(estado);
        mapaActual.cargarMapa(estado.getNombreMapaActual());

        world.puntoGuardado = mapaActual.getObjetoPuntoGuardado();
        world.setProtagonista(estado.getProtagonista());
        world.getProtagonista().cargarAnimaciones();
        world.getProtagonista().setMapaActual(mapaActual);

        world.lenn = estado.getLenn();
        world.lenn.cargarAnimaciones();
        world.lenn.setMapaActual(mapaActual);

        Inventario.restablecer();
        Inventario.getInstance().restablecer(estado);

        world.setTiempoJuego(estado.getTiempoJuego());
        juego.inicializarNucleo(world);
    }

    /**
     * Recupera una lista con los estado del mundo salvados con anterioridad.
     * Este metodo sera utilizado a la hora de cargar una partida anterior.
     *
     * @return una lista con los estados del mundo salvados con anterioridad.
     */
    public Array<EstadoMundo> getSavesDisponibles() {
        File carpetaSaves = new File(RUTA_CARPETA_SAVES);
        FileFilter filtroSaves = new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                if (pathname.getAbsolutePath().endsWith(".save"))
                    return true;

                return false;
            }
        };

        File[] partidas = carpetaSaves.listFiles(filtroSaves);
        Arrays.sort(partidas, new Comparator<File>() {
            @Override
            public int compare(File o1, File o2) {
                int n1 = Integer.parseInt(o1.getName().substring(0, 1));
                int n2 = Integer.parseInt(o2.getName().substring(0, 1));
                return n1 - n2;
            }
        });

        ArrayList<Integer> listaRanurasOcupadas = new ArrayList<>();
        for (File partida : partidas)
            listaRanurasOcupadas.add(UtilsGdx.getRanuraSave(partida));

        Array<EstadoMundo> saves = new Array<>(4);
        for (int i = 1; i < 5; i++) {
            if (listaRanurasOcupadas.contains(i))
                saves.add(deserializarEstadoDelMundo(i));
            else
                saves.add(new EstadoMundo(null));
        }

        return saves;
    }

    /**
     * Indica si hay archivos de guardado validos en el directorio de partidas guardadas.
     *
     * @return verdadero en caso de encontrar archivos validos.
     */
    public boolean hayPartidasGuardadas() {
        File carpetaSaves = new File(RUTA_CARPETA_SAVES);
        FileFilter filtroSaves = new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                if (pathname.getAbsolutePath().endsWith(".save"))
                    return true;

                return false;
            }
        };

        File[] partidas = carpetaSaves.listFiles(filtroSaves);
        if (partidas.length == 0)
            return false;

        return true;
    }

    public void cargarPreferencias(int ranura) {
        baseDatos.conectar();
        Map<String, Boolean> preferencias = baseDatos.getPreferencias(ranura);
        baseDatos.desconectar();

        boolean ayuda = preferencias.get("ayuda");
        boolean sonido = preferencias.get("sonido");
        boolean musica = preferencias.get("musica");
        boolean dificultad = preferencias.get("dificultad");

        guardarPreferencia("ayuda", ayuda);
        guardarPreferencia("sonido", sonido);
        guardarPreferencia("musica", musica);
        guardarPreferencia("dificultad", dificultad);

        GestorSonido.configurarAudio(sonido, musica);
    }

}

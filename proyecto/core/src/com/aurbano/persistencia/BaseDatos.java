package com.aurbano.persistencia;

import org.sqlite.SQLiteConfig;

import java.sql.*;
import java.util.HashMap;
import java.util.Map;

import static com.aurbano.juego.Constantes.FICHERO_SQLITE;
import static com.aurbano.juego.Constantes.RUTA_CARPETA_SAVES;

/**
 * Clase para gestionar la base de datos SQLITE.
 */
public class BaseDatos {

    private Connection conexion;

    public void conectar() {
        try {
            Class.forName("org.sqlite.JDBC");
            SQLiteConfig configuracion = new SQLiteConfig();
            configuracion.enforceForeignKeys(true); // Habilitar claves foraneas

            conexion = DriverManager.getConnection("jdbc:sqlite:" + FICHERO_SQLITE, configuracion.toProperties());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void crearTablas() {
        String saves = "CREATE TABLE IF NOT EXISTS SAVES (numero_ranura INTEGER PRIMARY KEY, ruta_save VARCHAR(500));";

        String detalles = "CREATE TABLE IF NOT EXISTS DETALLES(numero_ranura INTEGER PRIMARY KEY, localizacion VARCHAR(255)," +
                "tiempo_juego VARCHAR(10), dinero INTEGER, nivelProtagonista INTEGER )";

        String config = "CREATE TABLE IF NOT EXISTS CONFIGURACION (" +
                "numero_ranura INTEGER PRIMARY KEY, ayuda BOOLEAN, sonido BOOLEAN, musica BOOLEAN, dificultad BOOLEAN)";

        try (PreparedStatement sentencia = conexion.prepareStatement(saves);
             PreparedStatement sentencia2 = conexion.prepareStatement(config);
            PreparedStatement sentencia3 = conexion.prepareStatement(detalles)) {

            sentencia.executeUpdate();
            sentencia2.executeUpdate();
            sentencia3.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void desconectar() {
        if (conexion == null)
            return;

        try {
            conexion.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void salvarPartida(int numeroRanura, String localizacion, String tiempoJuego, int dinero, int nivel) {
        boolean ranuraLibre = esRanuraLibre(numeroRanura);
        if (!ranuraLibre)
            eliminarRanura(numeroRanura);

        String salvarRuta = "INSERT INTO SAVES(numero_ranura, ruta_save) VALUES(?, ?)";

        ParametrosSQLITE parametros = ParametrosSQLITE.getInstance();
        parametros.agregarInt(numeroRanura);
        parametros.agregarString(RUTA_CARPETA_SAVES + numeroRanura + ".save");

        ejecutarUpdate(salvarRuta, parametros);

        String salvarDetalles = "INSERT INTO DETALLES" +
                "(numero_ranura, localizacion, tiempo_juego, dinero, nivelProtagonista) VALUES(?, ?, ?, ?, ?)";

        parametros = ParametrosSQLITE.getInstance();
        parametros.agregarInt(numeroRanura);
        parametros.agregarString(localizacion);
        parametros.agregarString(tiempoJuego);
        parametros.agregarInt(dinero);
        parametros.agregarInt(nivel);

        ejecutarUpdate(salvarDetalles, parametros);

        String salvarConfiguracion =
                "INSERT INTO CONFIGURACION(numero_ranura, ayuda, sonido, musica, dificultad) VALUES (?, ?, ?, ?, ?)";

        parametros = ParametrosSQLITE.getInstance();
        parametros.agregarInt(numeroRanura);
        parametros.agregarBoolean(GestorPersistencia.getPreferenciaBoolean("ayuda"));
        parametros.agregarBoolean(GestorPersistencia.getPreferenciaBoolean("sonido"));
        parametros.agregarBoolean(GestorPersistencia.getPreferenciaBoolean("musica"));
        parametros.agregarBoolean(GestorPersistencia.getPreferenciaBoolean("dificultad"));

        ejecutarUpdate(salvarConfiguracion, parametros);
    }

    /**
     * Recupera un mapa con las preferencias registradas durante una partida salvada.
     * @param numeroRanura de la partida guardada
     * @return el mapa con las propiedades registradas
     */
    public Map<String, Boolean> getPreferencias(int numeroRanura) {
        HashMap<String, Boolean> mapa = new HashMap<>();

        String consulta = "SELECT ayuda, sonido, musica, dificultad FROM configuracion WHERE numero_ranura = ?";

        try (PreparedStatement sentencia = conexion.prepareStatement(consulta)) {
            sentencia.setInt(1, numeroRanura);
            ResultSet resultado = sentencia.executeQuery();
            resultado.next();
            mapa.put("ayuda", resultado.getBoolean(1));
            mapa.put("sonido", resultado.getBoolean(2));
            mapa.put("musica", resultado.getBoolean(3));
            mapa.put("dificultad", resultado.getBoolean(4));

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }

        return mapa;
    }

    /**
     * Ejecuta un insert de manera segura con la consulta proporcionada como parametro.
     * El metodo devuelve un booleano indicando si la operacion pudo realizarse con exito o no.
     *
     * @param consultaSQL
     * @return si la operacion se realizo con exito
     */
    public boolean ejecutarUpdate(String consultaSQL) {
        return ejecutarUpdate(consultaSQL, null);
    }

    private boolean ejecutarUpdate(String consultaSQL, ParametrosSQLITE parametros) {
        try {
            PreparedStatement sentencia = conexion.prepareStatement(consultaSQL);
            if (parametros != null)
                parametros.parametrizarSentencia(sentencia);

            sentencia.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public ResultSet ejecutarConsulta(String consulta, ParametrosSQLITE parametros) {

        try (PreparedStatement sentencia = conexion.prepareStatement(consulta)) {
            if (parametros != null)
                parametros.parametrizarSentencia(sentencia);

            ResultSet resultado = sentencia.executeQuery();
            return resultado;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    public boolean esRanuraLibre(int numero) {
        String sql = "SELECT * FROM SAVES WHERE numero_ranura = ?";

        ParametrosSQLITE parametros = ParametrosSQLITE.getInstance();
        parametros.agregarInt(numero);

        ResultSet resultado = ejecutarConsulta(sql, parametros);
        if (resultado == null)
            return true;

        return false;
    }

    public void eliminarRanura(int ranura) {
        String sql = "DELETE FROM SAVES WHERE numero_ranura = ?";
        String sql2 = "DELETE FROM DETALLES WHERE numero_ranura = ?";
        String sql3 = "DELETE FROM CONFIGURACION WHERE numero_ranura = ?";

        try (PreparedStatement sentencia = conexion.prepareStatement(sql);
            PreparedStatement sentencia2 = conexion.prepareStatement(sql2);
            PreparedStatement sentencia3 = conexion.prepareStatement(sql3)) {

            iniciarTransaccion();
            sentencia.setInt(1, ranura);
            sentencia2.setInt(1, ranura);
            sentencia3.setInt(1, ranura);
            sentencia.executeUpdate();
            sentencia2.executeUpdate();
            sentencia3.executeUpdate();
        } catch (SQLException e) {
            cancelarTransaccion();
            e.printStackTrace();
        } finally {
            validarTransaccion();
        }
    }

    private void iniciarTransaccion() {
        try {
            conexion.setAutoCommit(false);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void validarTransaccion() {
        try {
            conexion.setAutoCommit(true);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void cancelarTransaccion() {
        try {
            conexion.rollback();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}

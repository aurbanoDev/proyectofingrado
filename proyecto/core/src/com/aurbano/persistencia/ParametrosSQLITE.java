package com.aurbano.persistencia;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Estrategia para almacenar y gestionar parametros SQL, permitiendo
 * parametrizar comodamente prepared statements.
 */
public class ParametrosSQLITE {

    /* Singleton a trav�s del cual se realizaran todas las operaciones. */
    private static ParametrosSQLITE instance;

    private ArrayList<Parametro> parametros;

    /* Constructor restringido. */
    private ParametrosSQLITE() {
        parametros = new ArrayList<>();
    }

    /**
     * Obtiene una instancia lista para almacenar los parametros.
     * (antes limpiar� los parametros registrados anteriormente).
     *
     * @return
     */
    public static ParametrosSQLITE getInstance() {
        if (instance == null)
            instance = new ParametrosSQLITE();

        instance.parametros.clear();
        return instance;
    }

    public ParametrosSQLITE agregarString(String valor) {
        Parametro texto = new Parametro(valor, String.class);
        parametros.add(texto);

        return instance;
    }

    public ParametrosSQLITE agregarInt(int valor) {
        Parametro entero = new Parametro(valor, Integer.class);
        parametros.add(entero);

        return instance;
    }

    public ParametrosSQLITE agregarFloat(float valor) {
        Parametro decimal = new Parametro(valor, Float.class);
        parametros.add(decimal);

        return instance;
    }

    public ParametrosSQLITE agregarDouble(double valor) {
        Parametro decimal = new Parametro(valor, Double.class);
        parametros.add(decimal);

        return instance;
    }

    public ParametrosSQLITE agregarBoolean(boolean valor) {
        Parametro booleano = new Parametro(valor, Boolean.class);
        parametros.add(booleano);

        return instance;
    }

    /**
     * Recupera los parametros registrados previamente.
     *
     * @return
     */
    public Parametro[] get() {
        return parametros.toArray(new Parametro[0]);
    }

    /**
     * Aplica los parametros registrados hasta el momento al prepared Statment proporcionado.
     *
     * @param sentencia que sera parametrizada
     * @throws SQLException
     */
    public void parametrizarSentencia(PreparedStatement sentencia) throws SQLException {
        int indice = 1;
        for (Parametro param : parametros) {

            if (param.esString())
                sentencia.setString(indice++, (String) param.getValor());
            else if (param.esInteger())
                sentencia.setInt(indice++, (Integer) param.getValor());
            else if (param.esFloat())
                sentencia.setFloat(indice++, (Float) param.getValor());
            else if (param.esDouble())
                sentencia.setDouble(indice++, (Double) param.getValor());
            else if (param.esBoolean())
                sentencia.setBoolean(indice++, (Boolean) param.getValor());
        }

    }

}

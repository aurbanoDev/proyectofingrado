package com.aurbano.gestores;

import com.aurbano.entidades.Enemigo;
import com.aurbano.entidades.Entidad;
import com.aurbano.objetos.Elemento;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Timer;

import java.util.HashMap;

import static com.aurbano.juego.Constantes.RUTA_FUENTE_NEGRITA;
import static com.aurbano.juego.Constantes.RUTA_FUENTE_PRINCIPAL;
import static com.aurbano.objetos.RegistroObjetos.*;

/**
 * Gestor para manipular BitmapFonts durante el juego.
 *
 * Created by infe on 01/05/2016.
 */
public class GestorFuentes {

    private HashMap<String, BitmapFont> fuentes = new HashMap<String, BitmapFont>();

    private String texto;
    private Vector2 posicion;

    private boolean renderizando;
    private float tiempoRenderizado;
    private BitmapFont fuenteSeleccionada;

    /* Singleton de la clase */
    private static GestorFuentes instance;

    private GestorFuentes() {
        mapearFuentes();
        posicion = new Vector2();
        fuenteSeleccionada = fuentes.get("juego");
    }

    public static GestorFuentes getInstance() {
        if (instance == null)
            instance = new GestorFuentes();

        return instance;
    }

    private void mapearFuentes() {
        FreeTypeFontGenerator.FreeTypeFontParameter parametros = new FreeTypeFontGenerator.FreeTypeFontParameter();
        FreeTypeFontGenerator generador = new FreeTypeFontGenerator(Gdx.files.internal(RUTA_FUENTE_PRINCIPAL));

        parametros.size = 32;
        parametros.borderColor = Color.BLACK;
        fuentes.put("menuPrincipal", generador.generateFont(parametros));

        parametros.size = 22;
        parametros.borderWidth = 1;
        parametros.shadowColor = Color.BLACK;
        parametros.shadowOffsetX = 1;
        parametros.shadowOffsetY = 1;
        fuentes.put("juego", generador.generateFont(parametros));

        generador = new FreeTypeFontGenerator(Gdx.files.internal(RUTA_FUENTE_NEGRITA));
        parametros.size = 22;
        fuentes.put("siglas", generador.generateFont(parametros));

        generador.dispose();
    }

    /**
     * Establece la posición donde será renderizado el texto.
     * @param posicion en la pantalla
     */
    public void setPosicion(Vector2 posicion) {
        this.posicion.x = posicion.x;
        this.posicion.y = posicion.y;
    }

    /**
     * Establece la posicion de renderizado relativa a la entidad proporcionada como parametro.
     *
     * @param entidad cuyo rectangulo sera utilizado para posicionar el texto
     */
    public void setPosicion(Entidad entidad) {
        if (entidad instanceof Enemigo) {
            posicion.x = entidad.getX() +  entidad.getRectangulo().width;
            posicion.y = entidad.getY()+  entidad.getRectangulo().height;
            return;
        }

        posicion.x = entidad.getX();
        posicion.y = entidad.getY()+  entidad.getRectangulo().height;
    }

    public void setTexto(String texto) {
        this.texto = texto;
        fuenteSeleccionada.setColor(Color.WHITE);
    }

    public void setTexto(String texto, Color color) {
        this.texto = texto;
        fuenteSeleccionada.setColor(color);
    }

    /**
     * Establece el texto de ayuda que renderizara el gestor de fuentes mediante la informacion del elemento
     * proporcionado como parametro.
     *
     * @param elemento del cual se sacará la información
     */
    public void setTextoElemento(Elemento elemento) {
        int identificador = elemento.getId();
        switch (identificador) {
            case POCION:
                texto = "100";
                fuenteSeleccionada.setColor(Color.GREEN);
                break;
            case POCION_S:
                texto = "500";
                fuenteSeleccionada.setColor(Color.GREEN);
                break;
            case POCION_X:
                texto = "1000";
                fuenteSeleccionada.setColor(Color.GREEN);
                break;
            case ETER:
                texto = "100";
                fuenteSeleccionada.setColor(Color.CYAN);
                break;
            case ETER_TURBO:
                texto = "500";
                fuenteSeleccionada.setColor(Color.CYAN);
                break;
            case COLA_FENIX:
                texto = "150";
                fuenteSeleccionada.setColor(Color.GREEN);
                break;
        }
    }

    /**
     * Marca la fuente como preparada, de tal manera que apartir del siguiente ciclo de render
     * será renderizada durante un segundo.
     */
    public void solicitarRenderizado() {
        if (renderizando)
            return;

        tiempoRenderizado = 0;
        renderizando = true;

        Timer.schedule(new Timer.Task() {
            @Override
            public void run() {
                renderizando = false;
            }
        }, 1);
    }

    /**
     * Actualiza el tiempo y la posicion de renderizado.
     */
    private void actualizar() {
        if (tiempoRenderizado > 0.8)
            renderizando = false;

        tiempoRenderizado += Gdx.graphics.getDeltaTime();

        if (tiempoRenderizado < 0.2)
            posicion.y += 1.5;
        else
            posicion.y -= Gdx.graphics.getDeltaTime();
    }

    public void renderizar(SpriteBatch batch) {
        if (!renderizando)
            return;

        actualizar();
        fuenteSeleccionada.draw(batch, texto, posicion.x, posicion.y);
    }
}

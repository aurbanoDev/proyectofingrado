package com.aurbano.gestores;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.utils.Timer;

import java.util.HashMap;

/**
 * Gestor encargado de manejar el sonido ya la música del juego.
 * <p>
 * Created by adriu on 19/03/2016.
 */
public class GestorSonido {

    private static boolean musicaActivada = true;
    private static boolean efectosActivados = true;

    private static HashMap<String, Sound> mapaSonidos = new HashMap<String, Sound>();
    private static HashMap<String, Music> mapaMusicas = new HashMap<String, Music>();

    /**
     * Mapea el audio del juego
     *
     * @param manager en el cual se ha cargado el audio.
     */
    public static void mapearSonidos(AssetManager manager) {
        mapaSonidos.put("saco", manager.get("audios/saco.ogg", Sound.class));
        mapaSonidos.put("error", manager.get("audios/error.ogg", Sound.class));
        mapaSonidos.put("atras", manager.get("audios/atras.ogg", Sound.class));
        mapaSonidos.put("equipar", manager.get("audios/equipar.ogg", Sound.class));
        mapaSonidos.put("cofreMetal", manager.get("audios/cofre.ogg", Sound.class));
        mapaSonidos.put("cofreMadera", manager.get("audios/cofre.ogg", Sound.class));
        mapaSonidos.put("ZakAtacando", manager.get("audios/espadazo.ogg", Sound.class));
        mapaSonidos.put("LennAtacando", manager.get("audios/lanzazo.ogg", Sound.class));
        mapaSonidos.put("efectoElemento", manager.get("audios/elemento.ogg", Sound.class));
        mapaSonidos.put("usarElemento", manager.get("audios/usarElementoBatalla.ogg", Sound.class));
        mapaSonidos.put("saveActivado", manager.get("audios/saveActivado.ogg", Sound.class));
        mapaSonidos.put("HombreLoboAtacando", manager.get("audios/hachazo.ogg", Sound.class));
        mapaSonidos.put("YetiAtacando", manager.get("audios/mazazo.ogg", Sound.class));
        mapaSonidos.put("HidraAtacando", manager.get("audios/mazazo.ogg", Sound.class));
        mapaSonidos.put("confirmar", manager.get("audios/salvarCargar.ogg", Sound.class));
        mapaSonidos.put("turnoPreparado", manager.get("audios/turnoPreparado.ogg", Sound.class));
        mapaSonidos.put("desplazarCursor", manager.get("audios/desplazarCursor.ogg", Sound.class));
        mapaSonidos.put("agua", manager.get("audios/agua.ogg", Sound.class));
        mapaSonidos.put("fuego", manager.get("audios/fuego.ogg", Sound.class));
        mapaSonidos.put("relampago", manager.get("audios/rayo.ogg", Sound.class));
        mapaSonidos.put("hielo", manager.get("audios/hielo.ogg", Sound.class));
        mapaSonidos.put("encuentro", manager.get("audios/encuentro.ogg", Sound.class));

        mapaMusicas.put("intro", manager.get("audios/intro.mp3", Music.class));
        mapaMusicas.put("ambiente", manager.get("audios/ambiente.mp3", Music.class));
        mapaMusicas.put("combate", manager.get("audios/combate.mp3", Music.class));
        mapaMusicas.put("combateJefe", manager.get("audios/combateJefe.mp3", Music.class));
    }

    /**
     * Reproduce un sonido del mapa cotejando por nombre.
     *
     * @param nombre del sonido en el mapa
     */
    public static void reproducirSonido(String nombre) {
        if (!efectosActivados)
            return;

        mapaSonidos.get(nombre).play();
    }

    /**
     * Reproduce un sonido en el mapa cotejando por nombre.
     * La reproduccion será retrasada dependiendo del parametro delay.
     *
     * @param nombre del sonido en el mapa
     * @param delay  para reproducir el sonido
     */
    public static void reproducirSonido(final String nombre, float delay) {
        Timer.schedule(new Timer.Task() {
            @Override
            public void run() {
                reproducirSonido(nombre);
            }
        }, delay);
    }

    public static void reproducirMusica(String nombre) {
        if (!musicaActivada)
            mutearMusica();

        mapaMusicas.get(nombre).setLooping(true);
        mapaMusicas.get(nombre).play();
    }

    public static void reproducirMusica(String nombre, float volumen) {
        mapaMusicas.get(nombre).setVolume(volumen);
        reproducirMusica(nombre);
    }

    /**
     * Establece a 0 el volumen de la musica del juego
     */
    public static void mutearMusica() {
        for (Music musica : mapaMusicas.values())
            musica.setVolume(0);
    }

    /**
     * Reestablece el volumen de la musica del juego
     */
    public static void desmutearMusica(String nombre) {
        for (Music musica : mapaMusicas.values())
            musica.setVolume(1);
    }

    public static void pausarMusica(String nombre) {
        Music musica = mapaMusicas.get(nombre);
        musica.pause();
    }

    public static void pararMusica(String nombre) {
        Music musica = mapaMusicas.get(nombre);
        musica.stop();
    }

    public static void mutearGradualmente(final String nombre, int segundos) {
        final float intervalo = 0.2f;
        final int repeticiones = (int) (segundos / intervalo);

        Music musica = mapaMusicas.get(nombre);
        float volumenInicial = musica.getVolume();
        final float disminucion = volumenInicial / repeticiones;

        Timer.schedule(new Timer.Task() {
            @Override
            public void run() {
                reducirVolumenMusica(nombre, disminucion);
            }
        }, 0, intervalo, repeticiones);
    }

    public static void reducirVolumenMusica(String nombre, float cantidad) {
        Music musica = mapaMusicas.get(nombre);

        float volumen = musica.getVolume() - cantidad;
        if (volumen < 0)
            musica.setVolume(0);
        else
            musica.setVolume(volumen);
    }

    public static void reanudarGradualmente(final String nombre, int segundos, float volumenDeseado) {
        final float intervalo = 0.2f;
        final int repeticiones = (int) (segundos / intervalo);

        Music musica = mapaMusicas.get(nombre);
        musica.setVolume(0);
        final float aumento = volumenDeseado / repeticiones;

        reproducirMusica(nombre);
        if (!musicaActivada)
            return;

        Timer.schedule(new Timer.Task() {
            @Override
            public void run() {
                aumentarVolumenMusica(nombre, aumento);
            }
        }, 0, intervalo, repeticiones);
    }

    private static void aumentarVolumenMusica(String nombre, float aumento) {
        Music musica = mapaMusicas.get(nombre);

        float volumen = musica.getVolume() + aumento;
        if (volumen > 1)
            musica.setVolume(1);
        else
            musica.setVolume(volumen);
    }

    public static void configurarAudio(boolean efectos, boolean musica) {
        efectosActivados = efectos;
        musicaActivada = musica;
    }

    public static void disposearTodo() {
        for (Sound sound : mapaSonidos.values())
            sound.dispose();

        for (Music music : mapaMusicas.values())
            music.dispose();
    }
}

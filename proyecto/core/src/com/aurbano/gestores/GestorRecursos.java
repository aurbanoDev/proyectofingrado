package com.aurbano.gestores;

import com.aurbano.Utilidades.UtilsGdx;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.ProgressBar.ProgressBarStyle;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane.ScrollPaneStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;
import com.badlogic.gdx.utils.Timer;

import java.util.HashMap;
import java.util.Map;

import static com.aurbano.juego.Constantes.*;

/**
 * Clase encargada de cargar y manejar los recursos del juego.
 *
 * @author Adrián Urbano
 */
public class GestorRecursos {

    private static Map<String, TiledMap> mapas = new HashMap<String, TiledMap>();
    private static Map<String, TextureAtlas> atlas = new HashMap<String, TextureAtlas>();
    private static Map<String, BitmapFont> fuentes = new HashMap<String, BitmapFont>();
    private static Map<String, Animation> animaciones = new HashMap<String, Animation>();
    private static Map<String, LabelStyle> estilosEtiqueta = new HashMap<String, LabelStyle>();

    private static Timer.Task tareaRestablecerMapas;
    private static ScrollPaneStyle estiloScrollPanel = new ScrollPaneStyle();
    private static ProgressBarStyle estiloBarraProgreso = new ProgressBarStyle();

    public static void mapearRecursos(final AssetManager manager) {

        cargarAtlas(manager);
        cargarMapas(manager);
        cargarAnimaciones();
        cargarFuentes();
        cargarEstiloEtiqueta();
        cargarEstiloBarraNivel();
        cargarEstiloScrollPanel();

        tareaRestablecerMapas = nuevaTareaRestablecerMapas(manager);
    }

    private static void cargarAtlas(final AssetManager manager) {
        cargarRecurso(ATLAS_GUI, manager.get("atlas/gui.pack", TextureAtlas.class));
        cargarRecurso("aventureros", manager.get("atlas/aventureros.pack", TextureAtlas.class));
        cargarRecurso("enemigos", manager.get("atlas/enemigos.pack", TextureAtlas.class));
        cargarRecurso("puntoGuardado", manager.get("atlas/puntoGuardado.pack", TextureAtlas.class));
        cargarRecurso("item", manager.get("atlas/item.pack", TextureAtlas.class));
        cargarRecurso("efectos", manager.get("atlas/efectos.pack", TextureAtlas.class));
        cargarRecurso("guardia", manager.get("atlas/guardia.pack", TextureAtlas.class));
    }

    private static void cargarMapas(final AssetManager manager){
        cargarRecurso("ciudadBosque", manager.get("mapas/ciudadBosque.tmx", TiledMap.class));
        cargarRecurso("cueva", manager.get("mapas/cueva.tmx", TiledMap.class));
        cargarRecurso("cuevaTesoro", manager.get("mapas/cuevaTesoro.tmx", TiledMap.class));
    }

    private static void cargarAnimaciones() {
        cargarRecurso("zakArriba", new Animation(0.2f, atlas.get("aventureros").findRegions("zakArriba")));
        cargarRecurso("zakAbajo", new Animation(0.2f, atlas.get("aventureros").findRegions("zakAbajo")));
        cargarRecurso("zakDerecha", new Animation(0.2f, atlas.get("aventureros").findRegions("zakDerecha")));
        cargarRecurso("zakIzquierda", new Animation(0.2f, atlas.get("aventureros").findRegions("zakIzquierda")));
        cargarRecurso("zakEsperando", new Animation(0.2f, atlas.get("aventureros").findRegions("zakParado")));
        cargarRecurso("zakAtacando", new Animation(0.2f, atlas.get("aventureros").findRegions("zakAtacando")));
        cargarRecurso("zakMuriendo", new Animation(0.4f, atlas.get("aventureros").findRegions("zakMuriendo")));
        cargarRecurso("zakElemento", new Animation(0.2f, atlas.get("aventureros").findRegions("zakElemento")));
        cargarRecurso("zakEspecial", new Animation(0.2f, atlas.get("aventureros").findRegions("zakElemento")));

        cargarRecurso("lennEsperando", new Animation(0.2f, atlas.get("aventureros").findRegions("LennEsperando")));
        cargarRecurso("lennAtacando", new Animation(0.2f, atlas.get("aventureros").findRegions("LennAtacando")));
        cargarRecurso("lennMuriendo", new Animation(0.2f, atlas.get("aventureros").findRegions("LennMuriendo")));
        cargarRecurso("lennElemento", new Animation(0.2f, atlas.get("aventureros").findRegions("LennElemento")));
        cargarRecurso("lennEspecial", new Animation(0.2f, atlas.get("aventureros").findRegions("LennElemento")));

        cargarRecurso("hombreloboAtacando", new Animation(0.2f, atlas.get("enemigos").findRegions("loboAtacando")));
        cargarRecurso("hombreloboCombateDerecha", new Animation(0.2f, atlas.get("enemigos").findRegions("loboAndando")));
        cargarRecurso("hombreloboEsperando", new Animation(0.2f, atlas.get("enemigos").findRegions("loboParado")));
        cargarRecurso("hombreloboMuriendo", new Animation(0.2f, atlas.get("enemigos").findRegions("loboMuriendo")));

        cargarRecurso("yetiAtacando", new Animation(0.2f, atlas.get("enemigos").findRegions("yetiAtacando")));
        cargarRecurso("yetiEsperando", new Animation(0.2f, atlas.get("enemigos").findRegions("yetiParado")));
        cargarRecurso("yetiMuriendo", new Animation(0.2f, atlas.get("enemigos").findRegions("yetiMuriendo")));

        cargarRecurso("hidraAtacando", new Animation(0.2f, atlas.get("enemigos").findRegions("hidraAtacando")));
        cargarRecurso("hidraEsperando", new Animation(0.2f, atlas.get("enemigos").findRegions("hidraParado")));
        cargarRecurso("hidraMuriendo", new Animation(0.2f, atlas.get("enemigos").findRegions("hidraMuriendo")));
        cargarRecurso("hidraEspecial", new Animation(0.2f, atlas.get("enemigos").findRegions("hidraEspecial")));

        cargarRecurso("item", new Animation(0.1f, atlas.get("item").findRegions("item")));
        cargarRecurso("puntoGuardado", new Animation(0.2f, atlas.get("puntoGuardado").findRegions("puntoGuardado")));

        cargarRecurso("tierra", new Animation(0.1f, atlas.get("efectos").findRegions("tierra")));
        cargarRecurso("fuego", new Animation(0.08f, atlas.get("efectos").findRegions("fuego")));
        cargarRecurso("agua", new Animation(0.08f, atlas.get("efectos").findRegions("agua")));
        cargarRecurso("hielo", new Animation(0.08f, atlas.get("efectos").findRegions("hielo")));
        cargarRecurso("relampago", new Animation(0.1f, atlas.get("efectos").findRegions("rayo")));
    }

    private static void cargarFuentes() {
        FreeTypeFontParameter parametros = new FreeTypeFontParameter();
        FreeTypeFontGenerator generador = new FreeTypeFontGenerator(Gdx.files.internal(RUTA_FUENTE_PRINCIPAL));

        parametros.size = 32;
        parametros.borderColor = Color.BLACK;
        fuentes.put("menuPrincipal", generador.generateFont(parametros));

        parametros.size = 22;
        parametros.borderWidth = 1;
        parametros.shadowColor = Color.BLACK;
        parametros.shadowOffsetX = 1;
        parametros.shadowOffsetY = 1;
        fuentes.put("juego", generador.generateFont(parametros));

        generador = new FreeTypeFontGenerator(Gdx.files.internal(RUTA_FUENTE_NEGRITA));
        parametros.size = 22;
        fuentes.put("siglas", generador.generateFont(parametros));

        generador.dispose();
    }

    private static void cargarEstiloEtiqueta() {
        estilosEtiqueta.put(FUENTE_CYAN, new LabelStyle(fuentes.get("juego"), Color.CYAN));
        estilosEtiqueta.put(FUENTE_BLANCA, new LabelStyle(fuentes.get("juego"), Color.WHITE));
        estilosEtiqueta.put(FUENTE_SIGLA_CYAN, new LabelStyle(fuentes.get("siglas"), Color.CYAN));
        estilosEtiqueta.put(FUENTE_MENU_PRINCIPAL, new LabelStyle(fuentes.get("menuPrincipal"), Color.WHITE));
        estilosEtiqueta.put("desactivada", new LabelStyle(fuentes.get("juego"), Color.GRAY));
    }

    private static void cargarEstiloBarraNivel() {
        estiloBarraProgreso = new ProgressBarStyle();
        estiloBarraProgreso.background = UtilsGdx.getNinePatch("nine/barra9.9.png");
        estiloBarraProgreso.knobBefore = UtilsGdx.getNinePatch("nine/progreso9.9.png", 2);
    }

    private static void cargarEstiloScrollPanel() {
        Skin skin = new Skin(Gdx.files.internal("skin/uiskin.json"));
        estiloScrollPanel.vScroll = skin.getDrawable("default-round");
        estiloScrollPanel.vScrollKnob = skin.getDrawable("default-round");
    }

    /**
     * Restablece el estado de todos los mapas del juego , devolviendo a los objetos del mapa su estado por defecto.
     */
    public static void restablecerMapasPorDefecto() {
        Timer.schedule(tareaRestablecerMapas, 0);
    }

    private static Timer.Task nuevaTareaRestablecerMapas(final AssetManager manager) {
        return new Timer.Task() {
            @Override
            public void run() {
                for (TiledMap mapa : mapas.values())
                    mapa.dispose();

                mapas.clear();
                manager.unload("mapas/cueva.tmx");
                manager.unload("mapas/ciudadBosque.tmx");
                manager.load("mapas/ciudadBosque.tmx", TiledMap.class);
                manager.load("mapas/cueva.tmx", TiledMap.class);
                manager.finishLoading();
                cargarMapas(manager);
            }
        };
    }

	private static void cargarRecurso(String nombre, TextureAtlas textureAtlas) {
		atlas.put(nombre, textureAtlas);
	}

	private static void cargarRecurso(String nombre, Animation animacion) {
		animaciones.put(nombre, animacion);
	}

	private static void cargarRecurso(String clave, TiledMap mapa) {
		mapas.put(clave, mapa);
	}
    /**
     * Devuelve un estilo de progress bar , con el progreso dibujado en el color indicado como parametro.
     *
     * @param color
     * @return ProgressBarStyle
     */
    public static ProgressBarStyle getEstiloBarraProgreso(Color color) {
        ProgressBarStyle estilo = new ProgressBarStyle();
        NinePatchDrawable patch = UtilsGdx.getNinePatch("nine/progreso9.9.png", 2);

        estilo.background = UtilsGdx.getNinePatch("nine/barra9.9.png");
        estilo.knobBefore = patch.tint(color);

        return estilo;
    }
    /**
     * Devuelve una Skin básica con 3 estilos de etiqueta , para poder escribir texto en las celdas sin crear Labels.
     *
     * @return la Skin preparada para ser usada.
     */
    public static Skin getTablaSkin() {
        Skin skin = new Skin();
        skin.add("default", estilosEtiqueta.get(FUENTE_BLANCA), LabelStyle.class);
        skin.add("cyan", estilosEtiqueta.get(FUENTE_CYAN), LabelStyle.class);
        skin.add("sigla", estilosEtiqueta.get(FUENTE_SIGLA_CYAN), LabelStyle.class);
        skin.add("fuente", fuentes.get("juego"), BitmapFont.class);

        return skin;
    }
    /**
     * Devuelve una imagen del atlas , indicando su nombre y el nombre de la región.
     *
     * @param nombreAtlas
     * @param region nombre de la region.
     * @return Image con la region correspondiente.
     */
    public static Image nuevaImagen(String nombreAtlas, String region) {
        return new Image(atlas.get(nombreAtlas).findRegion(region));
    }

    public static TextureAtlas getAtlas(String nombre) {
        return atlas.get(nombre);
    }

    public static Animation getAnimation(String nombre) {
        return animaciones.get(nombre);
    }

    public static TiledMap getMapa(String nombre) {
        return mapas.get(nombre);
    }

    public static LabelStyle getEstiloEtiqueta(String nombre) {
        return estilosEtiqueta.get(nombre);
    }

    public static ScrollPaneStyle getEstiloScroll() {
        return estiloScrollPanel;
    }

    public static ProgressBarStyle getEstiloBarraProgreso() {
        return estiloBarraProgreso;
    }

}

package com.aurbano.gestores;

import com.aurbano.juego.Juego;
import com.aurbano.pantallas.Pantalla;
import com.aurbano.pantallas.ScreenAbstracta;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.utils.IntMap;

/**
 * Clase encargada de gestionar las pantallas del juego.
 *
 * @author Adrián Urbano
 */
public class GestorPantallas {

    private Juego juego;
	private IntMap<ScreenAbstracta> pantallas;

    private static GestorPantallas instance;

	private GestorPantallas() {
		pantallas = new IntMap<>();
	}

	public static GestorPantallas getInstance() {
		if (instance == null)
			instance = new GestorPantallas();

		return instance;
	}

	public void inicializar(Juego juego) {
		this.juego = juego;
	}

    /**
     * Carga la screen asociada a la Pantalla proporcionada como parametro. La pantalla será
     * agregada al mapa de pantallas en caso de que no lo estuviera ya.
     *
     * @param pantalla que se utilizara para determinar la screen que debe ser cargada.
     */
	public void cargar(Pantalla pantalla) {
		if (juego == null)
			return;

		if (!pantallas.containsKey(pantalla.ordinal()))
			pantallas.put(pantalla.ordinal(), pantalla.getInstance(juego));

		ScreenAbstracta screen = pantallas.get(pantalla.ordinal());
		juego.setScreen(screen);
	}

	public void dispose(Pantalla pantalla) {
		if (!pantallas.containsKey(pantalla.ordinal()))
			return;

		pantallas.remove(pantalla.ordinal()).dispose();
	}

	public void dispose() {
		for (Screen screen : pantallas.values())
			screen.dispose();

		pantallas.clear();
	}
}

package com.aurbano.gestores.controles;

import com.aurbano.juego.Constantes;

/**
 * Clase que registra las teclas usadas por el gestor de controles.
 * @author Adrián Urbano
 */
public class CodigosTeclas {

    protected int codigoArriba;
    protected int codigoAbajo;
    protected int codigoIzquierda;
    protected int codigoDerecha;
    protected int codigoMenu;
    protected int codigoConfirmar;
    protected int codigoCancelar;

    public CodigosTeclas() {
        codigoArriba = Constantes.CONTROL_ARRIBA;
        codigoAbajo = Constantes.CONTROL_ABAJO;
        codigoIzquierda = Constantes.CONTROL_IZQUIERDA;
        codigoDerecha = Constantes.CONTROL_DERECHA;
        codigoMenu = Constantes.CONTROL_MENU;
        codigoConfirmar = Constantes.CONTROL_CONFIRMAR;
        codigoCancelar = Constantes.CONTROL_CANCELAR;
    }

    public void setCodigoArriba(int codigoArriba) {
        this.codigoArriba = codigoArriba;
    }

    public void setCodigoAbajo(int codigoAbajo) {
        this.codigoAbajo = codigoAbajo;
    }

    public void setCodigoIzquierda(int codigoIzquierda) {
        this.codigoIzquierda = codigoIzquierda;
    }

    public void setCodigoDerecha(int codigoDerecha) {
        this.codigoDerecha = codigoDerecha;
    }

    public void setCodigoMenu(int menu) {
        this.codigoMenu = menu;
    }

    public void setCodigoConfirmar(int codigoConfirmar) {
        this.codigoConfirmar = codigoConfirmar;
    }

    public void setCodigoCancelar(int codigoCancelar) {
        this.codigoCancelar = codigoCancelar;
    }
}

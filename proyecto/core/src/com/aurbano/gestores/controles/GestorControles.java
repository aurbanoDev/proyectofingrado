package com.aurbano.gestores.controles;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;

/**
 *  Clase encargada de gestionar los controles del juego.
 *  Permite gestionar pulsaciones cortas y mantenidas.
 *
 *  @author Adrián Urbano
 */
public class GestorControles extends InputAdapter {

    private final CodigosTeclas teclas;
    private boolean izquierda, derecha, arriba, abajo;

    private static GestorControles instance;

    private GestorControles() {
        teclas = new CodigosTeclas();
    }

    public static GestorControles getInstance() {
        if (instance == null)
            instance = new GestorControles();

        return instance;
    }

    public boolean mantenidoIzquierda() {
        return izquierda;
    }

    public boolean mantenidoDerecha() {
        return derecha;
    }

    public boolean mantenidoArriba() {
        return arriba;
    }

    public boolean mantenidoAbajo() {
        return abajo;
    }

    public boolean pulsacionAbajo() {
        return (Gdx.input.isKeyJustPressed(teclas.codigoAbajo)) ? true : false;
    }

    public boolean pulsacionArriba() {
        return (Gdx.input.isKeyJustPressed(teclas.codigoArriba)) ? true : false;
    }

    public boolean pulsacionIzquierda() {
        return (Gdx.input.isKeyJustPressed(teclas.codigoIzquierda)) ? true : false;
    }

    public boolean pulsacionDerecha() {
        return (Gdx.input.isKeyJustPressed(teclas.codigoDerecha)) ? true : false;
    }

    public boolean pulsacionMenu() {
        return (Gdx.input.isKeyJustPressed(teclas.codigoMenu)) ? true : false;
    }

    public boolean pulsacionConfirmar() {
        return (Gdx.input.isKeyJustPressed(teclas.codigoConfirmar)) ? true : false;
    }

    public boolean pulsacionCancelar() {
        return (Gdx.input.isKeyJustPressed(teclas.codigoCancelar)) ? true : false;
    }

    public boolean siguiente() {
        return (pulsacionAbajo() || pulsacionDerecha()) ? true : false;
    }

    public boolean anterior() {
        return (pulsacionArriba() || pulsacionIzquierda()) ? true : false;
    }

    @Override
    public boolean keyDown(int keycode) {

        if (keycode == teclas.codigoArriba && !abajo) {
             arriba = true;
             return true;
        }

        if (keycode == teclas.codigoAbajo && !arriba) {
             abajo = true;
             return true;
        }

        if (keycode == teclas.codigoIzquierda&& !derecha) {
             izquierda = true;
             return true;
        }

        if (keycode == teclas.codigoDerecha && !izquierda) {
             derecha = true;
             return true;
        }

        return false;
    }

    @Override
    public boolean keyUp(int keycode) {

        if (keycode == teclas.codigoArriba ) {
            arriba = false;
            return true;
        }

        if (keycode == teclas.codigoAbajo) {
            abajo = false;
            return true;
        }

        if (keycode == teclas.codigoIzquierda) {
            izquierda = false;
            return true;
        }

        if (keycode == teclas.codigoDerecha) {
            derecha = false;
            return true;
        }

        return false;
    }
}

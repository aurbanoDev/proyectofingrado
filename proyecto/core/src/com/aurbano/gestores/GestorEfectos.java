package com.aurbano.gestores;

import com.aurbano.interfazUsuario.menus.MenuResumen;
import com.aurbano.pantallas.ScreenAbstracta;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
/**
 * Clase encargada de manejar los efectos gráficos del juego.
 *
 * @author Adrián Urbano
 */
public class GestorEfectos {

    public static void interpolacionSalida(MenuResumen menuResumen) {
        fundirEnNegroDinamico(Gdx.graphics.getWidth(), 0 , 0.4f, menuResumen.personajes);
        fundirEnNegroDinamico(Gdx.graphics.getWidth(), 0, 0.4f, menuResumen.tablaPie);
        fundirEnNegroDinamico(0, -Gdx.graphics.getHeight(), 0.4f, menuResumen.tablaSecciones);
        fundirEnNegroDinamico(0, -Gdx.graphics.getHeight(), 0.4f, menuResumen.tablaTiempoOro);
    }

    public static void interpolacionEntrada(MenuResumen menuResumen) {
        recuperarAlfaDinamico(-Gdx.graphics.getWidth(), 0 , 0.5f, menuResumen.personajes);
        recuperarAlfaDinamico(-Gdx.graphics.getWidth(), 0 , 0.5f, menuResumen.tablaPie);
        recuperarAlfaDinamico(0, Gdx.graphics.getHeight() , 0.5f, menuResumen.tablaSecciones);
        recuperarAlfaDinamico(0, Gdx.graphics.getHeight(), 0.5f, menuResumen.tablaTiempoOro);
    }

    /**
     * Decrementa el alfa del actor gradualmente hasta llegar a 0.
     * los parametros x, y permiten desplazar al actor durante el proceso.
     * Al completar el efecto el actor recupera su estado inicial.
     *
     * @param x cantidad de pixeles que se desplazará el actor en el eje horizontal
     * @param y cantidad de pixeles que se desplazará el actor  en el eje vertical
     * @param duracion del efecto
     * @param actor objetivo
     */
    public static void fundirEnNegroDinamico(float x, float y, float duracion, Actor actor) {
        actor.addAction(Actions.sequence(
                 Actions.parallel(Actions.moveBy(x, y, duracion), Actions.fadeOut(duracion)),
                 Actions.moveBy(-x, -y)
        ));
    }

    /**
     * Incrementa el alfa del actor gradualmente hasta llegar a 1.
     * los parametros x, y permiten desplazar al actor durante el proceso.
     * El efecto es puramente visual , la posicion real del actor no se verá modificada al acabar.
     *
     * @param x cantidad de pixeles que se desplazará el actor en el eje horizontal
     * @param y cantidad de pixeles que se desplazará el actor  en el eje vertical
     * @param duracion del efecto
     * @param actor objetivo
     */
    public static void recuperarAlfaDinamico(float x, float y, float duracion, Actor actor) {
        actor.addAction(Actions.sequence(
                Actions.moveBy(x, y),
                Actions.parallel(Actions.moveBy(-x, -y, duracion), Actions.fadeIn(0.5f))
        ));
    }

    public static void fundirEnNegro(float duracion, ScreenAbstracta pantalla) {
        pantalla.getRoot().addAction(Actions.sequence(Actions.fadeOut(duracion), Actions.fadeIn(duracion)));
    }
}

package com.aurbano.entidades;

import com.aurbano.gestores.GestorRecursos;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

/**
 * Clase que representa una zona de guardado en el mapa.
 *
 * Created by infe on 02/05/2016.
 */
public class PuntoGuardado {

    private Vector2 posicion;
    private Animation animacion;
    private TextureRegion frameActual;
    private Rectangle rectangulo;
    private float tiempoAnimacion;

    public PuntoGuardado(float x , float y) {
        posicion = new Vector2(x, y);
        animacion = GestorRecursos.getAnimation("puntoGuardado");
        frameActual = animacion.getKeyFrame(1);

        rectangulo = new Rectangle();
        rectangulo.x = posicion.x;
        rectangulo.y = posicion.y;
        rectangulo.width = frameActual.getRegionWidth();
        rectangulo.height = frameActual.getRegionHeight();
    }

    public void actualizar(float delta) {
        tiempoAnimacion += delta;
        frameActual = animacion.getKeyFrame(tiempoAnimacion, true);
    }

    public void renderizar(SpriteBatch batch) {
        batch.draw(frameActual, posicion.x, posicion.y , frameActual.getRegionWidth(), frameActual.getRegionHeight());
    }

    public Vector2 getPosicion() {
        return posicion;
    }

    public void setPosicion(Vector2 posicion) {
        this.posicion = posicion;
    }

    public Rectangle getRectangulo() {
        return rectangulo;
    }
}

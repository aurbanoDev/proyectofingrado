package com.aurbano.entidades;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Array;

/**
 * Factoria que permite generar enemigos. Los mapas en Tiled tienen un campo con los
 * ids de los enemigos que pueden aparecer en cada mapa. Tras recoger los ids del mapa
 * la factoria crea enemigos comodamente.
 *
 * Created by adriu on 24/01/2016.
 */
public class FactoriaEnemigos {

    public static final int HOMBRE_LOBO = 1;
    public static final int YETI = 2;
    public static final int HIDRA = 3;

    public static Enemigo nuevoEnemigo(int id) {

        switch (id) {
            case HOMBRE_LOBO:
                return new Enemigo("HombreLobo", 50, false, false);
            case YETI:
                return new Enemigo("Yeti", 65, false, false);
            case HIDRA:
                Enemigo hidra = new Enemigo("Hidra", 80, true, true);
                hidra.setAtaquesEspeciales("agua", "fuego", "hielo");
                return hidra;

        }

        return null;
    }

    /**
     * Genera un numero aleatorio de enemigos, los tipos de enemigos vendran determinados por el array
     * de identificadores, seleccionando de manera aleatoria valores de la lista.
     *
     * @param identificadores de los enemigos
     * @return enemigos generados aleatoriamente usando los ids proporcionados
     */
    public static Array<Enemigo> nuevoEncuentroAleatorio(Array<Integer> identificadores) {
        int cantidadEnemigos = MathUtils.random(2, 2);
        Array<Enemigo> enemigos = new Array<Enemigo>(cantidadEnemigos);

        for (int i = 0; i < cantidadEnemigos; i++) {
            float tiempoEsperado = MathUtils.random(0.1f, 3);

            Enemigo enemigo = nuevoEnemigo(identificadores.random());
            enemigo.setTiempoEsperado(tiempoEsperado + i);
            enemigos.add(enemigo);
        }

        return enemigos;
    }

}

package com.aurbano.entidades;

import com.aurbano.objetos.RegistroObjetos;
import com.badlogic.gdx.maps.objects.TextureMapObject;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

import java.io.Serializable;

import static com.aurbano.juego.Constantes.ALTO_SPRITE;
import static com.aurbano.juego.Constantes.ANCHO_SPRITE;

/**
 * Clase que representa al personaje principal de juego.
 * @author Adrián Urbano
 */
public class Protagonista extends Aventurero {

    /** Referencia donde debe posicionarse el jugador al terminar una batalla */
    private Vector2 posicionPreBatalla;

    public Protagonista(String nombre, float x, float y) {
        super(nombre);

        posicionPreBatalla = new Vector2();
        posicion.x = x;
        posicion.y = y;
        estadoActual = Estado.QUIETO;
        ultimoEstado = Estado.ABAJO;
        frameActual = animAbajo.getKeyFrame(0);

        inicializarAtributos(4, 2, 7, 3, 5);
    }

    /**
     * Dado el TextureMapObject indicado ,
     * devuelve true si el objeto cumple los requisitos para ser recogido (proximidad y que no haya sido recogido ya)
     * devuelve false si no cumple los requisitos.
     *
     * @param objeto del mapa
     * @return true si se puede recoger, false si no se puede.
     */
    public boolean puedeRecoger(TextureMapObject objeto) {
        Rectangle rectanguloProtagonista = getRectangulo();
        Rectangle rectanguloObjeto = new Rectangle(objeto.getX(), objeto.getY(), ANCHO_SPRITE, ALTO_SPRITE);

        if (!rectanguloObjeto.overlaps(rectanguloProtagonista))
            return false;

        boolean estaRecogido = Boolean.valueOf(objeto.getProperties().get("recogido", String.class));
        if (estaRecogido)
            return false;

        return true;
    }

    /**
     * Registra la posicion del jugador en el mapa antes de comenzar un combate.
     */
    public void guardarPosicionPreBatalla() {
        posicionPreBatalla.x = posicion.x;
        posicionPreBatalla.y = posicion.y;
    }

    /**
     * Recupera la posicion que tenia el jugador en el mapa al concluir un combate.
     */
    public void restaurarPosicionPreBatalla() {
        posicion.x = posicionPreBatalla.x;
        posicion.y = posicionPreBatalla.y;
    }
}

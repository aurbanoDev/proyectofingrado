package com.aurbano.entidades;

import com.aurbano.gestores.GestorRecursos;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

import java.util.Observable;

/**
 * Clase que representa un efecto visual durante el combate
 *
 */
public class EfectoEspecial extends Observable {

    private static final float VELOCIDAD_ATAQUE = 20;

    public String nombre;
    public Vector2 posicion;
    public Vector2 velocidad;
    public Vector2 posicionInicial;
    public Animation animacion;
    public TextureRegion frameActual;
    public float tiempoAnimacion;

    public boolean renderizando;
    public boolean izquierda;
    public boolean derecha;

    public Rectangle rectangulo;

    private static EfectoEspecial instance;

    private EfectoEspecial(String nombre) {
        this.nombre = nombre;

        posicion = new Vector2();
        velocidad =  new Vector2(0, 0);
        rectangulo = new Rectangle();
        animacion = GestorRecursos.getAnimation(nombre);
    }

    public static EfectoEspecial getInstance() {
        if (instance == null)
            instance = new EfectoEspecial("item");

        return instance;
    }

    public void setIzquierda(boolean izquierda) {
        this.derecha = false;
        this.izquierda = izquierda;
    }

    public void setDerecha(boolean derecha) {
        this.izquierda = false;
        this.derecha = derecha;
    }

    public void setPosicion(Vector2 posicion) {
        this.posicion = posicion;
    }

    public void setPosicionInicial(Vector2 posicionInicial) {
        this.posicionInicial = posicionInicial;
    }

    public void renderizar(SpriteBatch batch) {
        if (!renderizando)
            return;

        batch.draw(frameActual, posicion.x, posicion.y, frameActual.getRegionHeight(), frameActual.getRegionHeight());
    }

    public void actualizar(float delta) {
        if (!renderizando)
            return;

        if (animacion.isAnimationFinished(tiempoAnimacion)) {
            renderizando = false;
            posicion.x = 0;
            posicion.y = 0;
            notificarFinalizacion();
            return;
        }

        tiempoAnimacion += delta;
        frameActual = animacion.getKeyFrame(tiempoAnimacion);
        calcularOffSet();
        velocidad.scl(delta);
        posicion.add(velocidad);
    }

    private void notificarFinalizacion() {
        setChanged();
        notifyObservers("efectoRenderizado");
    }

    public void solicitarRenderizado() {

        tiempoAnimacion = 0;
        posicion.x = posicionInicial.x;
        posicion.y = posicionInicial.y;
        renderizando = true;
    }

    private void calcularOffSet() {
        if (!izquierda && !derecha)
            return;

        if (izquierda)
            velocidad.x = -VELOCIDAD_ATAQUE;

        if (derecha)
            velocidad.x = VELOCIDAD_ATAQUE;
    }

    public void cambiarAnimacion(String nombre) {
        this.nombre = nombre;
        this.animacion = GestorRecursos.getAnimation(nombre);
    }

    public Rectangle getRectangulo() {
        rectangulo.x = posicion.x;
        rectangulo.y = posicion.y;
        rectangulo.width = frameActual.getRegionWidth();
        rectangulo.height = frameActual.getRegionHeight();

        return rectangulo;
    }

    public Rectangle getRectAtaque() {
        rectangulo.x = posicion.x;
        rectangulo.y = posicion.y;
        rectangulo.width = 40;
        rectangulo.height = 64;

        return rectangulo;
    }

    public String getNombre() {
        return nombre;
    }
}

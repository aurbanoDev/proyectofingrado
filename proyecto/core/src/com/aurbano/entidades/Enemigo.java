package com.aurbano.entidades;

import com.aurbano.gestores.GestorRecursos;
import com.aurbano.persistencia.GestorPersistencia;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Array;

/**
 * @autor Clase que representa un enemigo.
 */
public class Enemigo extends Entidad {

    /* Cantidad de puntos de experiencia que otorga al grupo al ser derrotado. */
    private int premioExperiencia;

    /* Cantidad de puntos de oro que otorga al grupo al ser derrotado. */
    private int premioDinero;

    private boolean tieneAtaqueEspecial;
    private Array<String> ataquesEspeciales;

    private boolean esJefe;

    public Enemigo(String nombre, int premioExperiencia, boolean tieneAtaqueEspecial, boolean esJefe) {
        super(nombre);
        cargarAnimaciones();
        cargarAtributos();

        this.ataquesEspeciales = new Array<>();
        this.premioExperiencia = premioExperiencia;
        this.premioDinero = MathUtils.random(18, 60);
        this.estadoActual = Estado.ESPERANDO;
        this.ultimoEstado = Estado.ESPERANDO;
        this.frameActual = animEsperando.getKeyFrame(0);
        this.tieneAtaqueEspecial = tieneAtaqueEspecial;
        this.esJefe = esJefe;

        if (!esJefe)
            this.vida = new BarraEstado(MathUtils.random(750, 1000));
        else
            this.vida = new BarraEstado(MathUtils.random(2500, 2850));
    }

    @Override
    protected void cargarAnimaciones() {
        String enemigo = nombre.toLowerCase();

        animAtaque = GestorRecursos.getAnimation(enemigo + "Atacando");
        animEsperando = GestorRecursos.getAnimation(enemigo + "Esperando");
        animDerechaCombate = GestorRecursos.getAnimation(enemigo + "CombateDerecha");
        animMuerte = GestorRecursos.getAnimation(enemigo + "Muriendo");
        animEspecial = GestorRecursos.getAnimation(enemigo + "Especial");
    }

    @Override
    public void renderizar(SpriteBatch batch) {
        if (frameActual == null)
            return;

        float anchura = frameActual.getRegionWidth();
        float altura = frameActual.getRegionHeight();

        if (esJefe)
            batch.draw(frameActual, posicion.x, posicion.y, anchura * 1.5f, altura * 1.5f);
        else
            batch.draw(frameActual, posicion.x, posicion.y, anchura, altura);
    }

    private void cargarAtributos() {
        boolean dificultad = GestorPersistencia.getPreferenciaBoolean("dificultad");

        int maximo = 20;
        if (dificultad)
            maximo = 25;

        int fuerzaRandom = MathUtils.random(10, maximo);
        int magiaRandom = MathUtils.random(10, maximo);
        int destrezaRandom = MathUtils.random(10, maximo);
        int espirituRandom = MathUtils.random(10, maximo);
        int vitalidadRandom = MathUtils.random(10, maximo);

        inicializarAtributos(fuerzaRandom, magiaRandom, destrezaRandom, espirituRandom, vitalidadRandom);
    }

    public void setPosicionDeBatalla(int posicionDeBatalla) {
        switch (posicionDeBatalla) {
            case 1:
                posicion.x = Gdx.graphics.getWidth() * 0.20f;
                posicion.y = Gdx.graphics.getHeight() * 0.45f;
                break;
            case 2:
                posicion.x = Gdx.graphics.getWidth() * 0.25f;
                posicion.y = Gdx.graphics.getHeight() * 0.3f;
                break;
        }

        if (esJefe) {
            posicion.x = Gdx.graphics.getWidth() * 0.20f;
            posicion.y = Gdx.graphics.getHeight() * 0.35f;
        }
    }

    public int getPremioExperiencia() {
        return premioExperiencia;
    }

    public int getPremioDinero() {
        return premioDinero;
    }

    public boolean tieneAtaqueEspecial() {
        return tieneAtaqueEspecial;
    }

    public boolean esJefe() {
        return esJefe;
    }

    public Array<String> getAtaquesEspeciales() {
        return ataquesEspeciales;
    }

    public void setAtaquesEspeciales(String... identificadores) {
        ataquesEspeciales.addAll(identificadores);
    }
}

package com.aurbano.entidades;

import com.aurbano.Utilidades.UtilsGdx;
import com.aurbano.gestores.GestorFuentes;
import com.aurbano.gestores.GestorRecursos;
import com.aurbano.gestores.GestorSonido;
import com.aurbano.mapas.Mapa;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Timer;

import java.io.Serializable;
import java.util.Observable;

import static com.aurbano.juego.Constantes.*;
/**
 * Clase abstracta que representa una entidad genérica del juego.
 *
 * @author Adrián Urbano
 */
public abstract class Entidad extends Observable implements Serializable {

    protected String nombre;
    protected Vector2 posicion;
    protected Vector2 velocidad;
    protected transient Mapa mapaActual;

    /* Atributos */
    protected int nivel;
    protected int magia;
    protected int fuerza;
    protected int destreza;
    protected int espiritu;
    protected int vitalidad;

    /* Representa una barra de estado con valor actual y maximo, 0000 / 0000. */
    protected BarraEstado vida, mana;

    /* Imagen que se mostrará sobre el cuadro de diálogo cuando el personaje hable */
    protected transient Image retrato;

    /* Frame actual de la entidad, recogido de las animaciones. */
    protected transient TextureRegion frameActual;

    /* Animaciones de movimiento en el mapa. */
    protected transient Animation animIzquierda, animDerecha, animArriba, animAbajo;

    /* Animaciones de las entidades que se mostraran en los combates */
    protected transient Animation animAtaque, animMuerte, animEspecial, animElemento, animDerechaCombate, animEsperando;

    /* Muestra el estado actual de movimiento (arriba, abajo , derecha, izquierda , parado */
    protected Estado estadoActual;

    /* Muestra el ultimo estado de movimiento (arriba, abajo , derecha, izquierda , parado)*/
    protected Estado ultimoEstado;

    /* Tiempo transcurrido desde que utilizo su ultimo turno de batalla. */
    protected float tiempoEsperado;

    /* Tiempo  que debe esperar una entidad para realizar una accion durante una batalla. */
    protected float tiempoAccion;

    /* marcador de tiempo que sirve para indicar el indice del frame de la animacion que se mostrara */
    protected float tiempoAnimacion;

    /* Punto de referencia al que volveran las entidades en un combate despues de desplazarse para atacar. */
    protected Vector2 posicionPrevia;

    /* Rectangulo que utiliza la entidad para posicionarse y calcular su area de acción */
    protected Rectangle rectangulo;

    public enum Estado {
        QUIETO, IZQUIERDA, DERECHA, ABAJO, ARRIBA, ESPERANDO, ATACANDO, MURIENDO, MUERTO, USAR_ELEMENTO, ESPECIAL
    }

    public Entidad(String nombre) {
        this.nombre = nombre;
        this.tiempoAccion = TIEMPO_ACCION_BASE;

        posicion = new Vector2();
        velocidad = new Vector2();
        rectangulo = new Rectangle();
        posicionPrevia = new Vector2();

        mana = new BarraEstado(450, 450);
        vida = new BarraEstado(450);

        cargarRetrato(nombre);
        calcularTiempoAccion();
    }

    protected abstract void cargarAnimaciones();

    public void cargarRetrato(String nombre) {
        String nombreRegion = "retrato" + nombre;
        AtlasRegion region = GestorRecursos.getAtlas(ATLAS_GUI).findRegion(nombreRegion);
        if (region == null)
            return;

        retrato = new Image(region);
    }

    public void actualizarMovimiento(float delta) {
        actualizarEstado(delta);

        if (mapaActual.colisionaVertical(this))
            velocidad.y = 0;

        if (mapaActual.colisionaHorizontal(this))
            velocidad.x = 0;

        velocidad.scl(delta);
        posicion.add(velocidad);
    }

    public void actualizarEstado(float delta) {
        tiempoAnimacion += delta;

        switch (estadoActual) {
            case ARRIBA:
                velocidad.y = VELOCIDAD_MOVIMIENTO;
                frameActual = animArriba.getKeyFrame(tiempoAnimacion, true);
                ultimoEstado = estadoActual;
                break;
            case ABAJO:
                velocidad.y = -VELOCIDAD_MOVIMIENTO;
                frameActual = animAbajo.getKeyFrame(tiempoAnimacion, true);
                ultimoEstado = estadoActual;
                break;
            case IZQUIERDA:
                velocidad.x = -VELOCIDAD_MOVIMIENTO;
                frameActual = animIzquierda.getKeyFrame(tiempoAnimacion, true);
                ultimoEstado = estadoActual;
                break;
            case DERECHA:
                velocidad.x = VELOCIDAD_MOVIMIENTO;
                frameActual = animDerecha.getKeyFrame(tiempoAnimacion, true);
                ultimoEstado = estadoActual;
                break;
            case ESPERANDO:
                frameActual = animEsperando.getKeyFrame(tiempoAnimacion, true);
                ultimoEstado = estadoActual;

                if (vida.getValorActual() < 1) { // Estar muerto.
                    setEstadoActual(Estado.MURIENDO, 0.8f);
                }
                break;
            case ATACANDO:
                frameActual = animAtaque.getKeyFrame(tiempoAnimacion);
                if (animAtaque.isAnimationFinished(tiempoAnimacion)) {
                    GestorFuentes.getInstance().solicitarRenderizado();
                    posicion.set(posicionPrevia);
                    estadoActual = Estado.ESPERANDO;
                    setChanged();
                    notifyObservers("ataqueFinalizado");
                }
                break;
            case USAR_ELEMENTO:
                frameActual = animElemento.getKeyFrame(tiempoAnimacion);
                if (animElemento.isAnimationFinished(tiempoAnimacion)) {
                    estadoActual = Estado.ESPERANDO;
                    setChanged();
                    notifyObservers("elementoFinalizado");
                }
                break;
            case ESPECIAL:
                frameActual = animEspecial.getKeyFrame(tiempoAnimacion);
                if (animEspecial.isAnimationFinished(tiempoAnimacion)) {
                    estadoActual = Estado.ESPERANDO;
                    setChanged();
                    notifyObservers("especialFinalizado");
                }
                break;
            case MURIENDO:
                frameActual = animMuerte.getKeyFrame(tiempoAnimacion);

                if (animMuerte.isAnimationFinished(tiempoAnimacion)) {
                    estadoActual = Estado.MUERTO;
                    setChanged();
                    notifyObservers("muerto");
                }
                break;
            case MUERTO:
                break;
            case QUIETO:
                setFrameParado();
                break;
        }
    }

    private void setFrameParado() {
        switch (ultimoEstado) {
            case IZQUIERDA:
                frameActual = animIzquierda.getKeyFrame(0);
                return;
            case DERECHA:
                frameActual = animDerecha.getKeyFrame(0);
                return;
            case ARRIBA:
                frameActual = animArriba.getKeyFrame(0);
                return;
            case ABAJO:
                frameActual = GestorRecursos.getAtlas("aventureros").findRegion(nombre.toLowerCase() + "ParadoAbajo");
                return;
        }
    }

    protected void inicializarAtributos(int fuerza, int magia, int destreza, int espiritu, int vitalidad) {
        this.fuerza = fuerza;
        this.magia = magia;
        this.destreza = destreza;
        this.espiritu = espiritu;
        this.vitalidad = vitalidad;
    }

    private void calcularTiempoAccion() {
        tiempoAccion = TIEMPO_ACCION_BASE - (destreza * 0.2f);
    }

    public void atacar(Entidad objetivo) {
        float destinoX = 0, destinoY = 0;

        if (this instanceof Aventurero) {
            destinoX = objetivo.getX() + (objetivo.getRectangulo().width * 0.75f);
            if (this.nombre.equalsIgnoreCase("zak"))
                destinoY = objetivo.getY();
            else
                destinoY = objetivo.getY() + 30;

        }

        if (this instanceof Enemigo) {
            destinoX = objetivo.getX() - getRectangulo().width;
            destinoY = objetivo.getY();
        }

        posicionPrevia.x = posicion.x;
        posicionPrevia.y = posicion.y;
        posicion.set(destinoX, destinoY);

        tiempoAnimacion = 0;
        estadoActual = Estado.ATACANDO;

        GestorSonido.reproducirSonido(nombre + "Atacando", 0.5f);

        /* Prepara el render de fuentes para mostrar el daño realizado al atacar. */
        int danoRealizado = UtilsGdx.calcularDanoAtaqueBasico(this, objetivo);
        GestorFuentes.getInstance().setTexto(String.valueOf(danoRealizado));
        GestorFuentes.getInstance().setPosicion(objetivo);

        objetivo.vida.restar(danoRealizado);
    }

    public void ataqueEspecial(Entidad objetivo) {
        tiempoAnimacion = 0;
        estadoActual = Estado.ESPECIAL;

         /* Prepara el renderer de fuentes para mostrar el daño realizado al atacar. */
        int danoRealizado = UtilsGdx.calcularDanoAtaqueEspecial(this, objetivo);
        GestorFuentes.getInstance().setTexto(String.valueOf(danoRealizado));
        GestorFuentes.getInstance().setPosicion(objetivo);

        objetivo.vida.restar(danoRealizado);
    }

    /**
     * Metodo que calcula el rectangulo de la entidad , el cual se usará para comprobar interacciones en el mundo, ya
     * sean otros npcs, objetos , transiciones...
     * Las dimensiones del rectángulo se ajustarán a la dirección en la que se mueva la entidad.
     *
     * @return El rectangulo ajustado a la dirección en la que se mueve
     */
    public Rectangle getRectangulo() {
        if (ultimoEstado == Estado.ARRIBA)
           rectangulo.set(posicion.x, posicion.y + ALTO_SPRITE / 2, ANCHO_SPRITE, 1);

        if (ultimoEstado == Estado.ABAJO)
            rectangulo.set(posicion.x, posicion.y, ANCHO_SPRITE, 1);

        if (ultimoEstado == Estado.IZQUIERDA)
            rectangulo.set(posicion.x, posicion.y, 1, ALTO_SPRITE);

        if (ultimoEstado == Estado.DERECHA)
            rectangulo.set(posicion.x + ANCHO_SPRITE, posicion.y, 1, ALTO_SPRITE);

        if (ultimoEstado == Estado.ESPERANDO)
            rectangulo.set(posicion.x, posicion.y, frameActual.getRegionWidth(), frameActual.getRegionHeight());

        return rectangulo;
    }

    public void renderizar(SpriteBatch batch) {
        if (frameActual == null)
            return;

        batch.draw(frameActual, posicion.x, posicion.y, frameActual.getRegionWidth(), frameActual.getRegionHeight());
    }

    public String getNombre() {
        return nombre;
    }

    public void setEstadoActual(Estado estadoActual) {
        this.estadoActual = estadoActual;
    }

    public void setEstadoActual(final Estado estadoActual, float delay) {
        Timer.schedule(new Timer.Task() {
            @Override
            public void run() {
                tiempoAnimacion = 0;
                Entidad.this.setEstadoActual(estadoActual);
            }
        }, delay);
    }

    public void setUltimoEstado(Estado ultimoEstado) {
        this.ultimoEstado = ultimoEstado;
    }

    public void setMapaActual(Mapa mapaActual) {
        this.mapaActual = mapaActual;
    }

    public Image getRetrato() {
        if (retrato == null)
            cargarRetrato(nombre);

        return retrato;
    }

    public float getTiempoEsperado() {
        return tiempoEsperado;
    }

    public void resetearTiempoEsperado() {
        tiempoEsperado = 0;
    }

    public void acumularTiempoEsperado(float tiempo) {
        tiempoEsperado += tiempo;
    }

    public float getTiempoAccion() {
        return tiempoAccion;
    }

    public boolean estaListo() {
        return (tiempoEsperado > tiempoAccion) ? true : false;
    }

    public boolean estaMuerto() {
        if (estadoActual == Estado.MURIENDO)
            return true;

        if (estadoActual == Estado.MUERTO)
            return true;

        return false;
    }

    public float getX() {
        return posicion.x;
    }

    public float getY() {
        return posicion.y;
    }

    public float getAncho() {
        return frameActual.getRegionWidth();
    }

    public float getAlto() {
        return frameActual.getRegionHeight();
    }

    public boolean estaAtacando() {
        return estadoActual == Estado.ATACANDO;
    }

    public void setPosicion(float x, float y) {
        posicion.x = x;
        posicion.y = y;
    }

    public float getVelocidadX() {
        return velocidad.x;
    }

    public float getVelocidadY() {
        return velocidad.y;
    }

    public int getNivel() {
        return nivel;
    }

    public int getMagia() {
        return magia;
    }

    public int getFuerza() {
        return fuerza;
    }

    public int getDestreza() {
        return destreza;
    }

    public int getEspiritu() {
        return espiritu;
    }

    public int getVitalidad() {
        return vitalidad;
    }

    public void setTiempoEsperado(float tiempoEsperado) {
        this.tiempoEsperado = tiempoEsperado;
    }
}

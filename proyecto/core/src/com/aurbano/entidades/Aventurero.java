package com.aurbano.entidades;

import com.aurbano.Utilidades.UtilsGdx;
import com.aurbano.gestores.GestorRecursos;
import com.aurbano.gestores.GestorSonido;
import com.aurbano.objetos.*;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ProgressBar;

import static com.aurbano.entidades.Entidad.Estado.USAR_ELEMENTO;
import static com.aurbano.juego.Constantes.ATLAS_GUI;
import static com.aurbano.juego.Constantes.TIEMPO_ACCION_BASE;
import static com.aurbano.objetos.RegistroObjetos.*;

/**
 * Clase que representa un Personaje del grupo.
 *
 * @author Adrián Urbano
 */
public class Aventurero extends Entidad {
    /* Equipo */
    protected Arma arma;
    protected Armadura armadura;
    protected Accesorio accesorio;

    /* Imagen que se mostrará para representar al aventurero en los menús. */
    protected transient Image avatar;

    /* Referencia del objeto que representa el inventario compartido por el grupo de personajes. */
    protected transient Inventario inventario;

    /* Representa una barra de estado con valor actual y maximo, 0000 / 0000. */
    protected final BarraEstado experiencia;

    /* Las barras de progreso que marcan el ritmo de accion , y el ritmo del limite durante los encuentros. */
    protected transient ProgressBar barraAccion;
    protected transient ProgressBar barraLimite;

    public Aventurero(String nombre) {
        super(nombre);
        cargarAvatar();
        cargarAnimaciones();
        calcularTiempoAccion();

        nivel = 1;
        barraAccion = UtilsGdx.nuevaBarraProgreso();
        barraLimite = UtilsGdx.nuevaBarraProgreso();
        vida = new BarraEstado(2400, 2400);
        experiencia = new BarraEstado(1, 850);
        inventario = Inventario.getInstance();
        vestirEquipoInicial();
        inicializarAtributos(4, 4, 6, 4, 3);
    }

    protected void cargarAvatar() {
        String nombreRegion = "avatar" + nombre;
        AtlasRegion region = GestorRecursos.getAtlas(ATLAS_GUI).findRegion(nombreRegion);
        if (region == null)
            return;

        avatar = new Image(region);
    }

    public void equiparArma(Arma nuevaArma) {
        if (arma != null)
            arma.setEquipado(false);

        nuevaArma.setEquipado(true);
        arma = nuevaArma;
    }

    public void equiparArmadura(Armadura nuevaArmadura) {
        if (armadura != null)
            armadura.setEquipado(false);

        nuevaArmadura.setEquipado(true);
        armadura = nuevaArmadura;
    }

    public void equiparAccesorio(Accesorio nuevoAccesorio) {
        if (accesorio != null)
            accesorio.setEquipado(false);

        nuevoAccesorio.setEquipado(true);
        accesorio = nuevoAccesorio;
    }

    public void vestirEquipoInicial() {
        if (nombre.equalsIgnoreCase("lenn")) {
            equiparArma(inventario.getArma(RegistroObjetos.LANZA_CORTA));
            equiparArmadura(inventario.getArmadura(RegistroObjetos.ARMADURA_ACERO));
            equiparAccesorio(inventario.getAccesorio(RegistroObjetos.ANILLO_PLATA));
        }

        if (nombre.equalsIgnoreCase("zak")) {
            equiparArma(inventario.getArma(RegistroObjetos.ESPADA_ACERO));
            equiparArmadura(inventario.getArmadura(RegistroObjetos.ARMADURA_CUERO));
            equiparAccesorio(inventario.getAccesorio(RegistroObjetos.ANILLO_LENN));
        }
    }

    @Override
    public void cargarAnimaciones() {
        String aventurero = nombre.toLowerCase();

        animIzquierda = GestorRecursos.getAnimation(aventurero + "Izquierda");
        animDerecha = GestorRecursos.getAnimation(aventurero + "Derecha");
        animArriba = GestorRecursos.getAnimation(aventurero + "Arriba");
        animAbajo = GestorRecursos.getAnimation(aventurero + "Abajo");
        animEsperando = GestorRecursos.getAnimation(aventurero + "Esperando");

        animAtaque = GestorRecursos.getAnimation(aventurero + "Atacando");
        animMuerte = GestorRecursos.getAnimation(aventurero + "Muriendo");
        animElemento = GestorRecursos.getAnimation(aventurero + "Elemento");
        animEspecial = GestorRecursos.getAnimation(aventurero + "Especial");

        if (frameActual == null)
            frameActual = animEsperando.getKeyFrame(0);
    }

    public void usarElemento(Elemento seleccionado, Aventurero objetivo) {
        tiempoAnimacion = 0;
        estadoActual = USAR_ELEMENTO;

        Inventario inventario = Inventario.getInstance();
        int identificador = seleccionado.getId();

        switch (identificador) {
            case POCION:
                objetivo.getVida().sumar(100);
                seleccionado.reducirCantidad(1);
                break;
            case POCION_S:
                objetivo.getVida().sumar(500);
                seleccionado.reducirCantidad(1);
                break;
            case POCION_X:
                objetivo.getVida().sumar(1000);
                seleccionado.reducirCantidad(1);
                break;
            case ETER:
                objetivo.getMana().sumar(50);
                seleccionado.reducirCantidad(1);
                break;
            case ETER_TURBO:
                objetivo.getMana().sumar(150);
                seleccionado.reducirCantidad(1);
                break;
        }

        GestorSonido.reproducirSonido("usarElemento");
    }

    public String getNombre() {
        return nombre;
    }

    public int getNivel() {
        return nivel;
    }

    public BarraEstado getVida() {
        return vida;
    }

    public BarraEstado getMana() {
        return mana;
    }

    public BarraEstado getExperiencia() {
        return experiencia;
    }

    public Inventario getInventario() {
        if (inventario == null)
            inventario = Inventario.getInstance();

        return inventario;
    }

    public Image getAvatar() {
        if (avatar == null)
            cargarAvatar();

        return avatar;
    }

    public Image getRetrato() {
        if (retrato == null)
            retrato = GestorRecursos.nuevaImagen(ATLAS_GUI, "retrato" + nombre);

        return retrato;
    }

    public Arma getArma() {
        return arma;
    }

    public Armadura getArmadura() {
        return armadura;
    }

    public Accesorio getAccesorio() {
        return accesorio;
    }

    public int getFuerza() {
        return fuerza;
    }

    public int getDestreza() {
        return destreza;
    }

    public int getVitalidad() {
        return vitalidad;
    }

    public int getMagia() {
        return magia;
    }

    public int getEspiritu() {
        return espiritu;
    }

    public Vector2 getPosicion() {
        return posicion;
    }

    public void setPosicion(float x , float y) {
        posicion.x = x;
        posicion.y = y;
    }

    public void setPosicionDeBatalla(int posicionDeBatalla) {
        switch (posicionDeBatalla) {
            case 1:
                posicion.x = Gdx.graphics.getWidth() * 0.78f;
                posicion.y = Gdx.graphics.getHeight() * 0.42f;
                break;
            case 2:
                posicion.x = Gdx.graphics.getWidth() * 0.85f;
                posicion.y = Gdx.graphics.getHeight() * 0.3f;
                break;
        }
    }

    @Override
    public float getTiempoAccion() {
        calcularTiempoAccion();
        return tiempoAccion;
    }

    private void calcularTiempoAccion() {
        tiempoAccion = TIEMPO_ACCION_BASE - (destreza * 0.2f);
    }

    public ProgressBar getBarraAccion() {
        if (barraAccion == null)
            barraAccion = UtilsGdx.nuevaBarraProgreso();

        return barraAccion;
    }

    public ProgressBar getBarraLimite() {
        if (barraLimite == null)
            barraLimite = UtilsGdx.nuevaBarraProgreso();

        return barraLimite;
    }
}

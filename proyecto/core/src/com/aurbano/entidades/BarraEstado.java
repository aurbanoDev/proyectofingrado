package com.aurbano.entidades;

import com.aurbano.Utilidades.UtilsGdx;

import java.io.Serializable;

/**
 * Clase que representa una barra de estado (la salud de personaje, la barra de experiencia...)
 *
 * @author Adrián Urbano
 */
public class BarraEstado implements Serializable{

    private int valorMaximo;
    private int valorActual;

    /**
     * Constructor que establece el valor actual por defecto al valor máximo indicado como parámetro.
     *
     * @param valorMaximo
     */
    public BarraEstado(int valorMaximo) {
        this(valorMaximo, valorMaximo);
    }
    /**
     * Constructor que permite especificar el valor actual de la barra en el momento de su construcción.
     *
     * @param actual
     * @param valorMaximo
     */
    public BarraEstado(int actual, int valorMaximo) {
        this.valorActual = actual;
        this.valorMaximo = valorMaximo;
    }

    /**
     * Aumenta el valor actual de la barra
     *
     * @param cantidad que será aumentada
     */
    public void sumar(int cantidad) {
        int valor = valorActual + cantidad;

        if (valor > valorMaximo)
            valorActual = valorMaximo;
        else
            valorActual += cantidad;
    }

    /**
     * Reduce el valor actual de la barra
     *
     * @param cantidad que sera disminuida
     */
    public void restar(int cantidad) {
        int valor = valorActual - cantidad;

        if (valor < 0)
            valorActual = 0;
        else
            valorActual = valor;
    }

    public int getPorcentaje() {
        return valorActual * 100 / valorMaximo;
    }

    /**
     * Indica si el estado actual es inferior al 33%
     *
     * @return true si el nivel actual es inferior al 33%
     */
    public boolean isEstadoCritico() {
        if (getPorcentaje() < 33)
            return true;

        return false;
    }

    public int getValorMaximo() {
        return valorMaximo;
    }

    public int getValorActual() {
        return valorActual;
    }

    public String getValorActualFormateado() {
        if (valorActual > 999)
            return String.valueOf(valorActual);

        if (valorActual < 100)
            return "00" + valorActual;
        else
            return "0" + valorActual;
    }

    public void setValorMaximo(int valorMaximo) {
        this.valorMaximo = valorMaximo;
    }

    /**
     * Devuelve el estado completo de la barra con el formato : 0000 / 0000
     *
     * @return StringBuffer estado completo
     */
    public StringBuffer getEstadoCompleto() {
        StringBuffer estado = new StringBuffer(11);

        estado.append(UtilsGdx.formatearNumero(valorActual, 5));
        estado.append(UtilsGdx.separador());
        estado.append(UtilsGdx.formatearNumero(valorMaximo, 5));

        return estado;
    }
}

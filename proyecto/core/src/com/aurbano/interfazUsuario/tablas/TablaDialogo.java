package com.aurbano.interfazUsuario.tablas;

import com.badlogic.gdx.scenes.scene2d.ui.Container;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

import static com.aurbano.juego.Constantes.*;

/**
 * Created by adriu on 05/01/2016.
 */
public class TablaDialogo extends TablaAzul {

    private Container<Image> portaRetrato;
    private TablaSeleccionEmergente opciones;

    private boolean solicitaRenderizado;

    public TablaDialogo() {
        left().bottom();
        setFondoDialogo();
        setBounds(X_FULL_SCREEN, 0, ANCHO_TOTAL, ALTO_TOTAL * 0.28f);
        setVisible(false);

        inicializarContenedores();
    }

    private void inicializarContenedores() {
        portaRetrato = new Container<>();
        opciones = new TablaSeleccionEmergente();
        add(portaRetrato);
        add(opciones).padTop(120).padLeft(50);
    }

    public void actualizarTexto(String... lineas) {
        opciones.clear();
        opciones.cargarContenido(lineas);
        opciones.mostrar(false, false);
    }

    public void actualizarTexto(int indice, String... lineas) {
        opciones.clear();
        opciones.cargarContenido(indice, lineas);
        opciones.mostrar(false, false);
        opciones.seleccionar(indice);
    }

    public void actualizarRetrato(Image retrato) {
        retrato.scaleBy(0);
        portaRetrato.width(retrato.getWidth());
        portaRetrato.height(retrato.getHeight());
        portaRetrato.setActor(retrato);
    }

    public boolean isSolicitaRenderizado() {
        return solicitaRenderizado;
    }

    public void setSolicitaRenderizado(boolean solicitaRenderizado) {
        this.solicitaRenderizado = solicitaRenderizado;
    }
}

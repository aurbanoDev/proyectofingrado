package com.aurbano.interfazUsuario.tablas;

import com.aurbano.interfazUsuario.celdas.CeldaAbstracta;
import com.aurbano.interfazUsuario.celdas.CeldaBasica;
import com.badlogic.gdx.utils.Array;

/**
 * Created by adriu on 01/12/2015.
 */
public class TablaSeleccionEmergente extends TablaSeleccion<String> {

    public TablaSeleccionEmergente() {
        this(0, 0);
    }

    public TablaSeleccionEmergente(float x, float y) {
        super(x, y);

        defaults().left();
        setVisible(false);
        activarSkin();
    }

    @Override
    public void cargarContenido(Array<String> contenido) {
        clear();
        celdas = new CeldaBasica[contenido.size];

        for (int i = 0; i < contenido.size; i++) {
            String seccion = contenido.get(i);
            celdas[i] = new CeldaBasica(seccion);
            add(celdas[i]).row();
        }

        seleccionar(0);
    }

    public void cargarContenidoConCabecera(Array<String> opciones, String... cabecera) {
        clear();
        celdas = new CeldaBasica[opciones.size];

        for (String linea : cabecera)
            add(linea).row();

        for (int i = 0; i < opciones.size; i++) {
            String seccion = opciones.get(i);
            celdas[i] = new CeldaBasica(seccion);
            add(celdas[i]).row();
        }

        seleccionarPrimeraCelda();
    }

    public void cargarContenido(String... args) {
        clear();
        celdas = new CeldaBasica[args.length];

        for (int i = 0; i < args.length; i++) {
            celdas[i] = new CeldaBasica(args[i]);
            add(celdas[i]).row();
        }
    }

    public void cargarContenido(int indice, String... args) {
        celdas = new CeldaBasica[args.length];

        for (int i = 0; i < args.length; i++) {
            celdas[i] = new CeldaBasica(args[i]);
            if (i < indice)
                add(celdas[i]).row();
            else
                add(celdas[i]).padLeft(50).row();
        }
    }

    public CeldaBasica[] getCeldas() {
        return (CeldaBasica[]) celdas;
    }

    public void ocultar() {
        limpiarSeleccion();
        setVisible(false);
    }

    public void mostrar() {
        dimensionarPorCeldas();
        seleccionar(0);
        definirFondo();
        setVisible(true);
    }

    public void mostrar(boolean primeraCeldaSeleccionada, boolean fondo) {
        dimensionarPorCeldas();
        if (primeraCeldaSeleccionada)
            seleccionarPrimeraCelda();

        if (fondo)
            definirFondo();

        setVisible(true);
    }

    private void dimensionarPorCeldas() {
        float alto = celdas[0].getPrefHeight() * celdas.length;

        float ancho = 0;
        for (CeldaAbstracta celda : celdas) {
            if (celda.getPrefWidth() > ancho)
                ancho = celda.getPrefWidth();
        }

        setSize(ancho, alto * 1.05f);
    }

    private void definirFondo() {
        float padIzquierdo = celdas[0].getCells().first().getPrefWidth();

        setFondoAzul();
        getBackground().setRightWidth(padIzquierdo);
    }
}

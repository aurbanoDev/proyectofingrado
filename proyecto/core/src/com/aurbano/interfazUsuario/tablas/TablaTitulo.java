package com.aurbano.interfazUsuario.tablas;

import static com.aurbano.juego.Constantes.X_TITULO;
import static com.aurbano.juego.Constantes.Y_TITULO;

/**
 * Created by adriu on 29/12/2015.
 */
public class TablaTitulo extends TablaAzul {

    public TablaTitulo(String titulo) {
        activarSkin();
        setFondoAzul();
        add(titulo);
        dimensionarPara(getChildren().first());
        posicionar();
    }

    private void posicionar() {
        float x = X_TITULO - getWidth();
        float y = Y_TITULO - getHeight();

        setPosition(x, y);
    }

}

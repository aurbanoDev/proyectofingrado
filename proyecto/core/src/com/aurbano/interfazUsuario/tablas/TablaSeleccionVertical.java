package com.aurbano.interfazUsuario.tablas;

import com.aurbano.interfazUsuario.celdas.CeldaAbstracta;
import com.aurbano.interfazUsuario.celdas.CeldaBasica;
import com.badlogic.gdx.utils.Array;

import java.util.ArrayList;

/**
 * Created by adriu on 06/01/2016.
 */
public class TablaSeleccionVertical extends TablaSeleccion<String> {

    public TablaSeleccionVertical() {
        this(0, 0, 0, 0);
    }

    public TablaSeleccionVertical(float x, float y, float anchura, float altura) {
        super(x, y, anchura, altura);
    }

    public void setEstiloNegrita() {
        for (CeldaAbstracta celda : celdas)
            ((CeldaBasica) celda).setEstiloNegrita();
    }

    @Override
    public void cargarContenido(Array<String> contenido) {
        celdas = new CeldaBasica[contenido.size];

        for (int i = 0; i < contenido.size; i++) {
            celdas[i] = new CeldaBasica(contenido.get(i));
            add(celdas[i]).row();
        }

        seleccionar(0);
    }

    public void cargarContenido(Array<String> contenido, float separacion) {
        celdas = new CeldaBasica[contenido.size];

        for (int i = 0; i < contenido.size; i++) {
            celdas[i] = new CeldaBasica(contenido.get(i));
            add(celdas[i]).row();
        }

        seleccionar(0);
    }

    public void cargarContenido(ArrayList<String> contenido, float pad) {
        celdas = new CeldaBasica[contenido.size()];

        for (int i = 0; i < contenido.size(); i++) {
            celdas[i] = new CeldaBasica(contenido.get(i));
            add(celdas[i]).pad(pad).row();
        }

        seleccionar(0);
    }

}

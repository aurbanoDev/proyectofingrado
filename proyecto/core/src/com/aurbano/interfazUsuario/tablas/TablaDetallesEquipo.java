package com.aurbano.interfazUsuario.tablas;

import com.aurbano.entidades.Aventurero;

/**
 * Created by adriu on 08/01/2016.
 */
public class TablaDetallesEquipo extends TablaAzul {

    private TablaRanuras arma;
    private TablaRanuras armadura;
    private TablaRanuras accesorio;

    public TablaDetallesEquipo() {
        arma = new TablaRanuras();
        armadura = new TablaRanuras();
        accesorio = new TablaRanuras();

        activarSkin();
        defaults().left().height(35).padLeft(50).padTop(5);
    }

    public void cargarDatos(Aventurero aventurero) {
        clear();

        arma.cargarRanuras(aventurero.getArma());
        armadura.cargarRanuras(aventurero.getArmadura());
        accesorio.cargarRanuras(aventurero.getAccesorio());

        add("Arma", "sigla").width(200);
        add(aventurero.getArma().getNombre());
        add(arma).padLeft(85).row();

        add("Armadura", "sigla");
        add(aventurero.getArmadura().getNombre());
        add(armadura).padLeft(85).row();

        add("Accesorio", "sigla");
        add(aventurero.getAccesorio().getNombre());
        add(accesorio).padLeft(85).row();
    }
}

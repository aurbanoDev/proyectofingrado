package com.aurbano.interfazUsuario.tablas.tablasScroll;


import com.aurbano.gestores.GestorRecursos;
import com.aurbano.interfazUsuario.tablas.TablaSeleccion;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Array;

/**
 * Created by adriu on 29/11/2015.
 */
public abstract class TablaDeslizable<T> extends TablaSeleccion<T> {

    protected Table contenedorCeldas;
    protected ScrollPane scrollPanel;
    protected Array<T> lista;

    public TablaDeslizable() {
        this(0, 0, 0, 0);
    }

    public TablaDeslizable(float x , float y , float anchura , float altura) {
        setBounds(x, y, anchura, altura);

        contenedorCeldas = new Table();
        scrollPanel = new ScrollPane(contenedorCeldas, GestorRecursos.getEstiloScroll());
        scrollPanel.setFadeScrollBars(false);
        scrollPanel.setVariableSizeKnobs(false);
        scrollPanel.setScrollingDisabled(true, false);
        add(scrollPanel).fillX().expandX();
    }

    public abstract void cargarContenido(Array<T> contenido);

    @Override
    public void seleccionar(int i) {
        super.seleccionar(i);
        scrollPanel.scrollTo(celdas[i].getX(), celdas[i].getY(), celdas[i].getWidth(), celdas[i].getHeight());
    }

    public void recuperarUltimaSeleccion() {
        super.seleccionar(indiceCeldaSeleccionada);
    }
}

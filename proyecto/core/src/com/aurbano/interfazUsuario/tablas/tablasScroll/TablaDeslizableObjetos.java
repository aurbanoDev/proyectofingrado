package com.aurbano.interfazUsuario.tablas.tablasScroll;

import com.aurbano.interfazUsuario.celdas.CeldaObjeto;
import com.aurbano.objetos.Inventario;
import com.aurbano.objetos.Objeto;
import com.aurbano.objetos.TipoOrden;
import com.badlogic.gdx.utils.Array;

/**
 * Created by adriu on 29/11/2015.
 */
public class TablaDeslizableObjetos extends TablaDeslizable<Objeto> {

    public TablaDeslizableObjetos() {
        this(0, 0, 0, 0);

        padTop(20);
        right().top();
        setFondoAzul();
    }

    public TablaDeslizableObjetos(float x, float y, float anchura, float altura) {
        super(x, y, anchura, altura);
    }

    public void ordenarObjetos(TipoOrden tipo) {
        Inventario inventario = Inventario.getInstance();
        inventario.ordenarElementos(tipo);

        cargarContenido(inventario.getListaCompleta());
    }

    @Override
    public void cargarContenido(Array<Objeto> contenido) {
        celdas = new CeldaObjeto[contenido.size];
        contenedorCeldas.clear();
        lista = contenido;

        for (int i = 0; i < contenido.size; i++) {
            celdas[i] = new CeldaObjeto(contenido.get(i));
            contenedorCeldas.add(celdas[i]).row();
        }
    }

}

package com.aurbano.interfazUsuario.tablas.tablasScroll;

import com.aurbano.interfazUsuario.celdas.CeldaMateria;
import com.aurbano.objetos.Materia;
import com.badlogic.gdx.utils.Array;

/**
 * Created by infe on 02/06/2016.
 */
public class TablaDeslizableMagia extends TablaDeslizable<Materia> {

    public TablaDeslizableMagia() {
        super(0, 0, 0, 0);
        padBottom(8);
        padRight(25);
        padTop(6);
        center().top();
        defaults().left();
        setFondoDialogo();
        setVisible(false);
    }

    @Override
    public void cargarContenido(Array<Materia> contenido) {
        lista = contenido;
        contenedorCeldas.clear();
        celdas = new CeldaMateria[contenido.size];

        for (int i = 0; i < contenido.size; i++) {
            celdas[i] = new CeldaMateria(contenido.get(i));
            contenedorCeldas.add(celdas[i]).row();
        }
    }

    @Override
    public void seleccionarPrimeraCelda() {
        super.seleccionarPrimeraCelda();
        scrollPanel.setScrollY(0);
    }
}

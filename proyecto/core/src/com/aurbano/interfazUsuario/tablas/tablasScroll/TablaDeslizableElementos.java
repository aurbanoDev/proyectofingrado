package com.aurbano.interfazUsuario.tablas.tablasScroll;

import com.aurbano.interfazUsuario.celdas.CeldaElemento;
import com.aurbano.objetos.Elemento;
import com.badlogic.gdx.utils.Array;

/**
 * Created by infe on 16/05/2016.
 */
public class TablaDeslizableElementos extends TablaDeslizable<Elemento> {

    public TablaDeslizableElementos() {
        super(0, 0, 0, 0);
        padBottom(8);
        padRight(25);
        padTop(6);
        center().top();
        defaults().left();
        setFondoDialogo();
        setVisible(false);
    }

    @Override
    public void cargarContenido(Array<Elemento> contenido) {
        lista = contenido;
        contenedorCeldas.clear();
        celdas = new CeldaElemento[contenido.size];

        for (int i = 0; i < contenido.size; i++) {
            celdas[i] = new CeldaElemento(contenido.get(i));
            contenedorCeldas.add(celdas[i]).row();
        }
    }

    @Override
    public void seleccionarPrimeraCelda() {
        super.seleccionarPrimeraCelda();
        scrollPanel.setScrollY(0);
    }
}

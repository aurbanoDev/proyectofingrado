package com.aurbano.interfazUsuario.tablas.cabeceras;

import com.aurbano.entidades.Aventurero;
import com.aurbano.gestores.GestorSonido;
import com.aurbano.interfazUsuario.celdas.CeldaEquipo;
import com.aurbano.interfazUsuario.tablas.TablaAzul;
import com.aurbano.objetos.Equipo;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

import java.util.ArrayList;

/**
 * Created by adriu on 12/12/2015.
 */
public class CabeceraEquipo extends TablaAzul {

    private Image avatar;
    private Label nombre, nivel, salud, mana;
    private Label siglasNivel, siglasHP, siglasMP;
    private Table contenedor;

    private int indiceCeldaSeleccionada;
    private CeldaEquipo arma, armadura, accesorio;
    private ArrayList<CeldaEquipo> celdas;


    public CabeceraEquipo() {
        celdas = new ArrayList<CeldaEquipo>();
        arma = new CeldaEquipo();
        armadura = new CeldaEquipo();
        accesorio = new CeldaEquipo();

        arma.setSiglasArma();
        armadura.setSiglasArmadura();
        accesorio.setSiglasAccesorio();

        celdas.add(arma);
        celdas.add(armadura);
        celdas.add(accesorio);

        inicializarComponentes();
        setFondoAzul();
        left();
    }

    private void inicializarComponentes() {
        siglasNivel = new Label("NV", estiloSigla);
        siglasHP = new Label("HP", estiloSigla);
        siglasMP = new Label("MP", estiloSigla);
        nombre = new Label("", estiloBlanco);
        nivel = new Label("", estiloBlanco);
        salud = new Label("", estiloBlanco);
        mana = new Label("", estiloBlanco);

        contenedor = new Table();
        contenedor.defaults().left();
        contenedor.defaults().height(35);
    }

    public void insertarComponentes() {
        contenedor.add(nombre).row();
        contenedor.add(siglasNivel);
        contenedor.add(nivel);
        contenedor.add().spaceRight(25);
        contenedor.add(arma).row();
        contenedor.add(siglasHP);
        contenedor.add(salud);
        contenedor.add().spaceRight(25);
        contenedor.add(armadura).row();
        contenedor.add(siglasMP);
        contenedor.add(mana);
        contenedor.add().spaceRight(25);
        contenedor.add(accesorio).row();

        add(avatar).width(140);
        add().spaceRight(25);
        add(contenedor);
    }

    public void refrescar(Aventurero aventurero) {
        avatar = aventurero.getAvatar();
        nombre.setText(aventurero.getNombre());
        nivel.setText(String.valueOf(aventurero.getNivel()));
        salud.setText(aventurero.getVida().getEstadoCompleto());
        mana.setText(aventurero.getMana().getEstadoCompleto());

        arma.setContenido(aventurero.getArma());
        armadura.setContenido(aventurero.getArmadura());
        accesorio.setContenido(aventurero.getAccesorio());

        arma.refrescar();
        armadura.refrescar();
        accesorio.refrescar();

        clear();
        contenedor.clear();
        insertarComponentes();
    }

    public void celdaSiguiente() {
        GestorSonido.reproducirSonido("desplazarCursor");
        int indice = indiceCeldaSeleccionada + 1;
        if (indice > celdas.size() - 1) {
            seleccionarPrimeraCelda();
            return;
        }

        seleccionar(indice);
    }

    public void celdaAnterior() {
        GestorSonido.reproducirSonido("desplazarCursor");
        int indice = indiceCeldaSeleccionada - 1;
        if (indice < 0) {
            seleccionarUltimaCelda();
            return;
        }

        seleccionar(indice);
    }

    public void seleccionarUltimaCelda() {
        seleccionar(celdas.size() - 1);
        celdas.get(0).setSeleccion(false);
    }

    public void seleccionarPrimeraCelda() {
        seleccionar(0);
        celdas.get(celdas.size() - 1).setSeleccion(false);
    }

    public void seleccionar(int indice) {
        celdas.get(indice).setSeleccion(true);

        int ultimaSeleccion = indiceCeldaSeleccionada;
        indiceCeldaSeleccionada = indice;

        if (indice != ultimaSeleccion)
            celdas.get(ultimaSeleccion).setSeleccion(false);
    }

    public int getIndiceCeldaSeleccionada() {
        return indiceCeldaSeleccionada;
    }

    public Equipo getContenidoSeleccionado() {
        return celdas.get(getIndiceCeldaSeleccionada()).getContenido();
    }
}

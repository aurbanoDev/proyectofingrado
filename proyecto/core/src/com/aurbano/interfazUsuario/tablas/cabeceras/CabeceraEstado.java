package com.aurbano.interfazUsuario.tablas.cabeceras;

import com.aurbano.entidades.Aventurero;
import com.aurbano.interfazUsuario.celdas.CeldaResumenPersonaje;
import com.aurbano.interfazUsuario.tablas.TablaAzul;

/**
 * Created by adriu on 08/01/2016.
 */
public class CabeceraEstado extends TablaAzul {

    private CeldaResumenPersonaje celdaPersonaje;

    public CabeceraEstado() {
        left();
        activarSkin();
        setFondoAzul();
    }

    public void cargarDatos(Aventurero aventurero) {
        celdaPersonaje = new CeldaResumenPersonaje(aventurero);
        clear();
        add(celdaPersonaje).padBottom(10);
    }
}

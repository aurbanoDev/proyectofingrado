package com.aurbano.interfazUsuario.tablas;

import com.aurbano.interfazUsuario.celdas.CeldaAbstracta;
import com.aurbano.interfazUsuario.celdas.CeldaOpcion;
import com.badlogic.gdx.utils.Array;

/**
 * Created by infe on 12/05/2016.
 */
public class TablaSeleccionOpciones extends TablaSeleccion<String> {

    @Override
    public void cargarContenido(Array<String> contenido) {
        clear();
        celdas = new CeldaOpcion[contenido.size];

        for (int i = 0; i < contenido.size; i++) {
            celdas[i] = new CeldaOpcion(contenido.get(i));
            add(celdas[i]).row();
        }

        seleccionar(0);
    }

    public CeldaOpcion getCeldaSeleccionada() {
        CeldaOpcion celda = (CeldaOpcion) celdas[indiceCeldaSeleccionada];
        return celda;
    }

    public String getContenido(int indice) {
        CeldaOpcion celda = (CeldaOpcion) celdas[indice];
        return celda.getOpcionSeleccionada();
    }
}

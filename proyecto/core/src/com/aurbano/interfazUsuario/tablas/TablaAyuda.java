package com.aurbano.interfazUsuario.tablas;

import com.badlogic.gdx.scenes.scene2d.ui.Label;

/**
 * Created by adriu on 12/12/2015.
 */
public class TablaAyuda extends TablaAzul {

    private Label etiqueta;

    public TablaAyuda() {
        this(0, 0, 0, 0);
    }

    public TablaAyuda(String textoAyuda) {
        this(0, 0, 0, 0);

        etiqueta.setText(textoAyuda);
    }

    public TablaAyuda(float x, float y , float ancho, float alto) {
        etiqueta = new Label("", estiloBlanco);
        add(etiqueta);

        left();
        setFondoAzul();
        setBounds(x, y, ancho, alto);
        padLeft(30);
    }

    public void mostrarAyuda(String nuevoTexto) {
        if (etiqueta.getText().equals(nuevoTexto))
            return;

        etiqueta.setText(nuevoTexto);
    }
}

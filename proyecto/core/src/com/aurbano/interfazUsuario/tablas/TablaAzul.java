package com.aurbano.interfazUsuario.tablas;

import com.aurbano.Utilidades.UtilsGdx;
import com.aurbano.gestores.GestorRecursos;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;

import static com.aurbano.juego.Constantes.*;

/**
 * Created by adriu on 14/12/2015.
 */
public class TablaAzul extends Table {

    public TablaAzul(float x, float y, float ancho , float alto) {
        super();
        setBounds(x, y ,ancho , alto);
    }

    public TablaAzul() {
        this(0, 0 , 0 , 0);
    }

    protected static LabelStyle estiloCyan = GestorRecursos.getEstiloEtiqueta(FUENTE_CYAN);
    protected static LabelStyle estiloSigla = GestorRecursos.getEstiloEtiqueta(FUENTE_SIGLA_CYAN);
    protected static LabelStyle estiloBlanco = GestorRecursos.getEstiloEtiqueta(FUENTE_BLANCA);
    protected static LabelStyle estiloMenuPrincipal = GestorRecursos.getEstiloEtiqueta(FUENTE_MENU_PRINCIPAL);
    protected static LabelStyle estiloDesactivado = GestorRecursos.getEstiloEtiqueta("desactivada");

    public void setMargenIzquierdo(float margen) {
        left();
        padLeft(margen);
    }

    public void setFondoAzul() {
        setBackground(UtilsGdx.getNinePatch("nine/marco.9.png"));
    }

    public void setFondoDialogo() {
        setBackground(UtilsGdx.getNinePatch("nine/marcoTranslucido.png", 20));
        getBackground().setBottomHeight(8);
    }

    public void activarSkin() {
        setSkin(GestorRecursos.getTablaSkin());
    }

    public void mostrarMensajeExperienciaGanada(int cantidadExperiencia) {
        clear();
        add("Has ganado : " + cantidadExperiencia + " px   ", "fuente", Color.WHITE);
    }

    public void mostrarMensajeOroGanado(int cantidadOro) {
        clear();
        Image sacoOro = GestorRecursos.nuevaImagen(ATLAS_GUI, "oro");

        add("Has ganado : " + cantidadOro + "  ");
        add(sacoOro);
    }

    public void mostrarMensajePulsaParaContinuar() {
        clear();
        add("Para Continuar Pulsa   ", "fuente", Color.WHITE);
        add("A  " , "sigla");
    }

    public void dimensionarPara(Actor referencia) {
        setWidth(referencia.getWidth() * 1.4f);
        setHeight(referencia.getHeight() * 2f);
    }

    public void dimensionarPorFilas() {
        float anchura = 0, altura = 0;

        for (Cell celda : getCells()) {
            anchura += celda.getPrefWidth();
            altura = Math.max(altura, celda.getPrefHeight());

            if (celda.isEndRow())
                break;
        }

        if (getRows() == 1)
            altura *= 2;
        else
            altura = altura * getRows() * 1.5f;

        setSize(anchura * 1.25f, altura);
    }

    public void dimensionarPorFilas(float margenAncho, float margenAlto) {
        float anchura = 0, altura = 0;

        for (Cell celda : getCells()) {
            anchura += celda.getPrefWidth();
            altura = Math.max(altura, celda.getPrefHeight());

            if (celda.isEndRow())
                break;
        }

        if (getRows() == 1)
            altura *= margenAlto;
        else
            altura = altura * getRows() * margenAlto;

        setSize(anchura * margenAncho, altura);
    }

    public void renderizar(SpriteBatch batch) {
        draw(batch, 1);
    }
}

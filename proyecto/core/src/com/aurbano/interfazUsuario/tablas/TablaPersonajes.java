package com.aurbano.interfazUsuario.tablas;

import com.aurbano.entidades.Aventurero;
import com.aurbano.interfazUsuario.celdas.CeldaPersonaje;
import com.aurbano.interfazUsuario.celdas.CeldaResumenPersonaje;
import com.badlogic.gdx.utils.Array;

/**
 * Created by adriu on 30/11/2015.
 */
public class TablaPersonajes extends TablaSeleccion<Aventurero> {

    public TablaPersonajes() {
        this(0, 0 , 0 , 0);
    }

    public TablaPersonajes(float x, float y, float anchura, float altura) {
        super(x, y, anchura, altura);

        top().left();
        setFondoAzul();
    }

    @Override
    public void cargarContenido(Array<Aventurero> contenido) {
        clear();
        celdas = new CeldaPersonaje[contenido.size];

        for (int i = 0; i < contenido.size; i++) {
            celdas[i] = new CeldaPersonaje(contenido.get(i));
            add(celdas[i]).padTop(14).padBottom(14).row();
        }
    }

    public void cargarAventurerosConBarraPX(Array<Aventurero> contenido) {
        clear();
        celdas = new CeldaResumenPersonaje[contenido.size];

        for (int i = 0; i < contenido.size; i++) {
            celdas[i] = new CeldaResumenPersonaje(contenido.get(i));
            add(celdas[i]).padTop(20).padBottom(20).row();
        }

        ajustarAlineamiento();
    }

    private void ajustarAlineamiento() {
        if (celdas.length == 3)
            center().left();
        else
            top().left();
    }
}

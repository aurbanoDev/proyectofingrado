package com.aurbano.interfazUsuario.tablas;

import com.aurbano.Utilidades.UtilsGdx;
import com.aurbano.entidades.Aventurero;

/**
 * Created by adriu on 08/01/2016.
 */
public class TablaAtributos extends TablaAzul {

    public TablaAtributos() {
        activarSkin();
        defaults().left().width(200).height(35).padBottom(8).padLeft(50);
    }

    public void cargarAtributos(Aventurero aventurero) {
        clear();

        add("Fuerza", "sigla");
        add(String.valueOf(aventurero.getFuerza())).row();

        add("Magia", "sigla");
        add(String.valueOf(aventurero.getMagia())).row();

        add("Espiritu", "sigla");
        add(String.valueOf(aventurero.getEspiritu())).row();

        add("Destreza", "sigla");
        add(String.valueOf(aventurero.getDestreza())).row();

        add("Vitalidad", "sigla");
        add(String.valueOf(aventurero.getVitalidad())).row();
    }

    public void cargarEstadisticasEquipo(Aventurero aventurero) {
        clear();

        add("Ataque", "cyan");
        add(String.valueOf(UtilsGdx.calcularAtaque(aventurero))).row();

        add("Ataque %", "cyan");
        add(String.valueOf(UtilsGdx.calcularPorcentajeAtaque(aventurero))).row();

        add("Defensa", "cyan");
        add(String.valueOf(UtilsGdx.calcularDefensa(aventurero))).row();

        add("Defensa %", "cyan");
        add(String.valueOf(UtilsGdx.calcularPorcentajeDefensa(aventurero))).row();

        add("Atq Mag", "cyan");
        add(String.valueOf(UtilsGdx.calcularAtaqueMagico(aventurero))).row();

        add("Def Mag", "cyan");
        add(String.valueOf(UtilsGdx.calcularDefensaMagica(aventurero))).row();
    }

    public void cargarAmbas(Aventurero aventurero) {
        clear();

        add("Fuerza", "sigla");
        add(String.valueOf(aventurero.getFuerza()));
        add("Ataque", "sigla");
        add(String.valueOf(UtilsGdx.calcularAtaque(aventurero))).row();

        add("Magia", "sigla");
        add(String.valueOf(aventurero.getMagia()));
        add("Ataque %", "sigla");
        add(String.valueOf(UtilsGdx.calcularPorcentajeAtaque(aventurero))).row();

        add("Espiritu", "sigla");
        add(String.valueOf(aventurero.getEspiritu()));
        add("Defensa", "sigla");
        add(String.valueOf(UtilsGdx.calcularDefensa(aventurero))).row();

        add("Destreza", "sigla");
        add(String.valueOf(aventurero.getDestreza()));
        add("Defensa %", "sigla");
        add(String.valueOf(UtilsGdx.calcularPorcentajeDefensa(aventurero))).row();

        add("Vitalidad", "sigla");
        add(String.valueOf(aventurero.getVitalidad()));
        add("Atq Mag", "sigla");
        add(String.valueOf(UtilsGdx.calcularAtaqueMagico(aventurero))).row();
    }
}

package com.aurbano.interfazUsuario.tablas;

import com.aurbano.Utilidades.UtilsGdx;
import com.aurbano.gestores.GestorRecursos;
import com.badlogic.gdx.scenes.scene2d.ui.Container;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;

import static com.aurbano.juego.Constantes.ATLAS_GUI;

/**
 * Created by adriu on 30/12/2015.
 */
public class TablaTiempoOro extends TablaAzul {

    private Label etiquetaOro;
    private Label etiquetaTiempo;

    public TablaTiempoOro() {
        left();
        setFondoAzul();
        setSize(210 , 110);
        defaults().padTop(5).padBottom(5);

        inicializarComponenetes();
    }

    private void inicializarComponenetes() {
        Image reloj = GestorRecursos.nuevaImagen(ATLAS_GUI, "reloj");
        Image sacoOro = GestorRecursos.nuevaImagen(ATLAS_GUI, "oro");

        etiquetaOro = new Label("10000", estiloBlanco);
        etiquetaTiempo = new Label("01:00:00", estiloBlanco);

        Container<Label> contenedorOro = new Container<Label>(etiquetaOro);
        Container<Label> contenedorTiempo = new Container<Label>(etiquetaTiempo);
        contenedorOro.right();
        contenedorTiempo.right();

        add(sacoOro).width(40).height(40);
        add(contenedorOro).width(140).row();
        add(reloj).width(40).height(40);
        add(contenedorTiempo).width(140).row();
    }

    public void actualizarOro(int cantidad) {
        etiquetaOro.setText(String.valueOf(cantidad));
    }

    public void actualizarTiempoJugado(int tiempoJuego) {
        String tiempoJugado = UtilsGdx.formatearTiempoJuego(tiempoJuego);
        etiquetaTiempo.setText(tiempoJugado);
    }
}

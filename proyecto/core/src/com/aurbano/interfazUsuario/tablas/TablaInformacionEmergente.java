package com.aurbano.interfazUsuario.tablas;

import com.aurbano.entidades.Entidad;
import com.aurbano.gestores.GestorRecursos;
import com.aurbano.objetos.Dinero;
import com.aurbano.objetos.Materia;
import com.aurbano.objetos.Objeto;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.Array;

/**
 *
 * @author Adrian Urbano
 */
public class TablaInformacionEmergente extends TablaSeleccionEmergente {

    private float tiempoRenderizado;
    private boolean solicitaRenderizado;
    private boolean renderizadoTemporal;

    public TablaInformacionEmergente() {
        left();
        setFondoAzul();
        defaults().pad(3);
        setSkin(GestorRecursos.getTablaSkin());

        solicitaRenderizado = false;
        renderizadoTemporal = false;
    }

    public void visualizarContenido(Array<Objeto> contenido) {
        clear();
        for (Objeto objeto : contenido)
            visualizarObjeto(objeto);

        dimensionarPorFilas();
        activarModoRenderizado();
    }

    public void visualizarTexto(String... lineas) {
        clear();
        for (String linea : lineas)
            add(linea).row();

        dimensionarPorFilas();
        activarModoRenderizado();
    }

    public void visualizarAyuda(String texto) {
        clear();
        add(texto).center();
        setVisible(true);
    }

    public void activarSkinBatalla() {

        float x = Gdx.graphics.getWidth() * 0.15f;
        float y = Gdx.graphics.getHeight() * 0.85f;
        float ancho = Gdx.graphics.getWidth() * 0.7f;
        float alto = Gdx.graphics.getHeight() * 0.1f;

        setBounds(x, y , ancho , alto);
        center();
        activarModoRenderizado();
    }

    private void visualizarObjeto(Objeto objeto) {
        if (objeto instanceof Materia) {
            visualizarMateria(objeto);
            return;
        }

        if (objeto instanceof Dinero) {
            visualizarDinero(objeto);
            return;
        }

        add(objeto.getIcono()).width(26).height(26).padBottom(4);
        add(" " + objeto.getNombre()).left().height(20);
        add(objeto.getCantidadComoString());
        add().row();
    }

    private void activarModoRenderizado() {
        tiempoRenderizado = 0;
        solicitaRenderizado = true;
    }

    public void desactivarModoRenderizado() {
        solicitaRenderizado = false;
        clear();
    }

    public void actualizarTiempoRenderizado() {
        if (!solicitaRenderizado || !renderizadoTemporal)
            return;

        tiempoRenderizado += Gdx.graphics.getDeltaTime();
        if (tiempoRenderizado > 2)
            desactivarModoRenderizado();
    }

    private void visualizarMateria(Objeto objeto) {
        add(objeto.getIcono()).width(24).height(24).padLeft(4);
        add().spaceRight(6);
        add("Materia " + "\"" + objeto.getNombre() + "\"" + " Recibida").right().height(20);
        add().row();
    }

    private void visualizarDinero(Objeto objeto) {
        add(objeto.getIcono()).width(24).height(24).padLeft(4);
        add(String.valueOf(objeto.getCantidad())).left().height(20).padLeft(11);
        add().row();
    }

    public void setPosicionRelativa(Entidad entidad) {
        setPosition(entidad.getX(), entidad.getY() + 75);
    }

    public void setRenderizadoTemporal(boolean renderizadoTemporal) {
        this.renderizadoTemporal = renderizadoTemporal;
    }

    public boolean solicitaRenderizado() {
        return solicitaRenderizado;
    }
}

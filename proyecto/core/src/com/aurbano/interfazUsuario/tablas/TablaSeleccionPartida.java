package com.aurbano.interfazUsuario.tablas;

import com.aurbano.interfazUsuario.celdas.CeldaPartidaGuardada;
import com.aurbano.persistencia.EstadoMundo;
import com.badlogic.gdx.math.Vector;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

/**
 * Created by infe on 07/05/2016.
 */
public class TablaSeleccionPartida extends TablaSeleccion<EstadoMundo> {

    @Override
    public void cargarContenido(Array<EstadoMundo> contenido) {

        clear();
        celdas = new CeldaPartidaGuardada[contenido.size];

        for (int i = 0; i < contenido.size; i++) {
            celdas[i] = new CeldaPartidaGuardada(contenido.get(i));
            add(celdas[i]).row();
        }

        celdas[0].setSeleccion(true);
    }

    public Vector2 getPosicionCeldaSeleccionada() {
        Vector2 posicion = new Vector2();
        posicion.x = celdas[indiceCeldaSeleccionada].getX() + 160;
        posicion.y = celdas[indiceCeldaSeleccionada].getY();

        return posicion;
    }
}

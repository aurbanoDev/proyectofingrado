package com.aurbano.interfazUsuario.tablas;

import com.aurbano.interfazUsuario.celdas.CeldaBasica;
import com.badlogic.gdx.utils.Array;

/**
 * Created by adriu on 30/11/2015.
 */
public class TablaSeleccionHorizontal extends TablaSeleccion<String> {

    public TablaSeleccionHorizontal() {
        this(0, 0, 0, 0);
    }

    public TablaSeleccionHorizontal(float x, float y, float anchura, float altura) {
        super(x, y, anchura, altura);

        defaults().padLeft(35);
        setFondoAzul();
    }

    @Override
    public void cargarContenido(Array<String> contenido) {
        celdas = new CeldaBasica[contenido.size];

        for (int i = 0; i < contenido.size; i++) {
            celdas[i] = new CeldaBasica(contenido.get(i));
            add(celdas[i]);
        }

        seleccionar(0);
    }
}

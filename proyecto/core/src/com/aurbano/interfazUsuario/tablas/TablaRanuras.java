package com.aurbano.interfazUsuario.tablas;

import com.aurbano.gestores.GestorRecursos;
import com.aurbano.objetos.Equipo;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

import static com.aurbano.juego.Constantes.ATLAS_GUI;

/**
 * Created by adriu on 13/12/2015.
 */
public class TablaRanuras extends TablaAzul {

    private Ranura ranura1;
    private Ranura ranura2;
    private Ranura ranura3;
    private Ranura ranura4;

    public TablaRanuras() {
        inicializar();

        add(ranura1);
        add(ranura2);
        add(ranura3);
        add(ranura4);
    }

    private void inicializar() {
        ranura1 = new Ranura();
        ranura2 = new Ranura();
        ranura3 = new Ranura();
        ranura4 = new Ranura();

        left(); padLeft(2);
        defaults().height(35);
        defaults().width(77);
    }

    public void cargarRanuras(Equipo equipo) {
        if (equipo == null)
            return;

        int ranurasDobles = equipo.getRanurasDobles();
        int ranurasSimples = equipo.getRanurasSimples();

        limpiarRanuras();
        cargarRanurasDobles(ranurasDobles);
        cargarRanurasSimples(ranurasSimples, ranurasDobles);
    }

    private void limpiarRanuras() {
        ranura1.clear();
        ranura2.clear();
        ranura3.clear();
        ranura4.clear();
    }

    private void cargarRanurasDobles(int ranurasDobles) {

        if (ranurasDobles == 1) {
            ranura1.ranuraDobleUnida();
        } else if (ranurasDobles == 2) {
            ranura1.ranuraDobleUnida();
            ranura2.ranuraDobleUnida();
        } else if (ranurasDobles == 3) {
            ranura1.ranuraDobleUnida();
            ranura2.ranuraDobleUnida();
            ranura3.ranuraDobleUnida();
        } else if (ranurasDobles == 4) {
            ranura1.ranuraDobleUnida();
            ranura2.ranuraDobleUnida();
            ranura3.ranuraDobleUnida();
            ranura4.ranuraDobleUnida();
        }
    }

    private void cargarRanurasSimples(int ranurasSimples, int ranurasDobles) {
        if (ranurasSimples == 1) {

            if (ranurasDobles == 0)
                ranura1.ranuraIndividual();
            else if (ranurasDobles == 1)
                ranura2.ranuraIndividual();
            else if (ranurasDobles == 2)
                ranura3.ranuraIndividual();
            else
                ranura4.ranuraIndividual();

        } else if (ranurasSimples == 2) {

            if (ranurasDobles == 0)
                ranura1.ranuraDoble();
            else if (ranurasDobles == 1)
                ranura2.ranuraDoble();
            else if (ranurasDobles == 2)
                ranura3.ranuraDoble();
            else
                ranura4.ranuraDoble();

        } else if (ranurasSimples == 3) {

            if (ranurasDobles == 0) {
                ranura1.ranuraDoble();
                ranura2.ranuraDoble();
            } else if (ranurasDobles == 1) {
                ranura2.ranuraDoble();
                ranura3.ranuraIndividual();
            } else if (ranurasDobles == 2) {
                ranura3.ranuraDoble();
                ranura4.ranuraIndividual();
            }

        } else if (ranurasSimples == 4) {

            ranura1.ranuraDoble();
            ranura2.ranuraDoble();
        } else if (ranurasSimples == 5) {
            ranura1.ranuraDoble();
            ranura2.ranuraDoble();
            ranura3.ranuraIndividual();
        } else if (ranurasSimples == 6) {
            ranura1.ranuraDoble();
            ranura2.ranuraDoble();
            ranura3.ranuraDoble();
        } else if (ranurasSimples == 7) {
            ranura1.ranuraDoble();
            ranura2.ranuraDoble();
            ranura3.ranuraDoble();
            ranura4.ranuraIndividual();
        } else if (ranurasSimples == 8) {
            ranura1.ranuraDoble();
            ranura2.ranuraDoble();
            ranura3.ranuraDoble();
            ranura4.ranuraDoble();
        }
    }

    public class Ranura extends Stack {
        private Image separador;
        private Image ranuraIzquierda;
        private Image ranuraDerecha;
        private Table contenedor;

        private boolean contieneMateria;

        public Ranura() {
            ranuraIzquierda = GestorRecursos.nuevaImagen(ATLAS_GUI, "ranuraVacia");
            ranuraDerecha = GestorRecursos.nuevaImagen(ATLAS_GUI, "ranuraVacia");
            separador = GestorRecursos.nuevaImagen(ATLAS_GUI, "separador");

            contenedor = new Table();
            contenedor.padLeft(3).padRight(3).padTop(3).padBottom(2);
            contenedor.add(ranuraIzquierda);
            contenedor.add(ranuraDerecha);
        }

        public void ranuraIndividual() {
            contenedor.clear();
            contenedor.add(ranuraIzquierda);
            contenedor.add().space(38);
            add(contenedor);
        }

        public void ranuraDobleUnida() {
            contenedor.clear();
            contenedor.add(ranuraIzquierda);
            contenedor.add().space(3);
            contenedor.add(ranuraDerecha);
            add(separador);
            add(contenedor);
        }

        public void ranuraDoble() {
            contenedor.clear();
            contenedor.add(ranuraIzquierda);
            contenedor.add().space(3);
            contenedor.add(ranuraDerecha);
            add(contenedor);
        }
    }
}

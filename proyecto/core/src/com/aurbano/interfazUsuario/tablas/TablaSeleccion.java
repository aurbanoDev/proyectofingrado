package com.aurbano.interfazUsuario.tablas;

import com.aurbano.gestores.GestorSonido;
import com.aurbano.interfazUsuario.celdas.CeldaAbstracta;
import com.aurbano.interfazUsuario.celdas.CeldaBasica;
import com.badlogic.gdx.utils.Array;

/**
 * Created by adriu on 29/11/2015.
 */
public abstract class TablaSeleccion<T> extends TablaAzul {

    protected int indiceCeldaSeleccionada;
    protected CeldaAbstracta<T>[] celdas;


    public TablaSeleccion() {
       this(0, 0, 0, 0);
    }

    public TablaSeleccion(float x, float y) {
        this(x, y, 0, 0);
    }

    public TablaSeleccion(float x, float y, float anchura, float altura) {
        setBounds(x, y, anchura, altura);
        activarSkin();
    }

    public abstract void cargarContenido(Array<T> contenido);


    public void celdaSiguiente() {
        if (celdas.length == 1)
            return;

        int indice = indiceCeldaSeleccionada + 1;
        if (indice > celdas.length - 1) {
            seleccionarPrimeraCelda();
            return;
        }

        seleccionar(indice);
        GestorSonido.reproducirSonido("desplazarCursor");
    }

    public void celdaAnterior() {
        if (celdas.length == 1)
            return;

        int indice = indiceCeldaSeleccionada - 1;
        if (indice < 0) {
            seleccionarUltimaCelda();
            return;
        }

        seleccionar(indice);
        GestorSonido.reproducirSonido("desplazarCursor");
    }

    public void seleccionarUltimaCelda() {
        for (CeldaAbstracta celda : celdas)
            celda.setSeleccion(false);

        seleccionar(celdas.length - 1);
        GestorSonido.reproducirSonido("desplazarCursor");
    }

    public void seleccionarPrimeraCelda() {
        seleccionar(0);
        GestorSonido.reproducirSonido("desplazarCursor");
    }

    public void seleccionarPrimeraCelda(boolean sonido) {
        seleccionar(0);
        if (sonido)
            GestorSonido.reproducirSonido("desplazarCursor");
    }

    public void seleccionar(int indice) {
        for (CeldaAbstracta celda : celdas)
            celda.setSeleccion(false);

        celdas[indice].setSeleccion(true);
        indiceCeldaSeleccionada = indice;
    }

    public int getIndiceCeldaSeleccionada() {
        return indiceCeldaSeleccionada;
    }

    public T getContenidoActual() {
        return celdas[indiceCeldaSeleccionada].getContenido();
    }

    public void limpiarSeleccion() {
        celdas[indiceCeldaSeleccionada].setSeleccion(false);
    }

    public boolean seleccionadaOpcion(String nombreSeccion) {
        return getContenidoActual().equals(nombreSeccion);
    }

    public CeldaAbstracta<T>[] getCeldas() {
        return  celdas;
    }

    public void desactivarCelda(int indiceCelda) {
        CeldaBasica celda = (CeldaBasica) celdas[indiceCelda];
        celda.setEstiloActivado(false);
    }

}

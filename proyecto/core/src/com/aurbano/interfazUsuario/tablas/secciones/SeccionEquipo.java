package com.aurbano.interfazUsuario.tablas.secciones;

import com.aurbano.entidades.Aventurero;
import com.aurbano.interfazUsuario.tablas.TablaAzul;
import com.aurbano.interfazUsuario.tablas.TablaRanuras;
import com.aurbano.interfazUsuario.tablas.tablasScroll.TablaDeslizableObjetos;
import com.aurbano.objetos.*;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Array;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by adriu on 13/12/2015.
 */
public class SeccionEquipo extends TablaAzul {

    //TODO refactorizar toda esta clase usando skin en las tablas.

    private TablaDeslizableObjetos listaEquipo;
    private TablaAzul valores;
    private TablaAzul crecimiento;
    private TablaRanuras ranuras;
    private Table contenedorInferior;

    private Label siglaAtaque, siglaAtaqueMagico, siglaPorcentajeAtaque;
    private Label siglaDefensa, siglaDefensaMagica, siglaPorcentajeDefensa;

    private Label ataque, ataqueMagico, porcentajeAtaque;
    private Label defensa, defensaMagica, porcentajeDefensa;

    private Label ataqueFuturo, ataqueMagicoFuturo, porcentajeAtaqueFuturo;
    private Label defensaFuturo, defensaMagicaFuturo, porcentajeDefensaFuturo;

    private Label separador1, separador2, separador3, separador4, separador5, separador6;

    private Aventurero aventurero;

    public SeccionEquipo() {
        inicializarTablas();
        inicializarEtiquetas();
        insertarEtiquetas();

        contenedorInferior.add(crecimiento).row();
        contenedorInferior.add(valores).row();

        add(contenedorInferior).expand().fill();
        add(listaEquipo).expand().fill();
    }

    private void inicializarTablas() {
        valores = new TablaAzul();
        ranuras = new TablaRanuras();
        crecimiento = new TablaAzul();
        contenedorInferior = new Table();
        listaEquipo = new TablaDeslizableObjetos();

        crecimiento.setMargenIzquierdo(35);
        crecimiento.add(new Label("Ranuras", estiloCyan)).width(185);
        crecimiento.add(ranuras).width(310);

        valores.setFondoAzul();
        crecimiento.setFondoAzul();

        contenedorInferior.defaults().expand().fill();
    }

    private void inicializarEtiquetas() {
        siglaAtaque = new Label("Ataque", estiloCyan);
        siglaAtaqueMagico = new Label("Atq Mag", estiloCyan);
        siglaPorcentajeAtaque = new Label("Ataque %", estiloCyan);
        siglaDefensa = new Label("Defensa", estiloCyan);
        siglaDefensaMagica = new Label("Def Mag", estiloCyan);
        siglaPorcentajeDefensa = new Label("Defensa %", estiloCyan);

        ataque = new Label("", estiloBlanco);
        ataqueMagico = new Label("", estiloBlanco);
        porcentajeAtaque = new Label("", estiloBlanco);
        defensa = new Label("", estiloBlanco);
        defensaMagica = new Label("", estiloBlanco);
        porcentajeDefensa = new Label("", estiloBlanco);

        separador1 = new Label("->", estiloCyan);
        separador2 = new Label("->", estiloCyan);
        separador3 = new Label("->", estiloCyan);
        separador4 = new Label("->", estiloCyan);
        separador5 = new Label("->", estiloCyan);
        separador6 = new Label("->", estiloCyan);

        ataqueFuturo = new Label("12", estiloBlanco);
        ataqueMagicoFuturo = new Label("22", estiloBlanco);
        porcentajeAtaqueFuturo = new Label("33", estiloBlanco);
        defensaFuturo = new Label("1", estiloBlanco);
        defensaMagicaFuturo = new Label("55", estiloBlanco);
        porcentajeDefensaFuturo = new Label("21", estiloBlanco);

        mostrarSeparadores(false);
        mostrarAtributosFuturos(false);
    }

    private void insertarEtiquetas() {
        valores.padLeft(35);
        valores.defaults().width(120).padTop(5).padBottom(5);
        valores.left();

        valores.add(siglaAtaque).width(200);
        valores.add(ataque);
        valores.add(separador1);
        valores.add(ataqueFuturo);
        valores.row();

        valores.add(siglaPorcentajeAtaque).width(200);
        valores.add(porcentajeAtaque);
        valores.add(separador2);
        valores.add(porcentajeAtaqueFuturo);
        valores.row();

        valores.add(siglaDefensa).width(200);
        valores.add(defensa);
        valores.add(separador3);
        valores.add(defensaFuturo);
        valores.row();

        valores.add(siglaPorcentajeDefensa).width(200);
        valores.add(porcentajeDefensa);
        valores.add(separador4);
        valores.add(porcentajeDefensaFuturo);
        valores.row();

        valores.add(siglaAtaqueMagico).width(200);
        valores.add(ataqueMagico);
        valores.add(separador5);
        valores.add(ataqueMagicoFuturo);
        valores.row();

        valores.add(siglaDefensaMagica).width(200);
        valores.add(defensaMagica);
        valores.add(separador6);
        valores.add(defensaMagicaFuturo);
        valores.row();
    }

    public void actualizarAtributos(Aventurero aventurero) {
        this.aventurero = aventurero;
        actualizarAtributos(mapearAtributos(aventurero));
    }

    public void actualizarAtributos(Map<String, Integer> mapa) {
        ataque.setText(String.valueOf(mapa.get("ataque")));
        ataqueMagico.setText(String.valueOf(mapa.get("ataqueMagico")));
        porcentajeAtaque.setText(String.valueOf(mapa.get("porcentajeAtaque")));
        defensa.setText(String.valueOf(mapa.get("defensa")));
        defensaMagica.setText(String.valueOf(mapa.get("defensaMagica")));
        porcentajeDefensa.setText(String.valueOf(mapa.get("porcentajeDefensa")));
    }

    public void actualizarAtributosFuturos(Map<String, Integer> mapa) {
        ataqueFuturo.setText(String.valueOf(mapa.get("ataque")));
        ataqueMagicoFuturo.setText(String.valueOf(mapa.get("ataqueMagico")));
        porcentajeAtaqueFuturo.setText(String.valueOf(mapa.get("porcentajeAtaque")));
        defensaFuturo.setText(String.valueOf(mapa.get("defensa")));
        defensaMagicaFuturo.setText(String.valueOf(mapa.get("defensaMagica")));
        porcentajeDefensaFuturo.setText(String.valueOf(mapa.get("porcentajeDefensa")));
    }

    private Map<String, Integer> mapearAtributosActuales() {
        return mapearAtributos(aventurero);
    }

    private Map<String, Integer> mapearAtributos(Aventurero aventurero) {
        int punteria = aventurero.getArma().getPorcentajeAtaque();
        int reflejos = aventurero.getArmadura().getPorcentajeDefensa();
        int fuerza = (int) (aventurero.getFuerza() * 0.1f);
        int destreza = (int) (aventurero.getDestreza() * 0.1f);

        HashMap<String, Integer> mapa = new HashMap<String, Integer>();

        mapa.put("ataque", aventurero.getFuerza() + aventurero.getArma().getAtaque());
        mapa.put("ataqueMagico", aventurero.getMagia() + aventurero.getArma().getAtaqueMagico());
        mapa.put("porcentajeAtaque", fuerza + punteria);
        mapa.put("defensa", aventurero.getDestreza() + aventurero.getArmadura().getDefensa());
        mapa.put("defensaMagica", aventurero.getEspiritu() + aventurero.getArmadura().getDefensaMagica());
        mapa.put("porcentajeDefensa", destreza + reflejos);

        return mapa;
    }

    private Map<String, Integer> mapearAtributos(Aventurero aventurero, Arma arma, Armadura amd, Accesorio acc) {
        int punteria = arma.getPorcentajeAtaque();
        int reflejos = amd.getPorcentajeDefensa();
        int fuerza = (int) (aventurero.getFuerza() * 0.1f);
        int destreza = (int) (aventurero.getDestreza() * 0.1f);

        HashMap<String, Integer> mapa = new HashMap<String, Integer>();

        mapa.put("ataque", aventurero.getFuerza() + arma.getAtaque());
        mapa.put("ataqueMagico", aventurero.getMagia() + arma.getAtaqueMagico());
        mapa.put("porcentajeAtaque", fuerza + punteria);
        mapa.put("defensa", aventurero.getDestreza() + amd.getDefensa());
        mapa.put("defensaMagica", aventurero.getEspiritu() + amd.getDefensaMagica());
        mapa.put("porcentajeDefensa", destreza + reflejos);

        return mapa;
    }

    public void cargarRanuras(Equipo equipo) {
        ranuras.cargarRanuras(equipo);
    }

    public void mostrarSeparadores(boolean visible) {
        separador1.setVisible(visible);
        separador2.setVisible(visible);
        separador3.setVisible(visible);
        separador4.setVisible(visible);
        separador5.setVisible(visible);
        separador6.setVisible(visible);
    }

    public void mostrarAtributosFuturos(boolean visible) {
        ataqueFuturo.setVisible(visible);
        defensaFuturo.setVisible(visible);
        ataqueMagicoFuturo.setVisible(visible);
        defensaMagicaFuturo.setVisible(visible);
        porcentajeAtaqueFuturo.setVisible(visible);
        porcentajeDefensaFuturo.setVisible(visible);
    }

    public void mostrarComparacion(Equipo equipo) {
        Map<String, Integer> futurosAtributos;
        if (equipo instanceof Arma) {
            Arma arma = (Arma) equipo;
            futurosAtributos = mapearAtributos(aventurero, arma, aventurero.getArmadura(), aventurero.getAccesorio());
            colorearAtributos(futurosAtributos);
            actualizarAtributosFuturos(futurosAtributos);
        }

        if (equipo instanceof Armadura) {
            Armadura armadura = (Armadura) equipo;
            futurosAtributos = mapearAtributos(aventurero, aventurero.getArma(), armadura, aventurero.getAccesorio());
            colorearAtributos(futurosAtributos);
            actualizarAtributosFuturos(futurosAtributos);
        }

        if (equipo instanceof Accesorio) {
            Accesorio accesorio = (Accesorio) equipo;
            futurosAtributos = mapearAtributos(aventurero, aventurero.getArma(), aventurero.getArmadura(), accesorio);
            colorearAtributos(futurosAtributos);
            actualizarAtributosFuturos(futurosAtributos);
        }

        mostrarSeparadores(true);
        mostrarAtributosFuturos(true);
    }

    private void colorearAtributos(Map<String, Integer> futurosAtributos) {
        Map<String, Integer> atributosActuales = mapearAtributosActuales();

        int valorActual, valorFuturo;

        valorActual = atributosActuales.get("ataque");
        valorFuturo = futurosAtributos.get("ataque");

        if (valorActual > valorFuturo)
            ataqueFuturo.setColor(Color.RED);
        else if (valorActual < valorFuturo)
            ataqueFuturo.setColor(Color.YELLOW);
        else
            ataqueFuturo.setColor(Color.WHITE);

        valorActual = atributosActuales.get("defensa");
        valorFuturo = futurosAtributos.get("defensa");

        if (valorActual > valorFuturo)
            defensaFuturo.setColor(Color.RED);
        else if (valorActual < valorFuturo)
            defensaFuturo.setColor(Color.YELLOW);
        else
            defensaFuturo.setColor(Color.WHITE);

        valorActual = atributosActuales.get("ataqueMagico");
        valorFuturo = futurosAtributos.get("ataqueMagico");

        if (valorActual > valorFuturo)
            ataqueMagicoFuturo.setColor(Color.RED);
        else if (valorActual < valorFuturo)
            ataqueMagicoFuturo.setColor(Color.YELLOW);
        else
            ataqueMagicoFuturo.setColor(Color.WHITE);

        valorActual = atributosActuales.get("defensaMagica");
        valorFuturo = futurosAtributos.get("defensaMagica");

        if (valorActual > valorFuturo)
            defensaMagicaFuturo.setColor(Color.RED);
        else if (valorActual < valorFuturo)
            defensaMagicaFuturo.setColor(Color.YELLOW);
        else
            defensaMagicaFuturo.setColor(Color.WHITE);

        valorActual = atributosActuales.get("porcentajeAtaque");
        valorFuturo = futurosAtributos.get("porcentajeAtaque");

        if (valorActual > valorFuturo)
            porcentajeAtaqueFuturo.setColor(Color.RED);
        else if (valorActual < valorFuturo)
            porcentajeAtaqueFuturo.setColor(Color.YELLOW);
        else
            porcentajeAtaqueFuturo.setColor(Color.WHITE);

        valorActual = atributosActuales.get("porcentajeDefensa");
        valorFuturo = futurosAtributos.get("porcentajeDefensa");

        if (valorActual > valorFuturo)
            porcentajeDefensaFuturo.setColor(Color.RED);
        else if (valorActual < valorFuturo)
            porcentajeDefensaFuturo.setColor(Color.YELLOW);
        else
            porcentajeDefensaFuturo.setColor(Color.WHITE);

    }

    public void listarArmas() {
        Array<Objeto> lista = new Array<Objeto>(aventurero.getInventario().getArmas());
        listaEquipo.cargarContenido(lista);
    }

    public void listarArmaduras() {
        Array<Objeto> lista = new Array<Objeto>(aventurero.getInventario().getArmaduras());
        listaEquipo.cargarContenido(lista);
    }

    public void listarAccesorios() {
        Array<Objeto> lista = new Array<Objeto>(aventurero.getInventario().getAccesorios());
        listaEquipo.cargarContenido(lista);
    }

    public Equipo getEquipoSeleccionado() {
        return (Equipo) listaEquipo.getContenidoActual();
    }

    public TablaDeslizableObjetos getListaEquipo() {
        return listaEquipo;
    }
}

package com.aurbano.interfazUsuario.menus;

import com.aurbano.interfazUsuario.celdas.CeldaBasica;
import com.aurbano.interfazUsuario.tablas.TablaAyuda;
import com.aurbano.interfazUsuario.tablas.TablaPersonajes;
import com.aurbano.interfazUsuario.tablas.TablaSeleccionEmergente;
import com.aurbano.interfazUsuario.tablas.TablaTiempoOro;
import com.badlogic.gdx.utils.Array;

import static com.aurbano.juego.Constantes.*;

/**
 * Created by adriu on 29/12/2015.
 */
public class MenuResumen {

     public TablaSeleccionEmergente tablaSecciones;
     public TablaPersonajes personajes;
     public TablaAyuda tablaPie;
     public TablaTiempoOro tablaTiempoOro;
     public FaseMenuResumen faseActual;

    public enum FaseMenuResumen {
        SELECCION_SECCION, SELECCION_AVENTURERO, EN_ESPERA, SALIR
    }

    public MenuResumen() {
        inicializarPie();
        inicialiarTablaSecciones();
        inicializarTablaTiempoOro();

        faseActual = FaseMenuResumen.SELECCION_SECCION;
        personajes = new TablaPersonajes
                (X_FULL_SCREEN, Y_FULL_SCREEN + tablaPie.getHeight() * 0.65f, ANCHO_TOTAL * 0.9f, ALTO_TOTAL * 0.885f);
    }

    private void inicialiarTablaSecciones() {
        tablaSecciones = new TablaSeleccionEmergente();
        tablaSecciones.cargarContenido(new Array<>(TITULOS_SECCIONES));
        tablaSecciones.desactivarCelda(1);
        tablaSecciones.desactivarCelda(3);

        float x = X_FULL_SCREEN + ANCHO_TOTAL - tablaSecciones.getPrefWidth();
        float y = Y_FULL_SCREEN + ALTO_TOTAL - tablaSecciones.getPrefHeight() * 1.1f;

        tablaSecciones.setPosition(x, y);
        tablaSecciones.mostrar();
    }

    private  void inicializarPie() {
        tablaPie = new TablaAyuda("Puesto Avanzado En el Bosque");
        tablaPie.setBounds(X_FULL_SCREEN + ANCHO_TOTAL / 2, Y_FULL_SCREEN, ANCHO_TOTAL / 2, ALTO_TOTAL / 12);
    }

    private void inicializarTablaTiempoOro() {
        tablaTiempoOro = new TablaTiempoOro();
        tablaTiempoOro.setPosition(X_FULL_SCREEN + ANCHO_TOTAL - tablaTiempoOro.getWidth(), Y_FULL_SCREEN + tablaPie.getHeight());
    }

    public void setFaseActual(FaseMenuResumen faseActual) {
        this.faseActual = faseActual;
    }

    public void setGuardado(boolean activado) {
        for (CeldaBasica seccion : tablaSecciones.getCeldas()) {
            if (seccion.getContenido().equals("Guardar"))
                seccion.setEstiloActivado(activado);
        }
    }

    public boolean estaGuardadoActivado() {
        for (CeldaBasica seccion : tablaSecciones.getCeldas()) {
            if (seccion.getContenido().equals("Guardar"))
                return seccion.estaActivada();
        }

        return false;
    }

}

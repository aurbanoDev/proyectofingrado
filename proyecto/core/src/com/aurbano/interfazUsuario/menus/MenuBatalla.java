package com.aurbano.interfazUsuario.menus;

import com.aurbano.Utilidades.UtilsGdx;
import com.aurbano.entidades.Aventurero;
import com.aurbano.entidades.EfectoEspecial;
import com.aurbano.entidades.Enemigo;
import com.aurbano.entidades.Entidad;
import com.aurbano.gestores.GestorFuentes;
import com.aurbano.gestores.GestorPantallas;
import com.aurbano.gestores.GestorRecursos;
import com.aurbano.gestores.GestorSonido;
import com.aurbano.gestores.controles.GestorControles;
import com.aurbano.interfazUsuario.tablas.TablaAzul;
import com.aurbano.interfazUsuario.tablas.TablaInformacionEmergente;
import com.aurbano.interfazUsuario.tablas.TablaSeleccionVertical;
import com.aurbano.interfazUsuario.tablas.tablasScroll.TablaDeslizableElementos;
import com.aurbano.interfazUsuario.tablas.tablasScroll.TablaDeslizableMagia;
import com.aurbano.juego.Constantes;
import com.aurbano.objetos.Elemento;
import com.aurbano.objetos.Inventario;
import com.aurbano.pantallas.Pantalla;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Timer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Observable;
import java.util.Observer;

import static com.aurbano.interfazUsuario.menus.MenuBatalla.FaseMenuBatalla.*;
import static com.aurbano.juego.Constantes.ALTO_TOTAL;
import static com.aurbano.juego.Constantes.ATLAS_GUI;

/**
 * Created by adriu on 21/01/2016.
 */
public class MenuBatalla extends TablaAzul implements Observer {

    private TablaAzul nombres;
    private TablaAzul detalles;
    private TablaSeleccionVertical tablaAcciones;
    private TablaDeslizableElementos tablaElementos;
    private TablaDeslizableMagia tablaMagias;
    private TablaInformacionEmergente cabeceraAyuda;
    /**
     * El miembro del grupo al que le toca jugar.
     */
    private Aventurero aventureroConTurnoDisponible;
    /**
     * Enemigo seleccionado como objetivo
     */
    private Enemigo enemigoSeleccionado;
    /**
     * Miembro del grupo seleccionado como objetivo
     */
    private Aventurero aventureroSeleccionado;

    /**
     * Enemigos del encuentro
     */
    private Array<Enemigo> enemigos;
    private Array<Aventurero> grupo;

    private int experienciaObtenida;
    private int dineroObtenido;

    private Image cursorEnemigos;
    private Image cursorAliados;

    /**
     * La fase en la que se encuentra el menu de batalla.
     */
    private FaseMenuBatalla faseActual;

    private GestorControles controles;
    private EfectoEspecial efectoEspecial;

    private boolean turnoResolviendose;
    private boolean combateFinalizado;

    public enum FaseMenuBatalla {
        SELECCION, MAGIA, INVOCACION, ELEMENTOS, ELECCION_OBJETIVO, GAME_OVER, ELECCION_ALIADO
    }

    public MenuBatalla() {
        setBounds(0, 1, Gdx.graphics.getWidth(), ALTO_TOTAL * 0.28f);
        defaults().height(this.getHeight());

        inicializar();
    }

    private void inicializar() {
        cabeceraAyuda = new TablaInformacionEmergente();
        cabeceraAyuda.activarSkinBatalla();

        faseActual = SELECCION;
        controles = GestorControles.getInstance();
        efectoEspecial = EfectoEspecial.getInstance();
        efectoEspecial.addObserver(this);

        cursorEnemigos = new Image(GestorRecursos.getAtlas(ATLAS_GUI).findRegion("cursorI"));
        cursorAliados = new Image(GestorRecursos.getAtlas(ATLAS_GUI).findRegion("cursorD"));
        cursorEnemigos.setVisible(false);
        cursorAliados.setVisible(false);

        nombres = new TablaAzul();
        nombres.setFondoDialogo();
        nombres.activarSkin();
        nombres.defaults().left().padLeft(25).padBottom(12);
        nombres.top().left();

        detalles = new TablaAzul();
        detalles.setFondoDialogo();
        detalles.activarSkin();
        detalles.defaults().left().padBottom(12);
        detalles.top().left();

        tablaAcciones = new TablaSeleccionVertical();
        tablaAcciones.left();
        tablaAcciones.defaults().left();
        tablaAcciones.cargarContenido(new ArrayList<>(Arrays.asList(Constantes.COMANDOS_BATALLA)), 0);
        tablaAcciones.desactivarCelda(2);
        tablaAcciones.setWidth(180);
        tablaAcciones.setHeight(this.getHeight());
        tablaAcciones.setPosition(this.getWidth() * 0.4f - tablaAcciones.getWidth(), this.getY());
        tablaAcciones.setFondoDialogo();
        tablaAcciones.getBackground().setLeftWidth(-15);
        tablaAcciones.setVisible(false);

        tablaElementos = new TablaDeslizableElementos();
        tablaElementos.setBounds(this.getX(), tablaAcciones.getY(), this.getWidth() * 0.4f, tablaAcciones.getHeight());
        tablaElementos.getBackground().setRightWidth(-25);

        tablaMagias = new TablaDeslizableMagia();
        tablaMagias.setBounds(this.getX(), tablaAcciones.getY(), this.getWidth() * 0.4f, tablaAcciones.getHeight());
        tablaMagias.getBackground().setRightWidth(-25);

        add(nombres).width(this.getWidth() * 0.4f);
        add(detalles).width(this.getWidth() * 0.6f);
    }

    /**
     * Carga los enemigos del combate aleatorio, registrando la experiencia y el dinero
     * que se obtendran en caso de derrotarlos.
     * @param enemigos
     */
    public void cargarEnemigos(Array<Enemigo> enemigos) {
        this.enemigos = enemigos;

        dineroObtenido = 0;
        experienciaObtenida = 0;

        combateFinalizado = false;
        tablaAcciones.seleccionarPrimeraCelda(false);
        cabeceraAyuda.setVisible(false);
        actualizarCabecera();

        for (Enemigo enemigo : enemigos) {
            dineroObtenido += enemigo.getPremioDinero();
            experienciaObtenida += enemigo.getPremioExperiencia();
            enemigo.addObserver(this);
        }
    }
    /**
     * Carga los miembros del grupo, rellenando con sus datos la tabla de información.
     * @param grupo
     */
    public void cargarGrupo(Array<Aventurero> grupo) {
        this.grupo = grupo;

        nombres.clear();
        detalles.clear();

        inicializarCabeceras();
        for (int i = 0; i < grupo.size; i++) {
            grupo.get(i).addObserver(this);
            cargarDatosDeAventurero(grupo.get(i));
        }
    }

    private void  inicializarCabeceras() {
        TablaAzul cabecera = new TablaAzul();
        cabecera.activarSkin();

        cabecera.add().spaceRight(35);
        cabecera.add("HP", "sigla");
        cabecera.add().spaceRight(145);
        cabecera.add("MP", "sigla");
        cabecera.add().spaceRight(115);
        cabecera.add("Limite", "sigla");
        cabecera.add().spaceRight(105);
        cabecera.add("Tiempo", "sigla");

        nombres.add("Nombre", "sigla").row();
        detalles.add(cabecera).row();
    }

    private void cargarDatosDeAventurero(Aventurero aventurero) {
        nombres.add(aventurero.getNombre()).row();

        TablaAzul celda = new TablaAzul();
        celda.activarSkin();
        celda.add().spaceRight(35);

        String vidaActual = aventurero.getVida().getValorActualFormateado();
        if (aventurero.getVida().isEstadoCritico())
            celda.add(vidaActual, "fuente", Color.YELLOW).width(60);
        else
            celda.add(vidaActual).width(60);

        celda.add("/" + aventurero.getVida().getValorMaximo());
        celda.add().spaceRight(50);

        String manaActual = UtilsGdx.formatearNumero(aventurero.getMana().getValorActual(), 3).toString();
        if (aventurero.getMana().isEstadoCritico())
            celda.add(manaActual, "fuente", Color.YELLOW);
        else
            celda.add(manaActual);

        celda.add("/" + aventurero.getMana().getValorMaximo());
        celda.add().spaceRight(50);

        celda.add(aventurero.getBarraLimite());
        celda.add().spaceRight(50);
        celda.add(aventurero.getBarraAccion());

        detalles.add(celda).row();
    }

    public void actualizarProgresoTurnoGrupo(Array<Aventurero> grupo, float delta) {
        if (turnoResolviendose || combateFinalizado)
            return;

        for (Aventurero aventurero : grupo) {
            if (aventurero.estaListo() && !aventurero.estaMuerto())
                turnoPreparado(aventurero);

            aventurero.acumularTiempoEsperado(delta);
            int progreso = UtilsGdx.calcularPorcentaje(aventurero.getTiempoEsperado(), aventurero.getTiempoAccion());
            aventurero.getBarraAccion().setValue(progreso);
        }
    }

    public void actualizarProgresoTurnoEnemigo(Array<Enemigo> enemigos) {
        if (turnoResolviendose || combateFinalizado)
            return;

        for (Enemigo enemigo : enemigos) {
            if (enemigo.estaListo() && !enemigo.estaMuerto())
                turnoPreparado(enemigo);

            enemigo.acumularTiempoEsperado(Gdx.graphics.getDeltaTime());
        }
    }

    private void turnoPreparado(Enemigo enemigo) {
        turnoResolviendose = true;

        Aventurero objetivo = grupo.random();
        while (objetivo.estaMuerto())
            objetivo = grupo.random();

        if (!enemigo.tieneAtaqueEspecial()) {
            enemigo.atacar(objetivo);
            return;
        }

        boolean especial = MathUtils.randomBoolean();
        if (!especial) {
            enemigo.atacar(objetivo);
            return;
        }

        Vector2 posicion = new Vector2();
        posicion.x = objetivo.getX() - objetivo.getAncho() / 2;
        posicion.y = objetivo.getY() - objetivo.getAlto() / 2;

        enemigo.ataqueEspecial(objetivo);
        String ataqueEspecial = enemigo.getAtaquesEspeciales().random();
        GestorSonido.reproducirSonido(ataqueEspecial);

        efectoEspecial.cambiarAnimacion(ataqueEspecial);
        efectoEspecial.setPosicionInicial(posicion);
        efectoEspecial.solicitarRenderizado();
    }

    private void turnoPreparado(Aventurero aventurero) {
        turnoResolviendose = true;
        aventureroConTurnoDisponible = aventurero;

        tablaAcciones.seleccionarPrimeraCelda(false);
        tablaAcciones.setVisible(true);
        cabeceraAyuda.setVisible(true);
        actualizarCabecera();
        GestorSonido.reproducirSonido("turnoPreparado");
    }

    public void actualizar() {

        switch (faseActual) {
            case SELECCION:
                actualizarSeleccionTipoAccion();
                break;
            case MAGIA:
                actualizarSeleccionMagia();
                break;
            case INVOCACION:
                break;
            case ELEMENTOS:
                actualizarSeleccionElemento();
                break;
            case ELECCION_ALIADO:
                elegirAliadoObjetivo();
                break;
            case ELECCION_OBJETIVO:
                elegirEnemigoObjetivo();
                break;
            case GAME_OVER:
                return;
        }

        if (grupoHaSidoDerrotado()) {
            combateFinalizado = true;
            cabeceraAyuda.visualizarAyuda(" El grupo ha sido derrotado ");
            cursorEnemigos.setVisible(false);
            tablaAcciones.setVisible(false);

            Timer.schedule(new Timer.Task() {
                @Override
                public void run() {
                    GestorRecursos.restablecerMapasPorDefecto();
                    GestorPantallas.getInstance().dispose();
                    GestorPantallas.getInstance().cargar(Pantalla.MENU_PRINCIPAL);
                }
            }, 3);

            faseActual = GAME_OVER;
        }

        if (enemigos.size == 0) {
            combateFinalizado = true;
            cabeceraAyuda.visualizarAyuda(" Los enemigos han sido derrotados ");
            Timer.schedule(new Timer.Task() {
                @Override
                public void run() {
                    GestorPantallas.getInstance().cargar(Pantalla.RECOMPENSA);
                }
            }, 3);
        }

        if (combateFinalizado) {
            GestorSonido.mutearGradualmente("combate", 5);
            GestorSonido.mutearGradualmente("combateJefe", 5);
            faseActual = GAME_OVER;
        }
    }

    private void actualizarSeleccionTipoAccion() {
        if (!tablaAcciones.isVisible())
            return;

        if (controles.siguiente()) {
            tablaAcciones.celdaSiguiente();
            actualizarCabecera();
        }

        if (controles.anterior()) {
            tablaAcciones.celdaAnterior();
            actualizarCabecera();
        }

        if (controles.pulsacionConfirmar()) {
            confirmarTipoAccion();
            GestorSonido.reproducirSonido("desplazarCursor");
        }
    }

    private void actualizarCabecera() {
        String seleccion = tablaAcciones.getContenidoActual();

        if (seleccion.equalsIgnoreCase("atacar"))
            cabeceraAyuda.visualizarAyuda("Realiza un ataque fisico");
        else if (seleccion.equalsIgnoreCase("magia"))
            cabeceraAyuda.visualizarAyuda("Realiza un ataque magico");
        else if (seleccion.equalsIgnoreCase("invocar"))
            cabeceraAyuda.visualizarAyuda("Invoca a un aliado");
        else if (seleccion.equalsIgnoreCase("elementos"))
            cabeceraAyuda.visualizarAyuda("Utiliza un elemento");
    }

    private void actualizarSeleccionElemento() {
        if (controles.siguiente())
            tablaElementos.celdaSiguiente();

        if (controles.anterior())
            tablaElementos.celdaAnterior();

        if (controles.pulsacionConfirmar()) {
            if (tablaElementos.getContenidoActual().estaAgotado()) {
                GestorSonido.reproducirSonido("error");
                return;
            }
            seleccionarObjetivo(grupo.first());
            faseActual = ELECCION_ALIADO;
        }

        if (controles.pulsacionCancelar()) {
            tablaElementos.setVisible(false);
            tablaAcciones.setVisible(true);
            nombres.setVisible(true);
            faseActual = SELECCION;
            GestorSonido.reproducirSonido("atras");
        }
    }

    private void actualizarSeleccionMagia() {
        if (controles.siguiente())
            tablaMagias.celdaSiguiente();

        if (controles.anterior())
            tablaMagias.celdaAnterior();

        if (controles.pulsacionConfirmar()) {
            int manaActual = aventureroConTurnoDisponible.getMana().getValorActual();
            if (manaActual < 16) {
                GestorSonido.reproducirSonido("error");
                return;
            }
            seleccionarObjetivo(enemigos.first());
            faseActual = ELECCION_OBJETIVO;
        }

        if (controles.pulsacionCancelar()) {
            tablaMagias.setVisible(false);
            tablaAcciones.setVisible(true);
            nombres.setVisible(true);
            faseActual = SELECCION;
            GestorSonido.reproducirSonido("atras");
        }
    }

    private void confirmarTipoAccion() {
        String seleccion = tablaAcciones.getContenidoActual();

        if (seleccion.equalsIgnoreCase("Atacar")) {
            seleccionarObjetivo(enemigos.first());
            faseActual = ELECCION_OBJETIVO;
        }

        if (seleccion.equalsIgnoreCase("Elementos")) {
            tablaElementos.cargarContenido(Inventario.getInstance().getElementos());
            tablaElementos.seleccionarPrimeraCelda();
            nombres.setVisible(false);
            tablaAcciones.setVisible(false);
            tablaElementos.setVisible(true);
            faseActual = ELEMENTOS;
        }

        if (seleccion.equalsIgnoreCase("Magia")) {
            tablaMagias.cargarContenido(Inventario.getInstance().getMaterias());
            tablaMagias.seleccionarPrimeraCelda();
            nombres.setVisible(false);
            tablaAcciones.setVisible(false);
            tablaMagias.setVisible(true);
            faseActual = MAGIA;
        }

        if (seleccion.equalsIgnoreCase("invocar")) {
            GestorSonido.reproducirSonido("error");
            return;
        }
    }

    private void resolverTurnoAventurero() {
        cursorEnemigos.setVisible(false);
        cursorAliados.setVisible(false);
        tablaAcciones.setVisible(false);
        cabeceraAyuda.setVisible(false);
        tablaElementos.setVisible(false);
        tablaMagias.setVisible(false);
        nombres.setVisible(true);

        String seleccion = tablaAcciones.getContenidoActual();
        if (seleccion.equalsIgnoreCase("atacar"))
            aventureroConTurnoDisponible.atacar(enemigoSeleccionado);

        if (seleccion.equalsIgnoreCase("elementos")) {
            Elemento seleccionado = tablaElementos.getContenidoActual();
            aventureroConTurnoDisponible.usarElemento(seleccionado, aventureroSeleccionado);
            Vector2 posicion = new Vector2();
            posicion.x = aventureroSeleccionado.getX() - aventureroSeleccionado.getAncho();
            posicion.y = aventureroSeleccionado.getY() - aventureroSeleccionado.getAlto() / 2;
            efectoEspecial.cambiarAnimacion("item");
            efectoEspecial.setPosicionInicial(posicion);
            efectoEspecial.solicitarRenderizado();

            /* Renderizar el texto con la informacion del uso del elemento */
            GestorFuentes.getInstance().setTextoElemento(seleccionado);
            GestorFuentes.getInstance().setPosicion(aventureroSeleccionado);
        }

        if (seleccion.equalsIgnoreCase("magia")) {
            aventureroConTurnoDisponible.ataqueEspecial(enemigoSeleccionado);

            Vector2 posicion = new Vector2();
            posicion.x = enemigoSeleccionado.getX() - enemigoSeleccionado.getAncho() / 3;
            posicion.y = enemigoSeleccionado.getY();
            String magia = tablaMagias.getContenidoActual().getNombre();

            GestorSonido.reproducirSonido(magia.toLowerCase());
            efectoEspecial.cambiarAnimacion(magia.toLowerCase());
            efectoEspecial.setPosicionInicial(posicion);
            efectoEspecial.solicitarRenderizado();

            aventureroConTurnoDisponible.getMana().restar(16);
            cargarGrupo(grupo);
        }
    }

    private void elegirEnemigoObjetivo() {

        if (controles.siguiente())
            seleccionarObjetivo(enemigos.peek());

        if (controles.anterior())
            seleccionarObjetivo(enemigos.first());

        if (controles.pulsacionConfirmar())
            resolverTurnoAventurero();

        if (controles.pulsacionCancelar()) {
            actualizarCabecera();
            faseActual = SELECCION;
            cursorEnemigos.setVisible(false);
            tablaMagias.setVisible(false);
            GestorSonido.reproducirSonido("atras");
        }

        if (enemigoSeleccionado != null) {
            Rectangle rectEnemigo = enemigoSeleccionado.getRectangulo();
            float x = rectEnemigo.getX() + rectEnemigo.width;
            float y = rectEnemigo.getY() + rectEnemigo.height / 2;

            cursorEnemigos.setPosition(x, y);
        }
    }

    private void elegirAliadoObjetivo() {
        if (controles.siguiente())
            seleccionarObjetivo(grupo.peek());

        if (controles.anterior())
            seleccionarObjetivo(grupo.first());

        if (controles.pulsacionConfirmar())
            resolverTurnoAventurero();

        if (controles.pulsacionCancelar()) {
            actualizarCabecera();
            faseActual = ELEMENTOS;
            cursorAliados.setVisible(false);
            GestorSonido.reproducirSonido("atras");
        }
    }

    private void seleccionarObjetivo(Entidad entidad) {
        if (entidad instanceof Enemigo) {
            cursorEnemigos.setVisible(true);
            if (entidad == enemigoSeleccionado)
                return;

            enemigoSeleccionado = (Enemigo) entidad;

            Rectangle rectangulo = enemigoSeleccionado.getRectangulo();
            float x = rectangulo.getX() + rectangulo.width;
            float y = rectangulo.getY() + rectangulo.height / 2;
            cursorEnemigos.setPosition(x, y);
        }

        if (entidad instanceof Aventurero) {
            cursorAliados.setVisible(true);
            if (entidad == aventureroSeleccionado)
                return;

            aventureroSeleccionado = (Aventurero) entidad;

            Rectangle rectangulo = aventureroSeleccionado.getRectangulo();
            float x = rectangulo.getX() - 50;
            float y = rectangulo.getY() + rectangulo.height / 2;
            cursorAliados.setPosition(x, y);
        }

        cabeceraAyuda.visualizarAyuda(entidad.getNombre());
        GestorSonido.reproducirSonido("desplazarCursor");
    }

    @Override
    public void update(Observable observado, Object arg) {
        final String evento = (String) arg;

        switch (evento) {
            case "muriendo":
                eventoMuriendo(observado);
                break;
            case "muerto":
                eventoMuerte(observado);
                break;
            case "ataqueFinalizado":
                eventoAtaqueFinalizado(observado);
                break;
            case "elementoFinalizado":
                eventoElementoFinalizado();
                break;
            case "especialFinalizado":
                eventoEspecialFinalizado(observado);
                break;
        }
    }

    private void eventoMuriendo(Observable observado) {
        Entidad entidad = (Entidad) observado;
        entidad.setEstadoActual(Entidad.Estado.MURIENDO, 1.5f);
    }

    private void eventoMuerte(Observable observado) {
        if (observado instanceof Enemigo)
            enemigos.removeValue((Enemigo) observado, false);
    }

    private void eventoAtaqueFinalizado(Observable observado) {
        if (observado instanceof Aventurero)
            aventureroConTurnoDisponible.resetearTiempoEsperado();

        if (observado instanceof Enemigo)
            ((Enemigo) observado).resetearTiempoEsperado();

        faseActual = SELECCION;
        cargarGrupo(grupo);
        liberarTurno();
    }

    private void eventoElementoFinalizado() {
        GestorSonido.reproducirSonido("efectoElemento");
        GestorFuentes.getInstance().solicitarRenderizado();
        aventureroConTurnoDisponible.resetearTiempoEsperado();

        faseActual = SELECCION;
        cargarGrupo(grupo);
        liberarTurno();
    }

    private void eventoEspecialFinalizado(Observable observado) {
        GestorFuentes.getInstance().solicitarRenderizado();

        if (observado instanceof Aventurero)
            aventureroConTurnoDisponible.resetearTiempoEsperado();

        if (observado instanceof Enemigo)
            ((Enemigo) observado).resetearTiempoEsperado();

        faseActual = SELECCION;
        cargarGrupo(grupo);
        liberarTurno();
    }

    private void liberarTurno() {
        Timer.schedule(new Timer.Task() {
            @Override
            public void run() {
                turnoResolviendose = false;
            }
        }, 1.5f);
    }

    public boolean grupoHaSidoDerrotado() {
        for (int i = 0; i < grupo.size; i++) {
            if (!grupo.get(i).estaMuerto())
                return false;
        }
        return true;
    }

    public void setFaseActual(FaseMenuBatalla faseActual) {
        this.faseActual = faseActual;
    }

    public int getExperienciaObtenida() {
        return experienciaObtenida;
    }

    public int getDineroObtenido() {
        return dineroObtenido;
    }

    public Image getCursorEnemigos() {
        return cursorEnemigos;
    }

    public Image getCursorAliados() {
        return cursorAliados;
    }

    public TablaDeslizableElementos getTablaElementos() {
        return tablaElementos;
    }

    public TablaInformacionEmergente getCabeceraAyuda() {
        return cabeceraAyuda;
    }

    public TablaSeleccionVertical getTablaAcciones() {
        return tablaAcciones;
    }

    public boolean esEncuentroAleatorio() {
        for (Enemigo enemigo : enemigos) {
            if (enemigo.esJefe())
                return false;
        }
        return true;
    }

    public void setTurnoResolviendose(boolean turnoResolviendose) {
        this.turnoResolviendose = turnoResolviendose;
    }

    public TablaDeslizableMagia getTablaMagias() {
        return tablaMagias;
    }
}

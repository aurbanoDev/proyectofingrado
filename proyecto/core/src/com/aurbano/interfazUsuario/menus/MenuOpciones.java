package com.aurbano.interfazUsuario.menus;

import com.aurbano.gestores.GestorPantallas;
import com.aurbano.gestores.GestorSonido;
import com.aurbano.gestores.controles.GestorControles;
import com.aurbano.interfazUsuario.celdas.CeldaOpcion;
import com.aurbano.interfazUsuario.tablas.TablaAzul;
import com.aurbano.interfazUsuario.tablas.TablaSeleccionOpciones;
import com.aurbano.juego.Constantes;
import com.aurbano.pantallas.Pantalla;
import com.aurbano.persistencia.GestorPersistencia;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Array;


/**
 * Created by infe on 12/05/2016.
 */
public class MenuOpciones extends Table {

    private TablaAzul cabecera;
    private TablaAzul titulo;
    private TablaAzul cuerpo;
    private TablaSeleccionOpciones opciones;

    private GestorControles controles;

    public MenuOpciones() {
        top().left();
        setBounds(Constantes.X_FULL_SCREEN + 75, Constantes.Y_FULL_SCREEN + 50, Constantes.ANCHO_TOTAL - 150, Constantes.ALTO_TOTAL - 70);
        controles = GestorControles.getInstance();
        cabecera = new TablaAzul();
        titulo = new TablaAzul();
        cuerpo = new TablaAzul();

        Array<String> lista = new Array();
        lista.addAll("Ayuda Si No", "Música Si No", "Sonido Si No", "Dificultad Normal Difícil");

        opciones = new TablaSeleccionOpciones();
        opciones.defaults().left().pad(10);
        opciones.cargarContenido(lista);

        cabecera.setFondoAzul();
        cabecera.activarSkin();
        cabecera.left();
        cabecera.defaults().padLeft(45);

        titulo.setFondoAzul();
        titulo.activarSkin();
        titulo.center();
        titulo.add("Opciones");

        cuerpo.setFondoAzul();
        cuerpo.activarSkin();
        cuerpo.top().left();
        cuerpo.defaults().pad(20).padTop(45);
        cuerpo.add(opciones);

        Table contenedor = new Table();
        contenedor.defaults().height(60);
        contenedor.add(cabecera).fillX().expandX();
        contenedor.add(titulo).width(Gdx.graphics.getWidth() * 0.18f);

        add(contenedor).expandX().fillX().row();
        add(cuerpo).fill().expand();
    }

    public void actualizar() {
        if (controles.siguiente()) {
            opciones.celdaSiguiente();
            actualizarCabecera();
        }

        if (controles.anterior()) {
            opciones.celdaAnterior();
            actualizarCabecera();
        }

        if (controles.pulsacionCancelar()) {
            guardarConfiguracion();
            GestorPantallas.getInstance().cargar(Pantalla.JUEGO);
        }

        if (controles.pulsacionConfirmar()) {
            CeldaOpcion seleccionada = opciones.getCeldaSeleccionada();
            seleccionada.siguienteOpcion();
            GestorSonido.reproducirSonido("desplazarCursor");
        }
    }

    private void guardarConfiguracion() {
        boolean ayuda = parsearBooleano(opciones.getContenido(0));
        boolean musica = parsearBooleano(opciones.getContenido(1));
        boolean sonido = parsearBooleano(opciones.getContenido(2));
        boolean dificultad = parsearBooleano(opciones.getContenido(3));

        GestorPersistencia.guardarPreferencia("ayuda", ayuda);
        GestorPersistencia.guardarPreferencia("musica", musica);
        GestorPersistencia.guardarPreferencia("sonido", sonido);
        GestorPersistencia.guardarPreferencia("dificultad", dificultad);
        GestorSonido.configurarAudio(sonido , musica);
    }

    public void actualizarCabecera() {
        cabecera.clear();

        final int indiceSeleccionado = opciones.getIndiceCeldaSeleccionada();
        switch (indiceSeleccionado) {
            case 0:
                cabecera.add("Muestra indicadores y ayudas durante el juego");
                break;
            case 1:
                cabecera.add("Reproducir durante el juego música ambiental");
                break;
            case 2:
                cabecera.add("Reproducir durante el juego efectos");
                break;
            case 3:
                cabecera.add("La dificultad modificará las estadísticas de tus enemigos");
                break;
        }
    }

    public void actualizarPreferencias() {
        GestorPersistencia persistencia = GestorPersistencia.getInstance();

        CeldaOpcion opcionAyuda = (CeldaOpcion) opciones.getCeldas()[0];
        CeldaOpcion opcionMusica = (CeldaOpcion) opciones.getCeldas()[1];
        CeldaOpcion opcionSonido = (CeldaOpcion) opciones.getCeldas()[2];
        CeldaOpcion opcionDificultad = (CeldaOpcion) opciones.getCeldas()[3];

        opcionAyuda.setOpcionActivada(persistencia.getPreferenciaBoolean("ayuda"));
        opcionSonido.setOpcionActivada(persistencia.getPreferenciaBoolean("sonido"));
        opcionMusica.setOpcionActivada(persistencia.getPreferenciaBoolean("musica"));
        opcionDificultad.setOpcionActivada(persistencia.getPreferenciaBoolean("dificultad"));
    }

    private boolean parsearBooleano(String booleano) {
        if (booleano.equalsIgnoreCase("Si"))
            return true;

        if (booleano.equalsIgnoreCase("No"))
            return false;

        if (booleano.equalsIgnoreCase("normal"))
            return false;

        if (booleano.equalsIgnoreCase("difícil"))
            return true;

        return false;
    }

    public TablaSeleccionOpciones getOpciones() {
        return opciones;
    }

}

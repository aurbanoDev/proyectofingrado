package com.aurbano.interfazUsuario.menus;

import com.aurbano.entidades.Aventurero;
import com.aurbano.interfazUsuario.tablas.TablaAtributos;
import com.aurbano.interfazUsuario.tablas.TablaAzul;
import com.aurbano.interfazUsuario.tablas.TablaDetallesEquipo;
import com.aurbano.interfazUsuario.tablas.TablaTitulo;
import com.aurbano.interfazUsuario.tablas.cabeceras.CabeceraEstado;
import com.aurbano.juego.Constantes;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

/**
 * Created by adriu on 07/01/2016.
 */
public class MenuEstado extends Table {

     public CabeceraEstado cabecera;
     public TablaAtributos atributos;
     public TablaDetallesEquipo equipo;
     public TablaAzul tablaAzul;
     public TablaTitulo titulo;

    public MenuEstado() {
        cabecera = new CabeceraEstado();
        atributos = new TablaAtributos();
        equipo = new TablaDetallesEquipo();
        titulo = new TablaTitulo("Estado Actual");

        tablaAzul = new TablaAzul();
        tablaAzul.setFondoAzul();
        tablaAzul.top().left();
        tablaAzul.defaults().left().padTop(30);

        top().left();
        setBounds(Constantes.X_FULL_SCREEN, Constantes.Y_FULL_SCREEN, Constantes.ANCHO_TOTAL, Constantes.ALTO_TOTAL);
    }

    public void cargarDatos(Aventurero aventurero) {

        cabecera.cargarDatos(aventurero);
        atributos.cargarAmbas(aventurero);
        equipo.cargarDatos(aventurero);

        tablaAzul.clear();
        tablaAzul.add(equipo).row();
        tablaAzul.add().space(5).row();
        tablaAzul.add(atributos);

        clear();
        add(cabecera).expandX().fillX().row();
        add(tablaAzul).expand().fill();
    }
}

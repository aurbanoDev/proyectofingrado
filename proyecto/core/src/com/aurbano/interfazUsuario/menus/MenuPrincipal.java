package com.aurbano.interfazUsuario.menus;

import com.aurbano.interfazUsuario.tablas.TablaAzul;
import com.aurbano.interfazUsuario.tablas.TablaSeleccionVertical;
import com.aurbano.gestores.GestorRecursos;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

import java.util.ArrayList;
import java.util.Arrays;
import static com.aurbano.juego.Constantes.*;

/**
 * Created by adriu on 06/01/2016.
 */
public class MenuPrincipal extends TablaAzul {

    private Image logo;
    private TablaSeleccionVertical seleccion;

    public MenuPrincipal() {
        seleccion = new TablaSeleccionVertical();
        seleccion.cargarContenido(new ArrayList<String>(Arrays.asList(OPCIONES_MENU_PRINCIPAL)), 5);
        seleccion.setEstiloNegrita();
        seleccion.left();
        logo = GestorRecursos.nuevaImagen(ATLAS_GUI, "logo");

        add(logo).row();
        add(seleccion).padRight(Gdx.graphics.getWidth() / 9).padTop(20);
        setBounds(X_FULL_SCREEN, Y_FULL_SCREEN, ANCHO_TOTAL, ALTO_TOTAL);
    }

    public TablaSeleccionVertical getSeleccion() {
        return seleccion;
    }
}

package com.aurbano.interfazUsuario.menus;

import com.aurbano.gestores.GestorPantallas;
import com.aurbano.gestores.GestorSonido;
import com.aurbano.gestores.controles.GestorControles;
import com.aurbano.interfazUsuario.tablas.TablaAzul;
import com.aurbano.interfazUsuario.tablas.TablaSeleccionEmergente;
import com.aurbano.interfazUsuario.tablas.TablaSeleccionPartida;
import com.aurbano.juego.Constantes;
import com.aurbano.juego.Mundo;
import com.aurbano.pantallas.Pantalla;
import com.aurbano.persistencia.EstadoMundo;
import com.aurbano.persistencia.GestorPersistencia;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Array;

import static com.aurbano.interfazUsuario.menus.MenuSalvarPartida.FaseMenuGuardado.CONFIRMACION;
import static com.aurbano.interfazUsuario.menus.MenuSalvarPartida.FaseMenuGuardado.SELECCION;

/**
 * Created by infe on 06/05/2016.
 */
public class MenuSalvarPartida extends Table {

    private TablaAzul cabecera;
    private TablaAzul titulo;
    private TablaAzul ranura;

    private TablaSeleccionEmergente tablaConfirmar;
    private TablaSeleccionPartida tablaSeleccionPartida;

    private Mundo world;
    private GestorControles controles;

    private FaseMenuGuardado faseActual;
    private boolean salvando;

    public enum FaseMenuGuardado {
        SELECCION, CONFIRMACION
    }

    public MenuSalvarPartida(Mundo world) {
        this.world = world;
        this.faseActual = SELECCION;

        top().left();
        setBounds(Constantes.X_FULL_SCREEN + 75, Constantes.Y_FULL_SCREEN, Constantes.ANCHO_TOTAL - 150, Constantes.ALTO_TOTAL - 20);

        cabecera = new TablaAzul();
        titulo = new TablaAzul();
        ranura = new TablaAzul();

        cabecera.setFondoAzul();
        cabecera.activarSkin();
        cabecera.center();
        cabecera.add("Selecciona una ranura para continuar");

        titulo.setFondoAzul();
        titulo.activarSkin();
        titulo.center();

        ranura.setFondoAzul();
        ranura.activarSkin();
        ranura.center();

        tablaSeleccionPartida = new TablaSeleccionPartida();
        tablaSeleccionPartida.defaults().height(140);
        tablaSeleccionPartida.defaults().expandX().fillX();

        tablaConfirmar = new TablaSeleccionEmergente();
        tablaConfirmar.setFondoAzul();
        tablaConfirmar.setSize(460, 150);

        controles = GestorControles.getInstance();

        Table contenedor = new Table();
        contenedor.defaults().height(70);
        contenedor.add(cabecera).width(Gdx.graphics.getWidth() * 0.5f);
        contenedor.add(ranura).width(Gdx.graphics.getWidth() * 0.16f);
        contenedor.add(titulo).fillX().expandX();

        add(contenedor).expandX().fillX().row();
        add(tablaSeleccionPartida).expandX().fillX();
    }

    public void actualizarNumeroRanura(int numero) {
        ranura.clear();
        ranura.add("RANURA  ", "fuente", Color.YELLOW);
        ranura.add(String.valueOf(numero));
    }

    public void cargarRanurasDisponibles() {
        GestorPersistencia persistencia = GestorPersistencia.getInstance();
        Array<EstadoMundo> ranurasDisponibles = persistencia.getSavesDisponibles();

        tablaSeleccionPartida.cargarContenido(ranurasDisponibles);
        tablaSeleccionPartida.seleccionar(0);
        actualizarNumeroRanura(1);
        cargarTitulo();
    }

    private void cargarTitulo() {
        titulo.clear();
        if (salvando)
            titulo.add("GUARDAR");
        else
            titulo.add("CARGAR");
    }

    public void actualizar() {

        switch (faseActual) {
            case SELECCION:
                actualizarSeleccionGuardado();
                break;
            case CONFIRMACION:
                actualizarConfirmacionGuardado();
                break;
        }
    }

    private void actualizarSeleccionGuardado() {
        if (controles.siguiente()) {
            tablaSeleccionPartida.celdaSiguiente();
            actualizarNumeroRanura(tablaSeleccionPartida.getIndiceCeldaSeleccionada() + 1);
        }

        if (controles.anterior()) {
            tablaSeleccionPartida.celdaAnterior();
            actualizarNumeroRanura(tablaSeleccionPartida.getIndiceCeldaSeleccionada() + 1);
        }

        if (controles.pulsacionCancelar()) {
            GestorSonido.reproducirSonido("atras");
            if (salvando) {
                GestorPantallas.getInstance().cargar(Pantalla.JUEGO);
            } else {
                GestorSonido.pararMusica("ambiente");
                GestorPantallas.getInstance().cargar(Pantalla.MENU_PRINCIPAL);
            }
        }

        if (controles.pulsacionConfirmar()) {
            if (salvando) {
                cargarVentanaConfirmacionGuardado();
            } else {
                cargarVentanaConfirmacionCarga();
            }

            faseActual = CONFIRMACION;
            GestorSonido.reproducirSonido("desplazarCursor");
        }
    }

    private void actualizarConfirmacionGuardado() {
        if (controles.siguiente())
            tablaConfirmar.celdaSiguiente();

        if (controles.anterior())
            tablaConfirmar.celdaAnterior();

        if (controles.pulsacionCancelar()) {
            cancelarVentanaConfirmacion();
            return;
        }

        if (controles.pulsacionConfirmar()) {

            if (tablaConfirmar.seleccionadaOpcion("No, lo he pensado mejor")) {
                cancelarVentanaConfirmacion();
                return;
            }

            if (salvando)
                pulsadoConfirmarGuardado();
            else
                pulsadoConfirmarCarga();
        }
    }

    private void pulsadoConfirmarGuardado() {
        salvarPartida();
        tablaConfirmar.ocultar();
        cargarRanurasDisponibles();
        faseActual = SELECCION;
    }

    private void pulsadoConfirmarCarga() {
        tablaConfirmar.ocultar();
        faseActual = SELECCION;

        EstadoMundo estadoRecuperado = tablaSeleccionPartida.getContenidoActual();
        int ranuraSalvado = tablaSeleccionPartida.getIndiceCeldaSeleccionada() + 1;

        GestorPersistencia persistencia = GestorPersistencia.getInstance();
        persistencia.recuperarEstadoDelMundo(estadoRecuperado);
        persistencia.cargarPreferencias(ranuraSalvado);
        GestorPantallas.getInstance().cargar(Pantalla.JUEGO);
    }

    private void cancelarVentanaConfirmacion() {
        tablaConfirmar.ocultar();
        faseActual = SELECCION;
        GestorSonido.reproducirSonido("atras");
    }

    private void cargarVentanaConfirmacionGuardado() {
        Array<String> opciones = new Array<>(2);
        opciones.addAll("Si, quiero guardar la Partida", "No, lo he pensado mejor");

        cargarVentanaConfirmacion(opciones, "¿Quieres guardar ahora el progreso?");
    }

    private void cargarVentanaConfirmacionCarga() {
        Array<String> opciones = new Array<>(2);
        opciones.addAll("Si, quiero cargar la Partida", "No, lo he pensado mejor");

        cargarVentanaConfirmacion(opciones, "¿Quieres cargar ahora la partida?");
    }

    private void cargarVentanaConfirmacion(Array<String> opciones, String... cabecera) {
        Vector2 posicion = tablaSeleccionPartida.getPosicionCeldaSeleccionada();
        tablaConfirmar.setPosition(posicion.x, posicion.y);
        tablaConfirmar.cargarContenidoConCabecera(opciones, cabecera);
        tablaConfirmar.setVisible(true);
    }

    private void salvarPartida() {
        int ranuraSeleccionada = tablaSeleccionPartida.getIndiceCeldaSeleccionada() + 1;
        GestorPersistencia.getInstance().guardarPartida(ranuraSeleccionada, world);
        GestorSonido.reproducirSonido("confirmar");
    }

    public TablaSeleccionEmergente getTablaConfirmar() {
        return tablaConfirmar;
    }

    public void setSalvando(boolean salvando) {
        this.salvando = salvando;
    }
}

package com.aurbano.interfazUsuario.menus;

import com.aurbano.gestores.GestorSonido;
import com.aurbano.gestores.controles.GestorControles;
import com.aurbano.entidades.Aventurero;
import com.aurbano.interfazUsuario.tablas.TablaAyuda;
import com.aurbano.interfazUsuario.tablas.TablaTitulo;
import com.aurbano.interfazUsuario.tablas.cabeceras.CabeceraEquipo;
import com.aurbano.interfazUsuario.tablas.secciones.SeccionEquipo;
import com.aurbano.juego.Constantes;
import com.aurbano.gestores.GestorPantallas;
import com.aurbano.objetos.Accesorio;
import com.aurbano.objetos.Arma;
import com.aurbano.objetos.Armadura;
import com.aurbano.objetos.Equipo;
import com.aurbano.pantallas.Pantalla;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

/**
 * Created by adriu on 11/12/2015.
 */
public class MenuEquipo extends Table {

     public Aventurero aventurero;
     public TablaAyuda seccionAyuda;
     public CabeceraEquipo cabecera;
     public SeccionEquipo seccionEquipo;
     public TablaTitulo titulo;
     public FaseMenuEquipo faseActual;

    private enum FaseMenuEquipo {
        CABECERA, SELECCION, SALIR
    }

    public MenuEquipo() {
        seccionAyuda = new TablaAyuda();
        cabecera = new CabeceraEquipo();
        seccionEquipo = new SeccionEquipo();
        titulo = new TablaTitulo("Equipamiento");

        top();
        setBounds(Constantes.X_FULL_SCREEN, Constantes.Y_FULL_SCREEN, Constantes.ANCHO_TOTAL, Constantes.ALTO_TOTAL);
        defaults().width(Constantes.ANCHO_TOTAL);

        add(cabecera).row();
        add(seccionAyuda).row();
        add(seccionEquipo).expand().fill();
    }

    public void cargarDatos(Aventurero aventurero) {
        this.aventurero = aventurero;

        seccionEquipo.actualizarAtributos(aventurero);
        cabecera.refrescar(aventurero);
        cabecera.seleccionarPrimeraCelda();
        abrirMenuCabecera();
    }

    public void actualizar() {

        switch (faseActual) {
            case CABECERA:
                actualizarCabecera();
                break;
            case SELECCION:
                actualizarSeleccion();
                break;
            case SALIR:
                GestorPantallas.getInstance().cargar(Pantalla.MENU_RESUMEN);
                GestorSonido.reproducirSonido("atras");
                break;
        }
    }

    private void actualizarCabecera() {
        GestorControles controles = GestorControles.getInstance();

        if (controles.siguiente())
            siguiente_faseCabecera();

        if (controles.anterior())
            anterior_faseCabecera();

        if (controles.pulsacionConfirmar())
            abrirMenuSeleccion();

        if (controles.pulsacionCancelar())
            faseActual = FaseMenuEquipo.SALIR;
    }

    private void siguiente_faseCabecera() {
        cabecera.celdaSiguiente();
        seccionEquipo.cargarRanuras(cabecera.getContenidoSeleccionado());
        seccionAyuda.mostrarAyuda(cabecera.getContenidoSeleccionado().getDescripcion());
        refrescarListaEquipo();
    }

    private void anterior_faseCabecera() {
        cabecera.celdaAnterior();
        seccionEquipo.cargarRanuras(cabecera.getContenidoSeleccionado());
        seccionAyuda.mostrarAyuda(cabecera.getContenidoSeleccionado().getDescripcion());
        refrescarListaEquipo();
    }

    private void abrirMenuSeleccion() {
        faseActual = FaseMenuEquipo.SELECCION;
        seccionEquipo.getListaEquipo().seleccionarPrimeraCelda();
        seccionEquipo.cargarRanuras(seccionEquipo.getEquipoSeleccionado());
        seccionAyuda.mostrarAyuda(seccionEquipo.getEquipoSeleccionado().getDescripcion());
        seccionEquipo.mostrarComparacion(seccionEquipo.getEquipoSeleccionado());
    }

    private void actualizarSeleccion() {
        GestorControles controles = GestorControles.getInstance();

        if (controles.siguiente())
            siguiente_faseSeleccion();

        if (controles.anterior())
            anterior_faseSeleccion();

        if (controles.pulsacionConfirmar())
            confirmar_faseSeleccion();

        if (controles.pulsacionCancelar())
            abrirMenuCabecera();
    }

    private void siguiente_faseSeleccion() {
        seccionEquipo.getListaEquipo().celdaSiguiente();
        seccionEquipo.cargarRanuras(seccionEquipo.getEquipoSeleccionado());
        seccionAyuda.mostrarAyuda(seccionEquipo.getEquipoSeleccionado().getDescripcion());
        seccionEquipo.mostrarComparacion(seccionEquipo.getEquipoSeleccionado());
    }

    private void anterior_faseSeleccion() {
        seccionEquipo.getListaEquipo().celdaAnterior();
        seccionEquipo.cargarRanuras(seccionEquipo.getEquipoSeleccionado());
        seccionAyuda.mostrarAyuda(seccionEquipo.getEquipoSeleccionado().getDescripcion());
        seccionEquipo.mostrarComparacion(seccionEquipo.getEquipoSeleccionado());
    }

    private void confirmar_faseSeleccion() {
        if (seccionEquipo.getEquipoSeleccionado() instanceof Arma)
            aventurero.equiparArma((Arma) seccionEquipo.getEquipoSeleccionado());

        if (seccionEquipo.getEquipoSeleccionado() instanceof Armadura)
            aventurero.equiparArmadura((Armadura) seccionEquipo.getEquipoSeleccionado());

        if (seccionEquipo.getEquipoSeleccionado() instanceof Accesorio)
            aventurero.equiparAccesorio((Accesorio) seccionEquipo.getEquipoSeleccionado());

        cabecera.refrescar(aventurero);
        seccionEquipo.actualizarAtributos(aventurero);
        abrirMenuCabecera();
    }

    private void abrirMenuCabecera() {
        faseActual = FaseMenuEquipo.CABECERA;
        refrescarListaEquipo();
        seccionAyuda.mostrarAyuda(cabecera.getContenidoSeleccionado().getDescripcion());
        seccionEquipo.cargarRanuras(cabecera.getContenidoSeleccionado());
        seccionEquipo.getListaEquipo().limpiarSeleccion();
        seccionEquipo.mostrarSeparadores(false);
        seccionEquipo.mostrarAtributosFuturos(false);
    }

    private void refrescarListaEquipo() {
        Equipo equipo = cabecera.getContenidoSeleccionado();

        if (equipo instanceof Arma)
            seccionEquipo.listarArmas();
        else if (equipo instanceof Armadura)
            seccionEquipo.listarArmaduras();
        else
            seccionEquipo.listarAccesorios();
    }
}

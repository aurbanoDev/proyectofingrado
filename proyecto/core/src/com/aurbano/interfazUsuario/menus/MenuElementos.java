package com.aurbano.interfazUsuario.menus;

import com.aurbano.entidades.Aventurero;
import com.aurbano.interfazUsuario.tablas.*;
import com.aurbano.interfazUsuario.tablas.tablasScroll.TablaDeslizableObjetos;
import com.aurbano.juego.Constantes;
import com.aurbano.objetos.Inventario;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Array;

import static com.aurbano.juego.Constantes.*;

/**
 * Created by adriu on 28/12/2015.
 */
public class MenuElementos extends TablaAzul {

     public TablaAyuda tablaAyuda;
     public TablaAyuda tablaTitulo;
     public TablaSeleccionHorizontal cabecera;
     public TablaPersonajes tablaPersonajes;
     public TablaDeslizableObjetos tablaObjetos;
     public TablaSeleccionEmergente tablaTiposOrden;
     public FaseMenuElementos faseActual;

    public enum FaseMenuElementos {
        CABECERA, ORDENAR, USAR, ELEGIR_OBJETIVO, SALIR
    }

    public MenuElementos() {
        inicializarCabecera();
        inicializarTablaEmergente();

        tablaPersonajes = new TablaPersonajes();
        tablaObjetos = new TablaDeslizableObjetos();
        tablaObjetos.cargarContenido(Inventario.getInstance().getListaCompleta());

        top();
        setBounds(Constantes.X_FULL_SCREEN, Constantes.Y_FULL_SCREEN, Constantes.ANCHO_TOTAL, Constantes.ALTO_TOTAL);
        defaults().width(Constantes.ANCHO_TOTAL);

        Table contenedorSuperior = new Table();
        contenedorSuperior.add(cabecera).expandX().fillX();
        contenedorSuperior.add(tablaTitulo).width(defaults().getMaxWidth() * 0.15f);

        Table contenedorInferior = new Table();
        contenedorInferior.defaults().expand().fill();

        contenedorInferior.add(tablaPersonajes);
        contenedorInferior.add(tablaObjetos);

        add(contenedorSuperior).row();
        add(tablaAyuda).height(55).row();
        add(contenedorInferior).expandY().fill();

        faseActual = FaseMenuElementos.CABECERA;
    }

    private void inicializarCabecera() {
        tablaAyuda = new TablaAyuda();
        tablaTitulo = new TablaAyuda("Elementos");

        Array<String> acciones = new Array<String>();
        acciones.addAll(OPCIONES_ELEMENTOS);

        cabecera = new TablaSeleccionHorizontal();
        cabecera.cargarContenido(acciones);
        cabecera.left();
    }

    private void inicializarTablaEmergente() {
        tablaTiposOrden = new TablaSeleccionEmergente();
        tablaTiposOrden.setPosition(X_EMERGENTE, Y_EMERGENTE);
        tablaTiposOrden.cargarContenido(OPCIONES_ORDENAR);
    }

    public void cargarMenu(Array<Aventurero> grupo) {
        setFaseActual(FaseMenuElementos.CABECERA);
        tablaPersonajes.cargarContenido(grupo);
        tablaObjetos.cargarContenido(Inventario.getInstance().getListaCompleta());
    }

    public void mostrarDescripcion() {
        String descripcion = tablaObjetos.getContenidoActual().getDescripcion();
        tablaAyuda.mostrarAyuda(descripcion);
    }

    public void siguienteObjeto() {
        tablaObjetos.celdaSiguiente();
        mostrarDescripcion();
    }

    public void anteriorObjeto() {
        tablaObjetos.celdaAnterior();
        mostrarDescripcion();
    }

    public void setFaseActual(FaseMenuElementos faseActual) {
        this.faseActual = faseActual;
    }
}

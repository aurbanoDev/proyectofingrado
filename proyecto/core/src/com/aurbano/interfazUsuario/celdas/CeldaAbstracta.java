package com.aurbano.interfazUsuario.celdas;

import com.aurbano.gestores.GestorRecursos;
import com.aurbano.interfazUsuario.tablas.TablaAzul;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Scaling;

import static com.aurbano.juego.Constantes.ATLAS_GUI;

/**
 * Created by adriu on 29/11/2015.
 */
public abstract class CeldaAbstracta <E> extends TablaAzul {

    protected E contenido;
    protected Image cursor;
    protected boolean celdaSeleccionada;

    public CeldaAbstracta() {
        this(null);
    }

    public CeldaAbstracta(E contenido) {
        this.contenido = contenido;

        cursor = new Image(GestorRecursos.getAtlas(ATLAS_GUI).findRegion("cursor"));
        cursor.setScaling(Scaling.stretch);
        cursor.setVisible(false);
    }

    public void setSeleccion(boolean seleccionada) {
        celdaSeleccionada = seleccionada;
        cursor.setVisible(seleccionada);
    }

    public E getContenido() {
        return contenido;
    }

    public void setContenido(E nuevoContenido) {
        contenido = nuevoContenido;
    }

    public void refrescar(){};
}

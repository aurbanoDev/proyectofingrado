package com.aurbano.interfazUsuario.celdas;

import com.aurbano.entidades.Aventurero;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

/**
 * Created by adriu on 29/11/2015.
 */
public class CeldaPersonaje extends CeldaAbstracta<Aventurero> {

    private Image avatar;
    private Label nombre, nivel, salud, mana;
    private Label siglasNivel, siglasHP, siglasMP;

    public CeldaPersonaje(Aventurero aventurero) {
        super(aventurero);

        iniciarComponentes();
    }

    private void iniciarComponentes() {
        avatar = contenido.getAvatar();
        siglasNivel = new Label("NV", estiloSigla);
        siglasHP = new Label("HP", estiloSigla);
        siglasMP = new Label("MP", estiloSigla);

        nombre = new Label(contenido.getNombre(), estiloBlanco);
        nivel = new Label(String.valueOf(contenido.getNivel()), estiloBlanco);
        salud = new Label(contenido.getVida().getEstadoCompleto(), estiloBlanco);
        mana = new Label(contenido.getMana().getEstadoCompleto(), estiloBlanco);

        Table ficha = new Table();
        ficha.add(nombre).width(50).height(35).row();
        ficha.add(siglasNivel).width(50).height(35); ficha.add(nivel).width(175).height(35).row();
        ficha.add(siglasHP).width(50).height(35); ficha.add(salud).width(175).height(35).row();
        ficha.add(siglasMP).width(50).height(35); ficha.add(mana).width(175).height(35).row();

        add(cursor).width(50).height(25);
        add(avatar).width(142).height(150);
        add(ficha).width(250).height(140);
    }

    public void refrescar() {
        nombre.setText(contenido.getNombre());
        nivel.setText(String.valueOf(contenido.getNivel()));
        salud.setText(contenido.getVida().getEstadoCompleto());
        mana.setText(contenido.getMana().getEstadoCompleto());
    }

}

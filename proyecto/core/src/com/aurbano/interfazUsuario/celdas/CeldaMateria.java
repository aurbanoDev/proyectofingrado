package com.aurbano.interfazUsuario.celdas;

import com.aurbano.Utilidades.UtilsGdx;
import com.aurbano.objetos.Materia;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.Scaling;

/**
 * Created by infe on 02/06/2016.
 */
public class CeldaMateria extends CeldaAbstracta<Materia> {

    private Label cantidadMP;
    private Label nombre;
    private Image icono;

    public CeldaMateria(Materia contenido) {
        super(contenido);
        iniciarComponentes();
    }

    private void iniciarComponentes() {
        icono = contenido.getIcono();
        icono.setScaling(Scaling.stretch);
        nombre = new Label(contenido.getNombre(), estiloBlanco);
        cantidadMP = new Label(UtilsGdx.formatearCantidad(16) + " mp ", estiloBlanco);

        add(cursor).height(25).width(50).padTop(3);
        add().spaceRight(10);
        add(icono).height(30).width(30).padRight(10);
        add(nombre).height(40).width(185).padTop(5);
        add(cantidadMP).height(40).width(85).padTop(5);
    }

    public void refrescar() {
        StringBuffer cantidadObjetos = UtilsGdx.formatearCantidad(contenido.getCantidad());
        cantidadMP.setText(cantidadObjetos);
        nombre.setText(contenido.getNombre());
    }
}

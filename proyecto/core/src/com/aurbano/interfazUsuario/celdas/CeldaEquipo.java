package com.aurbano.interfazUsuario.celdas;

import com.aurbano.objetos.Equipo;
import com.badlogic.gdx.scenes.scene2d.ui.Label;

/**
 * Created by adriu on 03/12/2015.
 */
public class CeldaEquipo extends CeldaAbstracta<Equipo> {

    private Label siglas;
    private Label nombre;

    public CeldaEquipo() {
        super();

        iniciarComponentes();
    }

    private void iniciarComponentes() {
        siglas = new Label("", estiloSigla);
        nombre = new Label("", estiloBlanco);

        add(cursor).height(25).width(50);
        add(siglas).height(35).width(80);
        add(nombre).height(35).width(60);
    }

    public void refrescar() {
        if (contenido == null)
            return;

        siglas.setText(getSiglas(contenido));
        nombre.setText(contenido.getNombre());
    }

    public void setSiglasArma() {
        siglas.setText("Arm.");
    }

    public void setSiglasArmadura() {
        siglas.setText("Amd.");
    }

    public void setSiglasAccesorio() {
        siglas.setText("Acc.");
    }

    public String getSiglas(Equipo equipo) {
        switch (equipo.getTipo()) {
            case ARMA_BASTON:
            case ARMA_ESPADA:
            case ARMA_LANZA:
                return "Arm.";
            case ARMADURA:
                return "Amd.";
            case ACCESORIO:
               return "Acc.";
        }
        return null;
    }
}

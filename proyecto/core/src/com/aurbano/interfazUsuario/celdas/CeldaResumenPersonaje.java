package com.aurbano.interfazUsuario.celdas;

import com.aurbano.entidades.Aventurero;
import com.aurbano.gestores.GestorRecursos;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ProgressBar;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

/**
 * Created by adriu on 29/12/2015.
 */
public class CeldaResumenPersonaje extends CeldaAbstracta<Aventurero> {

    private Image avatar;
    private Label nombre, nivel, salud, mana;
    private Label siglasNivel, siglasHP, siglasMP;

    private Label etiquetaBarra;
    private Table contenedorBarra;
    private ProgressBar barraExperiencia;

    public CeldaResumenPersonaje(Aventurero aventurero) {
        super(aventurero);

        incializarBarra();
        iniciarComponentes();
    }

    private void iniciarComponentes() {
        avatar = contenido.getAvatar();
        siglasNivel = new Label("NV", estiloSigla);
        siglasHP = new Label("HP", estiloSigla);
        siglasMP = new Label("MP", estiloSigla);

        nombre = new Label(contenido.getNombre(), estiloBlanco);
        nivel = new Label(String.valueOf(contenido.getNivel()), estiloBlanco);
        salud = new Label(contenido.getVida().getEstadoCompleto(), estiloBlanco);
        mana = new Label(contenido.getMana().getEstadoCompleto(), estiloBlanco);

        Table ficha = new Table();
        ficha.defaults().height(35);

        ficha.add(nombre).width(50).row();
        ficha.add(siglasNivel).width(50); ficha.add(nivel).width(175); ficha.add(contenedorBarra).row();
        ficha.add(siglasHP).width(50); ficha.add(salud).width(175).row();
        ficha.add(siglasMP).width(50); ficha.add(mana).width(175).row();

        add(cursor).width(50).height(25);
        add(avatar).width(142).height(150);
        add(ficha).width(550).height(140);
    }

    private void incializarBarra() {
        etiquetaBarra = new Label("Proximo Nivel", estiloBlanco);
        barraExperiencia = new ProgressBar(0, 100, 1, false, GestorRecursos.getEstiloBarraProgreso());
        barraExperiencia.setValue(contenido.getExperiencia().getPorcentaje());

        contenedorBarra = new Table();
        contenedorBarra.defaults().padLeft(30).width(250).center();
        contenedorBarra.add(etiquetaBarra).row();
        contenedorBarra.add(barraExperiencia);
    }

    public void refrescar() {
        nombre.setText(contenido.getNombre());
        nivel.setText(String.valueOf(contenido.getNivel()));
        salud.setText(contenido.getVida().getEstadoCompleto());
        mana.setText(contenido.getMana().getEstadoCompleto());
        barraExperiencia.setValue(contenido.getExperiencia().getPorcentaje());
    }
}

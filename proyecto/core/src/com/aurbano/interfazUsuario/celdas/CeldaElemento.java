package com.aurbano.interfazUsuario.celdas;

import com.aurbano.Utilidades.UtilsGdx;
import com.aurbano.objetos.Elemento;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.Scaling;

/**
 * Created by infe on 16/05/2016.
 */
public class CeldaElemento extends CeldaAbstracta<Elemento> {

    private Label cantidad;
    private Label nombre;
    private Image icono;

    public CeldaElemento(Elemento contenido) {
        super(contenido);
        iniciarComponentes();
    }

    private void iniciarComponentes() {
        icono = contenido.getIcono();
        icono.setScaling(Scaling.stretch);
        nombre = new Label(contenido.getNombre(), estiloBlanco);
        cantidad = new Label(UtilsGdx.formatearCantidad(contenido.getCantidad()), estiloBlanco);

        add(cursor).height(25).width(50).padTop(3);
        add().spaceRight(10);
        add(icono).height(30).width(30).padRight(10);
        add(nombre).height(40).width(200).padTop(5);
        add(cantidad).height(40).width(70).padTop(5);
    }

    public void refrescar() {
        StringBuffer cantidadObjetos = UtilsGdx.formatearCantidad(contenido.getCantidad());
        cantidad.setText(cantidadObjetos);
        nombre.setText(contenido.getNombre());
    }
}

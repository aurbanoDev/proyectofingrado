package com.aurbano.interfazUsuario.celdas;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.ui.Label;

/**
 * Created by infe on 12/05/2016.
 */
public class CeldaOpcion extends CeldaAbstracta<String> {

    private Label descripcion;
    private Label opcion1;
    private Label opcion2;

    public CeldaOpcion(String nombre) {
        super(nombre);

        defaults().padLeft(20);
        String[] partes = nombre.split(" ");

        descripcion = new Label(partes[0], estiloCyan);
        opcion1 = new Label(partes[1], estiloBlanco);
        opcion2 = new Label(partes[2], estiloDesactivado);

        add(cursor).width(50).height(25);
        add(descripcion).width(Gdx.graphics.getWidth() * 0.24f);
        add(opcion1).width(150);
        add(opcion2);
    }

    public void activar(int opcion) {
        if (opcion == 1) {
            opcion1.setStyle(estiloBlanco);
            opcion2.setStyle(estiloDesactivado);
        }

        if (opcion == 2) {
            opcion2.setStyle(estiloBlanco);
            opcion1.setStyle(estiloDesactivado);
        }
    }

    public String getOpcionActivada() {
        if (opcion1.getStyle() == estiloBlanco)
            return String.valueOf(opcion1.getText());

        return String.valueOf(opcion2.getText());
    }

    public void setOpcionActivada(boolean activada) {
        if (activada)
            activar(1);
        else
            activar(2);
    }

    public void cargarFila(String texto , String valor1, String valor2) {
        descripcion.setText(texto);
        opcion1.setText(valor1);
        opcion2.setText(valor2);
    }

    public void siguienteOpcion() {
        if (opcion1.getStyle() == estiloDesactivado)
            activar(1);
        else
            activar(2);
    }

    public String getOpcionSeleccionada() {
        if (opcion1.getStyle() == estiloDesactivado)
            return opcion2.getText().toString();
        else
            return opcion1.getText().toString();
    }
}

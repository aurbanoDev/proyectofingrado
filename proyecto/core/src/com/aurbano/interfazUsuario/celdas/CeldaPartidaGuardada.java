package com.aurbano.interfazUsuario.celdas;

import com.aurbano.entidades.Aventurero;
import com.aurbano.interfazUsuario.tablas.TablaAzul;
import com.aurbano.juego.Constantes;
import com.aurbano.persistencia.EstadoMundo;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Scaling;

/**
 * Created by infe on 06/05/2016.
 */
public class CeldaPartidaGuardada extends CeldaAbstracta<EstadoMundo> {

    private Table contenedor;
    private TablaAzul tablaLocalizacion;
    private TablaAzul tablaTiempoOro;

    public CeldaPartidaGuardada(EstadoMundo estadoMundo) {
        super(estadoMundo);

        activarSkin();
        setFondoAzul();
        iniciarComponentes();
        getBackground().setRightWidth(-1);
        getBackground().setBottomHeight(-0.5f);
    }

    private void iniciarComponentes() {
        setMargenIzquierdo(50);
        add(cursor).width(50).height(25);

        if (contenido.isVacio()) {
            add().space(40);
            add(" RANURA VACIA");
            return;
        }

        tablaLocalizacion = new TablaAzul();
        tablaLocalizacion.defaults().width(Constantes.ANCHO_TOTAL / 2);
        tablaLocalizacion.activarSkin();
        tablaLocalizacion.setFondoAzul();
        tablaLocalizacion.add(contenido.getLocalizacion()).padLeft(5);
        tablaLocalizacion.left();

        tablaTiempoOro = new TablaAzul();
        tablaTiempoOro.defaults().left();
        tablaTiempoOro.activarSkin();
        tablaTiempoOro.setFondoAzul();
        tablaTiempoOro.add("Tiempo   " + contenido.getTiempoJuegoFormatedo()).row();
        tablaTiempoOro.add("Dinero    " + contenido.getDinero());

        TablaAzul datosProta = new TablaAzul();
        datosProta.defaults().left();
        datosProta.left();
        datosProta.activarSkin();
        datosProta.add(contenido.getProtagonista().getNombre()).row();
        datosProta.add("Nivel", "cyan");
        datosProta.add(" " + contenido.getProtagonista().getNivel());

        contenedor = new Table();
        contenedor.defaults().right();
        contenedor.right();
        contenedor.add(datosProta).left();
        contenedor.add(tablaTiempoOro);
        contenedor.row();
        contenedor.add(tablaLocalizacion).colspan(2).height(52);

        add().space(10);
        for (Aventurero aventurero : contenido.getGrupo()) {
            Image retrato = aventurero.getRetrato();
            retrato.setScaling(Scaling.stretch);
            add(retrato).width(124).height(124).padBottom(14);
            add().space(15);
        }

        add(contenedor).expandX().fillX();
    }
}

package com.aurbano.interfazUsuario.celdas;

import com.badlogic.gdx.scenes.scene2d.ui.Label;

/**
 * Created by adriu on 29/11/2015.
 */
public class CeldaBasica extends CeldaAbstracta<String> {

    private Label texto;
    private boolean activada;

    public CeldaBasica(String nombre) {
        super(nombre);
        texto = new Label(nombre, estiloBlanco);

        add(cursor).height(25).width(50).padTop(3);
        add(texto).height(38).width(texto.getWidth());
    }

    public void setEstiloActivado(boolean activado) {
        this.activada = activado;

        if (activado)
            texto.setStyle(estiloBlanco);
        else
            texto.setStyle(estiloDesactivado);
    }

    public void setEstiloNegrita() {
        texto.setStyle(estiloMenuPrincipal);
    }

    public boolean estaActivada() {
        return activada;
    }
}
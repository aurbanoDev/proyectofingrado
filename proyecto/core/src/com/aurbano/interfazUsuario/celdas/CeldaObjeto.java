package com.aurbano.interfazUsuario.celdas;

import com.aurbano.Utilidades.UtilsGdx;
import com.aurbano.objetos.Objeto;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.Scaling;

/**
 * Created by adriu on 27/11/2015.
 */
public class CeldaObjeto extends CeldaAbstracta<Objeto> {

    private Label cantidad;
    private Label nombre;
    private Image icono;
    private boolean activada;

    public CeldaObjeto(Objeto objeto) {
        super(objeto);

        iniciarComponentes();
    }

    private void iniciarComponentes() {
        icono = contenido.getIcono();
        icono.setScaling(Scaling.stretch);
        nombre = new Label(contenido.getNombre(), estiloBlanco);
        cantidad = new Label(UtilsGdx.formatearCantidad(contenido.getCantidad()), estiloBlanco);

        add(cursor).height(25).width(50).padLeft(12).padTop(3);
        add(icono).height(35).width(32).padRight(10);
        add(nombre).height(40).width(350).padTop(5);
        add(cantidad).height(40).width(60).padTop(5).padRight(25);
    }

    public void setEstiloActivado(boolean activado) {
        this.activada = activado;

        if (activado) {
            nombre.setStyle(estiloBlanco);
            cantidad.setStyle(estiloBlanco);
        } else {
            nombre.setStyle(estiloDesactivado);
            cantidad.setStyle(estiloDesactivado);
        }
    }

}

package com.aurbano.juego;

import com.aurbano.entidades.*;
import com.aurbano.gestores.GestorFuentes;
import com.aurbano.mapas.RenderizadorMapas;
import com.aurbano.persistencia.GestorPersistencia;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.math.MathUtils;


/**
 * Clase encargada de renderizar los gráficos.
 * Cuenta con una OrthogonalTiledMapRenderer preparado para renderizar MapObjects.
 *
 * @author Adrián Urbano
 */
public class Renderizador {

    private Mundo world;
    private SpriteBatch batch;
    private OrthographicCamera camara;
    private RenderizadorMapas renderizadorMapas;

    /* Lista de Referencias a entidades que se renderizaran */
    private Protagonista protagonista;
    private Aventurero lenn;
    private PuntoGuardado puntoGuardado;
    private Enemigo jefe;

    public Renderizador(Mundo world) {
        this.world = world;
    }

    /**
     * Prepara el renderizador con el tiled map actual y ajusta la camara.
     */
    public void prepararRenderizadorMapas() {
        TiledMap mapa = world.getMapaActual().getMapaTiled();
        renderizadorMapas = new RenderizadorMapas(mapa, batch);
        renderizadorMapas.setView(camara);
        referenciarEntidades();
    }

    private void referenciarEntidades() {
        protagonista = world.protagonista;
        lenn = world.lenn;
        jefe = world.jefe;
        puntoGuardado = world.mapaActual.getObjetoPuntoGuardado();
    }

    public void renderizar() {

        switch (world.getEstadoJuegoActual()) {
            case MAPA:
            case MAPA_EN_ESPERA:
                actualizarEnfoque();
                renderizarModoMapa();
                break;
            case TRANSICION:
                prepararRenderizadorMapas();
                actualizarEnfoque();
                break;
            case BATALLA:
                renderizarBatalla();
                break;
        }
    }

    /**
     * Actualiza la camara para evitar que el personaje se salga del enfoque.
     */
    private void actualizarEnfoque() {
        camara.position.x = MathUtils.clamp(protagonista.getX(), camara.viewportWidth / 2,  40 * 32 - camara.viewportWidth / 2);
        camara.position.y = MathUtils.clamp(protagonista.getY(), camara.viewportHeight / 2, 23 * 32 - camara.viewportHeight / 2);
        camara.update();
        renderizadorMapas.setView(camara);
    }

    private void renderizarModoMapa() {
        batch.begin();
        batch.setColor(1, 1, 1, 1);

        renderizadorMapas.renderTileLayer(world.getMapaActual().getCapaSuelo());
        renderizadorMapas.renderObjects(world.getMapaActual().getCapaObjetos());
        renderizadorMapas.renderTileLayer(world.getMapaActual().getCapaMedia());

        protagonista.renderizar(batch);
        puntoGuardado.renderizar(batch);

        if (world.mapaActual.tieneCombateContraJefe() && jefe != null && !jefe.estaMuerto())
            jefe.renderizar(batch);

        renderizadorMapas.renderTileLayer(world.getMapaActual().getCapaAlta());
        if (GestorPersistencia.getPreferenciaBoolean("ayuda"))
            renderizadorMapas.renderTileLayer(world.getMapaActual().getCapaAyuda());

        if (world.getTablaInformacionEmergente().solicitaRenderizado())
            world.getTablaInformacionEmergente().draw(batch, 1);

        if (world.getTablaDialogo().isVisible())
            world.getTablaDialogo().draw(batch, 1);

        batch.end();
    }

    private void renderizarBatalla() {
        batch.begin();
        world.getCampoBatallaActual().draw(batch, 1);

        if (protagonista.estaAtacando() || lenn.estaAtacando()) {
            for (Enemigo enemigo : world.enemigos)
                enemigo.renderizar(batch);

            protagonista.renderizar(batch);
            lenn.renderizar(batch);
        } else {
            protagonista.renderizar(batch);
            lenn.renderizar(batch);

            for (Enemigo enemigo : world.enemigos)
                enemigo.renderizar(batch);
        }

        EfectoEspecial.getInstance().renderizar(batch);
        GestorFuentes.getInstance().renderizar(batch);
        batch.end();
    }

    public void setBatch(SpriteBatch batch) {
        this.batch = batch;
    }

    public void setCamara(OrthographicCamera camara) {
        this.camara = camara;
    }
}

package com.aurbano.juego;

import com.badlogic.gdx.Input;

import java.io.File;

public class Constantes {

	// Constantes jugador
	public static final float TIEMPO_ACCION_BASE = 5;
	public static final float VELOCIDAD_MOVIMIENTO = 75;
	public static final float ANCHO_SPRITE = 32;
	public static final float ALTO_SPRITE = 64;

	//CodigosTeclas por defecto
	public static final int CONTROL_ARRIBA = Input.Keys.UP;
	public static final int CONTROL_ABAJO = Input.Keys.DOWN;
	public static final int CONTROL_IZQUIERDA = Input.Keys.LEFT;
	public static final int CONTROL_DERECHA = Input.Keys.RIGHT;
	public static final int CONTROL_CONFIRMAR = Input.Keys.A;
	public static final int CONTROL_CANCELAR = Input.Keys.S;
	public static final int CONTROL_MENU = Input.Keys.D;

	//Fuentes
	public static String RUTA_FUENTE_PRINCIPAL = "fuentes/verdana.ttf";
	public static String RUTA_FUENTE_NEGRITA = "fuentes/verdanab.ttf";
	public static String FUENTE_BLANCA = "textoBlanco";
	public static String FUENTE_CYAN = "textoCyan";
	public static String FUENTE_SIGLA_CYAN = "siglaCyan";
	public static String FUENTE_MENU_PRINCIPAL = "menuPrincipal";

	//TILED
	public static final float LADO_TILE = 32;

	//DISTRIBUCION GRAFICA

    public static float EJE_X_CENTRAL = 1280 * 0.01f;
    public static float ANCHO_CENTRAL = 1280 * 0.84f;
    public static float ALTO_CENTRAL = 720 * 0.75f;

    public static float X_EMERGENTE = EJE_X_CENTRAL + ANCHO_CENTRAL * 0.34f;
    public static float Y_EMERGENTE = ALTO_CENTRAL * 0.99f;

    public static float X_FULL_SCREEN = 1280 * 0.03f;
    public static float ANCHO_TOTAL = 1280 * 0.94f;

    public static float Y_FULL_SCREEN = 720 * 0.03f;
    public static float ALTO_TOTAL = 720 * 0.94f;

    public static float X_TITULO = X_FULL_SCREEN + ANCHO_TOTAL;
    public static float Y_TITULO = Y_FULL_SCREEN + ALTO_TOTAL;


	//LISTA DE OPCIONES DE MENUS
	public static final String[] OPCIONES_MENU_PRINCIPAL = {"Nueva Partida", "Cargar Partida", "Salir del Juego"};
    public static final String[] OPCIONES_ELEMENTOS = {"Usar", "Ordenar", "Objetos Clave"};
	public static final String[] OPCIONES_ORDENAR = {"Tipo", "Batalla", "Nombre", "Cantidad"};
	public static final String[] TITULOS_SECCIONES =
			{"Elementos", "Materia", "Equipo", "Magia", "Estado", "Opciones", "Guardar", "Salir"};

	public static final String[] COMANDOS_BATALLA = {"Atacar", "Magia", "Invocar", "Elementos"};

	public static final String ATLAS_GUI = "gui";

	public static final String FICHERO_CONFIGURACION = "tales.cfg";
	public static final String RUTA_CARPETA = System.getProperty("user.home") + File.separator + "toz" + File.separator;
	public static final String RUTA_CARPETA_SAVES = RUTA_CARPETA + "saves" + File.separator;
	public static final String FICHERO_SQLITE = RUTA_CARPETA + "tales.db";
}

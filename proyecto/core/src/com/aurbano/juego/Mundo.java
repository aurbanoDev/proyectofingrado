package com.aurbano.juego;

import com.aurbano.entidades.*;
import com.aurbano.entidades.Entidad.Estado;
import com.aurbano.gestores.GestorEfectos;
import com.aurbano.gestores.GestorPantallas;
import com.aurbano.gestores.GestorRecursos;
import com.aurbano.gestores.GestorSonido;
import com.aurbano.gestores.controles.GestorControles;
import com.aurbano.interfazUsuario.menus.*;
import com.aurbano.interfazUsuario.tablas.TablaDialogo;
import com.aurbano.interfazUsuario.tablas.TablaInformacionEmergente;
import com.aurbano.mapas.Mapa;
import com.aurbano.objetos.Inventario;
import com.aurbano.objetos.Objeto;
import com.aurbano.pantallas.Pantalla;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.objects.TextureMapObject;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Timer;

import static com.aurbano.juego.Constantes.ATLAS_GUI;
import static com.aurbano.juego.Mundo.EstadoJuego.MAPA_EN_ESPERA;

/**
 * Clase que recoge y representa el estado y las actualizaciones del mundo.
 *
 * @author Adrián Urbano
 */
public class Mundo {

    public Protagonista protagonista;
    public Aventurero lenn;
    public Enemigo jefe;

    public Array<Aventurero> grupo;
    public Array<Enemigo> enemigos;

    public MenuEquipo menuEquipo;
    public MenuElementos menuElementos;
    public MenuResumen menuResumen;
    public MenuEstado menuEstado;
    public MenuBatalla menuBatalla;
    public MenuSalvarPartida menuSalvarCargarPartida;

    public TablaDialogo tablaDialogo;
    public TablaInformacionEmergente tablaInformacionEmergente;

    public GestorControles controles;
    public EstadoJuego estadoJuegoActual;

    public Mapa mapaActual;
    public PuntoGuardado puntoGuardado;

    public float tiempoJuego;
    public float tiempoUltimoEncuentro;


    public enum EstadoJuego {
        MAPA, BATALLA, TRANSICION, MAPA_EN_ESPERA
    }

    public Mundo() {
        mapaActual = new Mapa();
        mapaActual.cargarMapa("ciudadBosque");
        puntoGuardado = mapaActual.getObjetoPuntoGuardado();
        controles = GestorControles.getInstance();

        Inventario.restablecer();
        inicializarEntidades();
        inicializarIU();
    }

    public void inicializarEntidades() {
        protagonista = new Protagonista("Zak", 150, 250);
        protagonista.setMapaActual(mapaActual);

        lenn = new Aventurero("Lenn");
        lenn.setMapaActual(mapaActual);

        enemigos = new Array<>();
        grupo = new Array<>();

        grupo.add(protagonista);
        grupo.add(lenn);
    }

    private void inicializarIU() {
        menuEquipo = new MenuEquipo();
        menuResumen = new MenuResumen();
        menuElementos = new MenuElementos();
        menuEstado = new MenuEstado();
        tablaDialogo = new TablaDialogo();
        tablaInformacionEmergente = new TablaInformacionEmergente();
        menuSalvarCargarPartida = new MenuSalvarPartida(this);
        menuBatalla = new MenuBatalla();
    }

    /**
     * Actualiza el estado del mundo dependiendo del estado en el que se encuentre.
     *
     * @param delta time
     */
    public void actualizar(float delta) {

        switch (estadoJuegoActual) {
            case MAPA:
                actualizarMapa(delta);
                break;
            case MAPA_EN_ESPERA:
                actualizarModoEnEspera();
                break;
            case TRANSICION:
                Timer.schedule(new Timer.Task() {
                    @Override
                    public void run() {
                        estadoJuegoActual = EstadoJuego.MAPA;
                    }
                }, 1);
                break;
            case BATALLA:
                actualizarCombate(delta);
                break;
        }
    }

    private void actualizarMapa(float delta) {
        actualizarMovimientoProtagonistaEnElMapa();
        comprobarTransiciones();
        comprobarPuntoGuardado();

        tablaInformacionEmergente.actualizarTiempoRenderizado();

        if (controles.pulsacionMenu()) {
            menuResumen.tablaSecciones.seleccionarPrimeraCelda();
            GestorPantallas.getInstance().cargar(Pantalla.MENU_RESUMEN);
            GestorEfectos.interpolacionEntrada(menuResumen);
        }

        if (mapaActual.tieneCombateContraJefe() && jefe != null) {
            jefe.actualizarEstado(delta);
            comprobarInteraccionConJefe();
        }

        if (controles.pulsacionConfirmar())
            pulsadoInteractuar();

        if (mapaActual.esPacifico())
            return;

        tiempoUltimoEncuentro += delta;
        if (hayEncuentroAleatorio()) {
            enemigos = FactoriaEnemigos.nuevoEncuentroAleatorio(mapaActual.getIdentificadoresEnemigos());
            prepararBatalla();
        }
    }

    private void actualizarMovimientoProtagonistaEnElMapa() {
        if (controles.mantenidoArriba())
            protagonista.setEstadoActual(Estado.ARRIBA);
        else if (controles.mantenidoAbajo())
            protagonista.setEstadoActual(Estado.ABAJO);
        else if (controles.mantenidoIzquierda())
            protagonista.setEstadoActual(Estado.IZQUIERDA);
        else if (controles.mantenidoDerecha())
            protagonista.setEstadoActual(Estado.DERECHA);
        else
            protagonista.setEstadoActual(Estado.QUIETO);

        protagonista.actualizarMovimiento(Gdx.graphics.getDeltaTime());
    }

    /**
     * Comprueba si el jugador accede a una zona marcada como transicion en el mapa.
     * En caso de que acceda a una transicion se leera del mapa el destino, la posicion
     * y la direccion inicial en la que mira el jugador. cargará el mapa destino.
     */
    private void comprobarTransiciones() {
        for (RectangleMapObject transicion : mapaActual.getObjetosTransicion()) {
            if (!protagonista.getRectangulo().overlaps(transicion.getRectangle()))
                continue;

            tiempoUltimoEncuentro = 0;
            estadoJuegoActual = EstadoJuego.TRANSICION;

            String destino = transicion.getProperties().get("destino", String.class);
            String direccion = transicion.getProperties().get("direccion", String.class);

            mapaActual.cargarMapa(destino);
            puntoGuardado = mapaActual.getObjetoPuntoGuardado();

            if (mapaActual.tieneCombateContraJefe())
                jefe = mapaActual.getJefe();

            float posicionX = Float.parseFloat(transicion.getProperties().get("posicionX", String.class));
            float posicionY = Float.parseFloat(transicion.getProperties().get("posicionY", String.class));
            protagonista.setPosicion(posicionX, posicionY);
            protagonista.setUltimoEstado(Estado.valueOf(direccion));
        }
    }

    private void comprobarPuntoGuardado() {
        if (!protagonista.getRectangulo().overlaps(puntoGuardado.getRectangulo())) {
            menuResumen.setGuardado(false);
            return;
        }

        if (!menuResumen.estaGuardadoActivado())
            GestorSonido.reproducirSonido("saveActivado");

        menuResumen.setGuardado(true);
        puntoGuardado.actualizar(Gdx.graphics.getDeltaTime());
    }

    private void comprobarInteraccionConJefe() {

        Rectangle areaJefeVencido = mapaActual.getAreaFinDelCapitulo();
        if (protagonista.getRectangulo().overlaps(areaJefeVencido)) {
            GestorRecursos.restablecerMapasPorDefecto();
            GestorPantallas.getInstance().dispose();
            GestorPantallas.getInstance().cargar(Pantalla.MENU_PRINCIPAL);
        }

        Rectangle areaJefe = mapaActual.getAreaEventoJefe();
        if (!protagonista.getRectangulo().overlaps(areaJefe))
            return;

        if (jefe.estaMuerto())
            return;

        enemigos.clear();
        enemigos.add(jefe);
        prepararBatalla();
    }

    private void pulsadoInteractuar() {
        comprobarInteraccionesConNPCs();
        comprobarInteraccionesConObjetos();
    }

    private void comprobarInteraccionesConNPCs() {
        for (RectangleMapObject dialogo : mapaActual.getObjetosDialogo()) {
            if (dialogo.getRectangle().overlaps(protagonista.getRectangulo())) {
                String texto = dialogo.getProperties().get("texto", String.class);
                String nombreRetrato = dialogo.getProperties().get("retrato", String.class);
                tablaDialogo.actualizarRetrato(GestorRecursos.nuevaImagen(ATLAS_GUI, nombreRetrato));
                tablaDialogo.actualizarTexto(texto.split("W"));
                tablaDialogo.setVisible(true);
                estadoJuegoActual = MAPA_EN_ESPERA;
            }
        }
    }

    private void comprobarInteraccionesConObjetos() {
        Array<Objeto> contenido;

        for (TextureMapObject objetoTexurizado : mapaActual.getObjetosTexturizados()) {
            if (!protagonista.puedeRecoger(objetoTexurizado))
                continue;

            mapaActual.animarRecogidaObjeto(objetoTexurizado);
            contenido = mapaActual.recuperarContenido(objetoTexurizado);

            tablaInformacionEmergente.setPosicionRelativa(protagonista);
            tablaInformacionEmergente.visualizarContenido(contenido);
            protagonista.getInventario().guardar(contenido);
            estadoJuegoActual = MAPA_EN_ESPERA;
        }
    }

    private boolean hayEncuentroAleatorio() {
        if (tiempoUltimoEncuentro < 10)
            return false;

        return MathUtils.randomBoolean();
    }

    private void prepararBatalla() {
        grupo.clear();
        grupo.add(protagonista);
        grupo.add(lenn);

        protagonista.guardarPosicionPreBatalla();

        for (int i = 0; i < enemigos.size; i++)
            enemigos.get(i).setPosicionDeBatalla(i + 1);

        for (int i = 0; i < grupo.size; i++) {
            grupo.get(i).setEstadoActual(Estado.ESPERANDO);
            grupo.get(i).setPosicionDeBatalla(i + 1);
        }

        menuBatalla.cargarEnemigos(enemigos);
        menuBatalla.cargarGrupo(grupo);

        tiempoUltimoEncuentro = 0;
        GestorPantallas.getInstance().cargar(Pantalla.BATALLA);
    }

    private void actualizarModoEnEspera() {
        if (!controles.pulsacionConfirmar())
            return;

        estadoJuegoActual = EstadoJuego.MAPA;
        tablaInformacionEmergente.desactivarModoRenderizado();
        tablaDialogo.setVisible(false);
    }

    private void actualizarCombate(float delta) {
        menuBatalla.actualizar();
        menuBatalla.actualizarProgresoTurnoGrupo(grupo, delta);
        menuBatalla.actualizarProgresoTurnoEnemigo(enemigos);

        for (Aventurero aventurero : grupo)
            aventurero.actualizarEstado(delta);

        for (Enemigo enemigo : enemigos)
            enemigo.actualizarEstado(delta);

        EfectoEspecial.getInstance().actualizar(delta);
    }

    public EstadoJuego getEstadoJuegoActual() {
        return estadoJuegoActual;
    }

    public Protagonista getProtagonista() {
        return protagonista;
    }

    public Mapa getMapaActual() {
        return mapaActual;
    }

    public Array<Aventurero> getGrupo() {
        return grupo;
    }

    public Image getCampoBatallaActual() {
        return mapaActual.getCampoBatalla();
    }

    public MenuEquipo getMenuEquipo() {
        return menuEquipo;
    }

    public MenuElementos getMenuElementos() {
        return menuElementos;
    }

    public MenuResumen getMenuResumen() {
        return menuResumen;
    }

    public MenuEstado getMenuEstado() {
        return menuEstado;
    }

    public TablaInformacionEmergente getTablaInformacionEmergente() {
        return tablaInformacionEmergente;
    }

    public TablaDialogo getTablaDialogo() {
        return tablaDialogo;
    }

    public MenuBatalla getMenuBatalla() {
        return menuBatalla;
    }

    public boolean estaGuardadoActivado() {
        return menuResumen.estaGuardadoActivado();
    }

    public void acumularTiempoJuego(float tiempo) {
        this.tiempoJuego += tiempo;
    }

    public int getTiempoJuego() {
        return (int) tiempoJuego;
    }

    public MenuSalvarPartida getMenuSalvarCargarPartida() {
        return menuSalvarCargarPartida;
    }

    public void setTiempoJuego(float tiempoJuego) {
        this.tiempoJuego = tiempoJuego;
    }

    public void setProtagonista(Protagonista protagonista) {
        this.protagonista = protagonista;
    }

    public void setEstadoJuegoActual(EstadoJuego estadoJuegoActual) {
        this.estadoJuegoActual = estadoJuegoActual;
    }
}

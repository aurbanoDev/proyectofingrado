package com.aurbano.juego;

import com.aurbano.Utilidades.UtilsGdx;
import com.aurbano.gestores.GestorPantallas;
import com.aurbano.pantallas.Pantalla;
import com.aurbano.persistencia.GestorPersistencia;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Created by adriu on 25/11/2015.
 */
public class Juego extends Game {

    public Mundo world;
    public SpriteBatch batch;
    public Renderizador renderizador;
    public OrthographicCamera camara;
    public AssetManager manager;

    @Override
    public void create() {
        batch = new SpriteBatch();
        manager = new AssetManager();
        camara = UtilsGdx.nuevaCamaraFullScreen();

        GestorPersistencia.getInstance().inicializar(this);
        GestorPersistencia.getInstance().asegurarRutas();
        GestorPantallas.getInstance().inicializar(this);
        GestorPantallas.getInstance().cargar(Pantalla.CARGA);
    }

    /**
     * Inicializa la representacion del mundo con todos sus elementos ( entidades , objetos ... )
     * Prepara el Renderizador del juego, configurando la matriz de la camara.
     *
     * (Importante)
     * Inicializar el mundo y configurar el renderizador de tiledMaps necesita que esten todos los recursos cargados
     * es por eso que este metodo no se utiliza al crear el objeto Juego.
     */
    public void inicializarNucleo() {
        world = new Mundo();
        renderizador = new Renderizador(world);
        renderizador.setBatch(batch);
        renderizador.setCamara(camara);
        renderizador.prepararRenderizadorMapas();
    }

    public void inicializarNucleo(Mundo world) {
        renderizador = new Renderizador(world);
        renderizador.setBatch(batch);
        renderizador.setCamara(camara);
        renderizador.prepararRenderizadorMapas();
    }

    @Override
    public void render() {
        super.render();
    }

    @Override
    public void dispose() {
        super.dispose();

        GestorPantallas.getInstance().dispose();
    }

    @Override
    public void pause() {
        super.pause();
    }

    @Override
    public void resume() {
        super.resume();
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
    }
}
